# Dcm

Dcm is a highly experimental build tool for Sencha **Ext JS** that aims to provide a flexible and extensible build process.

Dcm is **not**:
- Production ready
- A replacement for established tooling

## Goals

- Building existing JS and SCSS into deliverable assets with minimal adjustments
- Providing some fixes and optimizations out of the box
- Allowing developers to extend the build process

## Non-goals

- 100% compatibility with existing projects (especially the **Microloader**)
- Project / package scaffolding

## Current status

- [ ] Tooling
  - [ ] Build
    - [x] Basic proof-of-concept demo
    - [ ] Proper build tool
    - [ ] Minification / post-processing
  - [ ] Watch
    - [ ] Basic functionality
    - [ ] Hot reload
- [ ] JS
  - [x] Class definition support
  - [x] Assignment-style class definition support
  - [x] Class dependency awareness
  - [x] Override awareness
  - [ ] **Sencha Cmd** directive support (`//<debug>` and the like)
  - [ ] `callParent` inlining
  - [ ] Transformation to ESM
- [ ] SCSS
  - [ ] Language support
    - [x] CSS tokenization
    - [x] Parsing
      - [x] Variable declarations
      - [x] Basic expressions
      - [x] Function call expressions
      - [x] Map expressions
      - [x] Style rule blocks
      - [x] Selectors
      - [ ] Interpolation
      - [x] `@import` statements
      - [x] `@include` statements
      - [x] `@debug` / `@warn` / `@error` statements
      - [x] `@mixin` / `@function` declarations
      - [x] `@extend` statements
      - [ ] `@at-root` statements
      - [ ] Flow control
        - [x] `@if` / `@else` / `@else if` blocks
        - [x] `@for` statements
        - [x] `@return` statements
        - [x] `@each` statements
        - [ ] `@while` statements
  - [ ] Compilation to CSS (probably out of scope)
  - [ ] **Ext JS** flavored extension support
    - [x] `dynamic()` variable declarations
    - [ ] Resource path functions
    - [ ] Preprocessing (`/*<if ...>/*`)
  - [ ] Class awareness
- [ ] Resources