﻿using Breezy.Dcm.Core;
using Breezy.Dcm.Core.TypeScript;
using Microsoft.Extensions.Logging.Abstractions;

namespace Breezy.Dcm.Tests;

public class TypeDefinitionTests
{
    private StructureVisitorAction Action = null!;
    private TypeScriptDefinitionGenerator DefinitionGenerator = null!;

    [OneTimeSetUp]
    public async Task InitAction()
    {
        var config = new BuildConfiguration();
        await config.DiscoverPackages(new List<DirectoryInfo>() { ExtPaths.ExtPackagesDirectory, ExtPaths.ExtClassicDirectory },
            new List<string> { "classic" },
            "classic", null);
        
        Action = new StructureVisitorAction(config, NullLogger.Instance, NullLoggerFactory.Instance);
        await Action.Init();

        DefinitionGenerator = new TypeScriptDefinitionGenerator(Action.ExamineVisitor);
    }

    [Test]
    public void TestBasic()
    {
        var tsNodes = DefinitionGenerator.GenerateDefinition("Ext.view.Table").ToList();
        var protoNode = tsNodes.OfType<TypeScriptNamespace>().FirstOrDefault(n => n.Name == "Ext.view");
        var staticNode = tsNodes.OfType<TypeScriptNamespace>().FirstOrDefault(n => n.Name == "Ext.view.Table");
        
        Assert.Multiple(() =>
        {
            Assert.That(protoNode, Is.Not.Null);
            Assert.That(staticNode, Is.Not.Null);
        });

        var protoClassNode = protoNode.Body.OfType<TypeScriptClass>().FirstOrDefault();
        Assert.Multiple(() =>
        {
            Assert.That(protoClassNode, Is.Not.Null);
            Assert.That(protoClassNode.Name, Is.EqualTo("Table"));
            Assert.That(protoClassNode.Extends, Is.EqualTo("Ext.view.View"));
        });
    }
    
    [Test]
    public void TestTypedProp()
    {
        var tsNodes = DefinitionGenerator.GenerateDefinition("Ext.view.Table").ToList();
        var protoNode = tsNodes.OfType<TypeScriptNamespace>().FirstOrDefault(n => n.Name == "Ext.view");
        var protoClassNode = protoNode.Body.OfType<TypeScriptClass>().FirstOrDefault();
        
        Assert.Multiple(() =>
        {
            var isTableViewProp = protoClassNode.Members.OfType<TypeScriptObjectProperty>().FirstOrDefault(x => x.Name == "isTableView");
            Assert.That(isTableViewProp, Is.Not.Null);
            switch (isTableViewProp.Type)
            {
                case TypeScriptSimpleType { Name: "boolean" }:
                    break;
                default:
                    Assert.Fail($"Expected simple bool type, got {isTableViewProp.Type}");
                    break;
            }
        });
    }
    
    [Test]
    public void TestConfigAccessors()
    {
        var tsNodes = DefinitionGenerator.GenerateDefinition("Ext.view.Table").ToList();
        var protoNode = tsNodes.OfType<TypeScriptNamespace>().FirstOrDefault(n => n.Name == "Ext.view");
        var protoClassNode = protoNode.Body.OfType<TypeScriptClass>().FirstOrDefault();
        
        Assert.Multiple(() =>
        {
            var getter = protoClassNode.Members.OfType<TypeScriptMethod>().FirstOrDefault(x => x.Name == "getSelectionModel");
            Assert.That(getter, Is.Not.Null);
            Assert.That(getter.Parameters, Is.Empty);
            switch (getter.ReturnType)
            {
                case TypeScriptSimpleType { Name: "object" }:
                    break;
                default:
                    Assert.Fail($"Expected simple any return type, got {getter.ReturnType}");
                    break;
            }
        });
        
        Assert.Multiple(() =>
        {
            var setter = protoClassNode.Members.OfType<TypeScriptMethod>().FirstOrDefault(x => x.Name == "setSelectionModel");
            Assert.That(setter, Is.Not.Null);
            Assert.That(setter.Parameters, Is.Not.Empty);
            Assert.That(setter.Parameters[0].Name, Is.EqualTo("value"));
            switch (setter.Parameters[0].Type)
            {
                case TypeScriptSimpleType { Name: "object" }:
                    break;
                default:
                    Assert.Fail($"Expected simple object param type, got {setter.ReturnType}");
                    break;
            }
            switch (setter.ReturnType)
            {
                case TypeScriptSimpleType { Name: "void" }:
                    break;
                default:
                    Assert.Fail($"Expected simple void return type, got {setter.ReturnType}");
                    break;
            }
        });
    }
    
    [Test]
    public void TestTypedMethodWithUnionParam()
    {
        var tsNodes = DefinitionGenerator.GenerateDefinition("Ext.view.Table").ToList();
        var protoNode = tsNodes.OfType<TypeScriptNamespace>().FirstOrDefault(n => n.Name == "Ext.view");
        var protoClassNode = protoNode.Body.OfType<TypeScriptClass>().FirstOrDefault();
        
        Assert.Multiple(() =>
        {
            var method = protoClassNode.Members.OfType<TypeScriptMethod>().FirstOrDefault(x => x.Name == "getCell");
            Assert.That(method, Is.Not.Null);
            Assert.That(method.Parameters, Is.Not.Empty);

            var columnParam = method.Parameters[1];
            Assert.That(columnParam.Name, Is.EqualTo("column"));
            switch (method.Parameters[1].Type)
            {
                case TypeScriptUnionType { Left: TypeScriptSimpleType { Name: "Ext.grid.column.Column" }, Right: TypeScriptSimpleType { Name: "number" } }:
                    break;
                default:
                    Assert.Fail($"Expected Ext.grid.column.Column | number param type, got {method.ReturnType}");
                    break;
            }
        });
    }
    
    [Test]
    public void TestTypedMethodWithArrayParam()
    {
        var tsNodes = DefinitionGenerator.GenerateDefinition("Ext.view.Table").ToList();
        var protoNode = tsNodes.OfType<TypeScriptNamespace>().FirstOrDefault(n => n.Name == "Ext.view");
        var protoClassNode = protoNode.Body.OfType<TypeScriptClass>().FirstOrDefault();
        
        Assert.Multiple(() =>
        {
            var method = protoClassNode.Members.OfType<TypeScriptMethod>().FirstOrDefault(x => x.Name == "renderRow");
            Assert.That(method, Is.Not.Null);
            Assert.That(method.Parameters, Is.Not.Empty);

            var columnParam = method.Parameters[2];
            Assert.That(columnParam.Name, Is.EqualTo("out"));
            Assert.That(columnParam.Optional, Is.True);
            switch (method.Parameters[2].Type)
            {
                case TypeScriptGenericType { Name: "globalThis.Array", Parameters: var parameters }:
                    Assert.That(parameters, Has.Count.EqualTo(1));
                    switch (parameters[0])
                    {
                        case TypeScriptSimpleType { Name: "string" }:
                            break;
                        default:
                            Assert.Fail($"Expected string inner type, got {method.Parameters[2].Type}");
                            break;
                    }
                    break;
                default:
                    Assert.Fail($"Expected Array<string> param type, got {method.Parameters[2].Type}");
                    break;
            }
        });
    }
}