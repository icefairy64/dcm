﻿namespace Breezy.Dcm.Tests;

public sealed class TempDirectory : IDisposable
{
    public DirectoryInfo Directory { get; }

    public string FullPath => Directory.FullName;
    
    public TempDirectory()
    {
        var tempDir = Path.GetTempPath();
        Directory = System.IO.Directory.CreateTempSubdirectory("dcm-");
    }

    public void Dispose()
    {
        //Directory.Delete(true);
    }
}