﻿using Breezy.Dcm.Core;
using Breezy.Dcm.Core.Classes;
using Microsoft.Extensions.Logging;

namespace Breezy.Dcm.Tests;

public class StructureVisitorAction : AbstractJsBuildAction<BuildConfiguration>
{
    public StructureVisitorAction(BuildConfiguration configuration, ILogger logger, ILoggerFactory loggerFactory)
        : base(configuration, logger, loggerFactory)
    {
    }
    
    protected override IReadOnlyList<DefineVisitor> ExamineDefineVisitors
    {
        get
        {
            var visitors = new List<DefineVisitor>(base.ExamineDefineVisitors);
            visitors.Insert(0, new ExtClassDefineStructureVisitor());
            return visitors;
        }
    }

    public async Task Init()
    {
        await ExamineClasses();
    }

    public IClassDeclaration? GetClass(string name)
    {
        return ClassExamineVisitor.Classes[name];
    }
    
    public ExtClassDefineDeclaration GetDefineClass(string name)
    {
        return ClassExamineVisitor.Classes[name] as ExtClassDefineDeclaration ?? throw new Exception("Not a define class");
    }

    public ClassExamineVisitor ExamineVisitor => ClassExamineVisitor;
}