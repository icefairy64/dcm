﻿using Breezy.Dcm.Core;
using Breezy.Dcm.Core.Classes;
using Microsoft.Extensions.Logging.Abstractions;

namespace Breezy.Dcm.Tests;

public class ClassStructureVisitorTests
{
    private StructureVisitorAction Action = null!;

    [OneTimeSetUp]
    public async Task InitAction()
    {
        var config = new BuildConfiguration();
        await config.DiscoverPackages(new List<DirectoryInfo>() { ExtPaths.ExtPackagesDirectory, ExtPaths.ExtClassicDirectory },
            new List<string> { "classic" },
            "classic", null);
        
        Action = new StructureVisitorAction(config, NullLogger.Instance, NullLoggerFactory.Instance);
        await Action.Init();
    }
    
    [Test]
    public void TestTableViewProp()
    {
        var cls = Action.GetDefineClass("Ext.view.Table");

        var isTableViewProp = cls.Properties.FirstOrDefault(p => p.Name == "isTableView");
        
        Assert.Multiple(() =>
        {
            Assert.That(isTableViewProp, Is.Not.Null);
            Assert.That(isTableViewProp.OwnerLevel, Is.EqualTo(ExtClassMemberOwner.Instance));
            Assert.That(isTableViewProp.JsDoc, Is.Not.Null);
            Assert.That(isTableViewProp.Type, Is.EqualTo("Boolean"));
        });
    }
    
    [Test]
    public void TestTableViewInheritableStaticProp()
    {
        var cls = Action.GetDefineClass("Ext.view.Table");

        var normalSideEventsProp = cls.Properties.FirstOrDefault(p => p.Name == "normalSideEvents");
        
        Assert.Multiple(() =>
        {
            Assert.That(normalSideEventsProp, Is.Not.Null);
            Assert.That(normalSideEventsProp.OwnerLevel, Is.EqualTo(ExtClassMemberOwner.InheritableStatic));
        });
    }
    
    [Test]
    public void TestTableViewMethod()
    {
        var cls = Action.GetDefineClass("Ext.view.Table");

        var method = cls.Methods.FirstOrDefault(p => p.Name == "moveColumn");
        
        Assert.Multiple(() =>
        {
            Assert.That(method, Is.Not.Null);
            Assert.That(method.OwnerLevel, Is.EqualTo(ExtClassMemberOwner.Instance));
        });
        
        Assert.Multiple(() =>
        {
            var fromIdxParam = method.Parameters[0];
            Assert.That(fromIdxParam.Name, Is.EqualTo("fromIdx"));
            Assert.That(fromIdxParam.Type, Is.EqualTo("Number"));
        });
        
        Assert.Multiple(() =>
        {
            var toIdxParam = method.Parameters[1];
            Assert.That(toIdxParam.Name, Is.EqualTo("toIdx"));
            Assert.That(toIdxParam.Type, Is.EqualTo("Number"));
        });
        
        Assert.Multiple(() =>
        {
            var colsToMove = method.Parameters[2];
            Assert.That(colsToMove.Name, Is.EqualTo("colsToMove"));
            Assert.That(colsToMove.Type, Is.EqualTo("Number"));
            Assert.That(colsToMove.Optional, Is.True);
            Assert.That(colsToMove.DefaultValue, Is.EqualTo("1"));
        });
    }
    
    [Test]
    public void TestTableViewMethodReturns()
    {
        var cls = Action.GetDefineClass("Ext.view.Table");

        var method = cls.Methods.FirstOrDefault(p => p.Name == "getFeature");
        
        Assert.Multiple(() =>
        {
            Assert.That(method, Is.Not.Null);
            Assert.That(method.OwnerLevel, Is.EqualTo(ExtClassMemberOwner.Instance));
            Assert.That(method.ReturnType, Is.EqualTo("Ext.grid.feature.Feature"));
        });
    }
    
    [Test]
    public void TestTableViewConfig()
    {
        var cls = Action.GetDefineClass("Ext.view.Table");

        var config = cls.Configs.FirstOrDefault(p => p.Name == "selectionModel");
        
        Assert.That(config, Is.Not.Null);
    }
}