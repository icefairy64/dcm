using System.Text;
using Breezy.Dcm.Core;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Playwright;
using Microsoft.Playwright.NUnit;

namespace Breezy.Dcm.Tests;

[TestFixture]
public class BuildApplicationTests : PageTest
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public async Task TestBasicApplicationNoUi()
    {
        using var appDir = new TempDirectory();
        
        var appManifest = """
{
    "name": "Sample",
    "framework": "ext",
    "classpath": [
        
    ],
    "sass": {
        "namespace": "Sample",
        "src": [
        ],
        "var": [
        ],
        "etc": [
        ]
    },
    "js": [        
        {
            "path": "app.js",
            "bundle": true
        }
    ],
    "requires": [
        "font-awesome",
        "ux"
    ],
    "classic": {
        "js": [
            {
                "path": "../classic/shared/examples.js",
                "includeInBundle": true
            }
        ],
        "locales": [
            "en"
        ],
        "requires": [
            "locale",
            "font-ext"
        ]
    },
    "builds": {
        "classic": {
            "toolkit": "classic",
            "theme": "theme-classic"
        }
    }
}
""";

        var appJs = """
Ext.application({
    extend: 'Ext.app.Application',
    name: 'Sample',
    launch: function () {
        console.log('app launched');
    }
});
""";

        var html = """
<html>
<head>
<script src="bundle.js"></script>
</head>
<body>
</body>
</html>
""";
        
        await File.WriteAllTextAsync(Path.Join(appDir.FullPath, "app.json"), appManifest);
        await File.WriteAllTextAsync(Path.Join(appDir.FullPath, "app.js"), appJs);
        await File.WriteAllTextAsync(Path.Join(appDir.FullPath, "index.html"), html);

        var builder = new StringBuilder();
        await using var writer = new StringWriter(builder);

        var dcm = DcmBuild.Application(new NullLogger<DcmBuild>(), new NullLoggerFactory());
        await dcm.Build(new DirectoryInfo[] { ExtPaths.ExtClassicDirectory, ExtPaths.ExtPackagesDirectory }, appDir.Directory, "production", "classic", writer);

        var bundleJs = builder.ToString();
        await File.WriteAllTextAsync(Path.Join(appDir.FullPath, "bundle.js"), bundleJs);

        await Page.GotoAsync(new Uri(Path.Join(appDir.FullPath, "index.html")).AbsoluteUri);
        await Page.WaitForConsoleMessageAsync(new PageWaitForConsoleMessageOptions
        {
            Predicate = msg => msg.Text == "app launched"
        });
        
        Assert.Pass();
    }
    
    [Test]
    public async Task TestBasicApplicationNoUiStrict()
    {
        using var appDir = new TempDirectory();
        
        var appManifest = """
{
    "name": "Sample",
    "framework": "ext",
    "classpath": [
        
    ],
    "sass": {
        "namespace": "Sample",
        "src": [
        ],
        "var": [
        ],
        "etc": [
        ]
    },
    "js": [        
        {
            "path": "app.js",
            "bundle": true
        }
    ],
    "requires": [
        "font-awesome",
        "ux"
    ],
    "classic": {
        "js": [
            {
                "path": "../classic/shared/examples.js",
                "includeInBundle": true
            }
        ],
        "locales": [
            "en"
        ],
        "requires": [
            "locale",
            "font-ext"
        ]
    },
    "builds": {
        "classic": {
            "toolkit": "classic",
            "theme": "theme-classic"
        }
    }
}
""";

        var appJs = """
Ext.application({
    extend: 'Ext.app.Application',
    name: 'Sample',
    launch: function () {
        console.log('app launched');
    }
});
""";

        var html = """
<html>
<head>
<script src="bundle.js"></script>
</head>
<body>
</body>
</html>
""";
        
        await File.WriteAllTextAsync(Path.Join(appDir.FullPath, "app.json"), appManifest);
        await File.WriteAllTextAsync(Path.Join(appDir.FullPath, "app.js"), appJs);
        await File.WriteAllTextAsync(Path.Join(appDir.FullPath, "index.html"), html);

        var builder = new StringBuilder();
        await using var writer = new StringWriter(builder);

        await writer.WriteLineAsync("\"use strict\";");

        var dcm = DcmBuild.Application(new NullLogger<DcmBuild>(), new NullLoggerFactory());
        await dcm.Build(new DirectoryInfo[] { ExtPaths.ExtClassicDirectory, ExtPaths.ExtPackagesDirectory }, appDir.Directory, "production", "classic", writer);

        var bundleJs = builder.ToString();
        await File.WriteAllTextAsync(Path.Join(appDir.FullPath, "bundle.js"), bundleJs);

        Page.Console += (sender, message) =>
        {
            Console.WriteLine($"{message.Type}: {message.Text}");
        };

        await Page.GotoAsync(new Uri(Path.Join(appDir.FullPath, "index.html")).AbsoluteUri);
        await Page.WaitForConsoleMessageAsync(new PageWaitForConsoleMessageOptions
        {
            Predicate = msg => msg.Text == "app launched"
        });
        
        Assert.Pass();
    }
}