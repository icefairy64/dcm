﻿namespace Breezy.Dcm.Tests;

public static class ExtPaths
{
    public static readonly DirectoryInfo ExtDistDirectory = new DirectoryInfo("ext-js-gpl");
    public static readonly DirectoryInfo ExtClassicDirectory = new DirectoryInfo(Path.Join("ext-js-gpl", "classic"));
    public static readonly DirectoryInfo ExtModernDirectory = new DirectoryInfo(Path.Join("ext-js-gpl", "modern"));
    public static readonly DirectoryInfo ExtPackagesDirectory = new DirectoryInfo(Path.Join("ext-js-gpl", "packages"));
}