using System.Globalization;
using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Explorer;

public class AstNodeDescription
{
    public required string Name { get; init; }
    public IReadOnlyDictionary<string, object>? Properties { get; init; }
    public IReadOnlyList<AstNodeDescription>? Children { get; init; }

    public static AstNodeDescription FromSyntaxElement(ISyntaxElement element)
    {
        return element switch
        {
            Identifier id => FromIdentifier(id),
            
            StringLiteral sl => FromStringLiteral(sl),
            DimensionLiteral dl => FromDimensionLiteral(dl),
            NumberLiteral nl => FromNumberLiteral(nl),
            PercentageLiteral pl => FromPercentageLiteral(pl),
            BooleanLiteral bl => FromBooleanLiteral(bl),
            NullLiteral => NullLiteralDescription,
            NoneLiteral => NoneLiteralDescription,
            
            UnaryExpression ue => FromUnaryExpression(ue),
            BinaryExpression be => FromBinaryExpression(be),
            
            Argument arg => FromArgument(arg),
            CallArgument arg => FromCallArgument(arg),
            
            FunctionDeclaration fd => FromFunctionDeclaration(fd),
            MixinDeclaration md => FromMixinDeclaration(md),
            
            ClassSelector cs => FromClassSelector(cs),
            TypeSelector ts => FromTypeSelector(ts),
            IdSelector iSel => FromIdSelector(iSel),
            NestingSelector => NestingSelectorDescription,
            UniversalSelector => UniversalSelectorDescription,
            AttributeSelector ats => FromAttributeSelector(ats),
            
            PseudoClassSelector pcs => FromPseudoClassSelector(pcs),
            PseudoElementSelector pel => FromPseudoElementSelector(pel),
            
            CompoundSelector cs => FromList(cs),
            ChildSelector cs => FromChildSelector(cs),
            DescendantSelector ds => FromDescendantSelector(ds),
            NextSiblingSelector ns => FromNextSiblingSelector(ns),
            SubsequentSiblingSelector ss => FromSubsequentSiblingSelector(ss),
            
            RuleDeclaration rd => FromRuleDeclaration(rd),
            
            VariableDeclaration vd => FromVariableDeclaration(vd),
            PropertyDeclaration pd => FromPropertyDeclaration(pd),
            
            Node node => FromList(node),
            
            _ => FromUnknown(element)
        };
    }

    private static AstNodeDescription FromPropertyDeclaration(PropertyDeclaration pd)
    {
        return new AstNodeDescription
        {
            Name = "VariableDeclaration",
            Properties = new Dictionary<string, object>()
            {
                { "Name", FromIdentifier(pd.Property) },
                { "Expression", FromSyntaxElement(pd.Expression) },
                { "Important", pd.Important }
            }
        };
    }

    private static AstNodeDescription FromVariableDeclaration(VariableDeclaration vd)
    {
        return new AstNodeDescription
        {
            Name = "VariableDeclaration",
            Properties = new Dictionary<string, object>()
            {
                { "Name", FromSyntaxElement(vd.Variable) },
                { "Expression", FromSyntaxElement(vd.Expression) },
                { "Default", vd.Default },
                { "Global", vd.Global }
            }
        };
    }

    private static AstNodeDescription FromRuleDeclaration(RuleDeclaration rd)
    {
        return new AstNodeDescription
        {
            Name = "RuleDeclaration",
            Properties = new Dictionary<string, object>()
            {
                { "Selector", FromSyntaxElement(rd.Selector) },
                { "Body", FromList(rd.Body) }
            }
        };
    }

    private static AstNodeDescription FromSubsequentSiblingSelector(SubsequentSiblingSelector ss)
    { 
        return new AstNodeDescription
        {
            Name = "SubsequentSiblingSelector",
            Properties = new Dictionary<string, object>()
            {
                { "First", FromSyntaxElement(ss.First) },
                { "Next", FromSyntaxElement(ss.Next) }
            }
        };
    }

    private static AstNodeDescription FromNextSiblingSelector(NextSiblingSelector ns)
    {
        return new AstNodeDescription
        {
            Name = "NextSiblingSelector",
            Properties = new Dictionary<string, object>()
            {
                { "First", FromSyntaxElement(ns.First) },
                { "Next", FromSyntaxElement(ns.Next) }
            }
        };
    }

    private static AstNodeDescription FromDescendantSelector(DescendantSelector ds)
    {
        return new AstNodeDescription
        {
            Name = "DescendantSelector",
            Properties = new Dictionary<string, object>()
            {
                { "Parent", FromSyntaxElement(ds.Parent) },
                { "Child", FromSyntaxElement(ds.Child) }
            }
        };
    }

    private static AstNodeDescription FromChildSelector(ChildSelector cs)
    {
        return new AstNodeDescription
        {
            Name = "ChildSelector",
            Properties = new Dictionary<string, object>()
            {
                { "Parent", FromSyntaxElement(cs.Parent) },
                { "Child", FromSyntaxElement(cs.Child) }
            }
        };
    }

    private static AstNodeDescription FromPseudoElementSelector(PseudoElementSelector pel)
    {
        return new AstNodeDescription
        {
            Name = "PseudoElementSelector",
            Properties = new Dictionary<string, object>()
            {
                { "Name", pel.Name }
            }
        };
    }

    private static AstNodeDescription FromPseudoClassSelector(PseudoClassSelector pcs)
    {
        return new AstNodeDescription
        {
            Name = "PseudoClassSelector",
            Properties = new Dictionary<string, object>()
            {
                { "Name", pcs.Name }
            }
        };
    }

    private static AstNodeDescription FromAttributeSelector(AttributeSelector ats)
    {
        return new AstNodeDescription
        {
            Name = "AttributeSelector",
            Properties = new Dictionary<string, object>()
            {
                { "AttributeName", ats.AttributeName },
                { "Value", ats.Value is null ? NullLiteralDescription : ats.Value },
                { "Operator", ats.Operator is null ? NullLiteralDescription : ats.Operator },
                { "CaseSensitive", ats.CaseSensitive is null ? NullLiteralDescription : ats.CaseSensitive.Value }
            }
        };
    }

    private static AstNodeDescription FromIdSelector(IdSelector iSel)
    {
        return new AstNodeDescription
        {
            Name = "IdSelector",
            Properties = new Dictionary<string, object>()
            {
                { "Id", iSel.ElementId }
            }
        };
    }

    private static AstNodeDescription FromTypeSelector(TypeSelector ts)
    {
        return new AstNodeDescription
        {
            Name = "TypeSelector",
            Properties = new Dictionary<string, object>()
            {
                { "Tag", ts.TagName }
            }
        };
    }

    private static AstNodeDescription FromClassSelector(ClassSelector cs)
    {
        return new AstNodeDescription
        {
            Name = "ClassSelector",
            Properties = new Dictionary<string, object>()
            {
                { "Class", cs.ClassName }
            }
        };
    }

    private static AstNodeDescription FromMixinDeclaration(MixinDeclaration md)
    {
        return new AstNodeDescription
        {
            Name = "MixinDeclaration",
            Properties = new Dictionary<string, object>()
            {
                { "Name", FromIdentifier(md.Name) },
                { "Arguments", md.Arguments is null ? NullLiteralDescription : FromList(md.Arguments) },
                { "Body", FromList(md.Body) }
            }
        };
    }

    private static AstNodeDescription FromFunctionDeclaration(FunctionDeclaration fd)
    {
        return new AstNodeDescription
        {
            Name = "FunctionDeclaration",
            Properties = new Dictionary<string, object>()
            {
                { "Name", FromIdentifier(fd.Name) },
                { "Arguments", FromList(fd.Arguments) },
                { "Body", FromList(fd.Body) }
            }
        };
    }

    private static AstNodeDescription FromList(Node node)
    {
        return new AstNodeDescription
        {
            Name = node.GetType().Name,
            Children = node.Children.Select(FromSyntaxElement).ToList()
        };
    }

    private static AstNodeDescription FromCallArgument(CallArgument callArgument)
    {
        return new AstNodeDescription
        {
            Name = "CallArgument",
            Properties = new Dictionary<string, object>
            {
                { "Name", (object?)callArgument.Name ?? NullLiteralDescription },
                { "Value", FromSyntaxElement(callArgument.Value) }
            }
        };
    }

    private static AstNodeDescription FromArgument(Argument argument)
    {
        return new AstNodeDescription
        {
            Name = "Argument",
            Properties = new Dictionary<string, object>
            {
                { "Name", argument.Name },
                { "DefaultValue", argument.DefaultValue is null ? NullLiteralDescription : FromSyntaxElement(argument.DefaultValue) }
            }
        };
    }

    private static AstNodeDescription FromBinaryExpression(BinaryExpression be)
    {
        return new AstNodeDescription
        {
            Name = "BinaryExpression",
            Properties = new Dictionary<string, object>
            {
                { "Operator", be.Operator },
                { "Left", FromSyntaxElement(be.Left) },
                { "Right", FromSyntaxElement(be.Right) }
            }
        };
    }

    private static AstNodeDescription FromUnaryExpression(UnaryExpression ue)
    {
        return new AstNodeDescription
        {
            Name = "UnaryExpression",
            Properties = new Dictionary<string, object>
            {
                { "Operator", ue.Operator },
                { "Operand", FromSyntaxElement(ue.Operand) }
            }
        };
    }

    private static AstNodeDescription FromBooleanLiteral(BooleanLiteral bl)
    {
        return new AstNodeDescription
        {
            Name = "BooleanLiteral",
            Properties = new Dictionary<string, object>
            {
                { "Value", bl.Value }
            }
        };
    }

    private static AstNodeDescription FromDimensionLiteral(DimensionLiteral dl)
    {
        return new AstNodeDescription
        {
            Name = "DimensionLiteral",
            Properties = new Dictionary<string, object>
            {
                { "Value", dl.Value },
                { "Integer", dl.Integer },
                { "Unit", dl.Unit }
            }
        };
    }

    private static AstNodeDescription FromPercentageLiteral(PercentageLiteral pl)
    {
        return new AstNodeDescription
        {
            Name = "PercentageLiteral",
            Properties = new Dictionary<string, object>
            {
                { "Value", pl.Value }
            }
        };
    }

    private static AstNodeDescription FromUnknown(ISyntaxElement element)
    {
        return new AstNodeDescription
        {
            Name = element.GetType().Name
        };
    }

    private static AstNodeDescription FromNumberLiteral(NumberLiteral nl)
    {
        return new AstNodeDescription
        {
            Name = "NumberLiteral",
            Properties = new Dictionary<string, object>
            {
                { "Value", nl.Value },
                { "Integer", nl.Integer.ToString() }
            }
        };
    }

    private static AstNodeDescription FromStringLiteral(StringLiteral sl)
    {
        return new AstNodeDescription
        {
            Name = "StringLiteral",
            Properties = new Dictionary<string, object> { { "Value", sl.Value } }
        };
    }

    private static AstNodeDescription FromIdentifier(Identifier id)
    {
        return new AstNodeDescription
        {
            Name = "Identifier",
            Properties = new Dictionary<string, object> { { "Name", id.Name } }
        };
    }

    private static readonly AstNodeDescription NullLiteralDescription = new AstNodeDescription
    {
        Name = "NullLiteral"
    };
    
    private static readonly AstNodeDescription NoneLiteralDescription = new AstNodeDescription
    {
        Name = "NoneLiteral"
    };
    
    private static readonly AstNodeDescription NestingSelectorDescription = new AstNodeDescription
    {
        Name = "NestingSelector"
    };
    
    private static readonly AstNodeDescription UniversalSelectorDescription = new AstNodeDescription
    {
        Name = "UniversalSelector"
    };
}