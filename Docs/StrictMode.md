﻿# Strict mode support

Strict mode support is a necessary step to be able to transform Ext code to ESM modules, which would greatly improve general tooling support.

The following changes are made to make Ext code strict mode compliant:

- Writes to properties of globals like `Number` are replaced with no-ops
- Writes to names of functions are replaced with no-ops
- Accesses to `global` through `this` are fixed
- [All `callParent` / `callSuper` calls are inlined](CallParentInlining.md)