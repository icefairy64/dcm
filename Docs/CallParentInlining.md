﻿# `callParent` inlining

Inlining (or eliminating in other way) `callParent` / `callSuper` calls is an transformation that looks relatively simple at first, however it might be tricky to perform in practice as there are several cases that require either some heavy build-time analysis or keeping (parts of) resolution logic to run-time.

## Motivation
- [Strict mode support](StrictMode.md)
- Feature parity with existing tooling

## Cases
- From method definition of child class (static)
- From method definition of class override (static)
- From general function when applying a mixin (see `Ext.Mixin`) (dynamic-ish)
- From singleton class method definition (static)
- From private method definition (static)
- From method definition when method overrides a config accessor (static-ish)

## Current approaches
- Capturing previous method references from `Ext.define` config IIFE
- Runtime `$previous` / `$owner.superclass` walking logic