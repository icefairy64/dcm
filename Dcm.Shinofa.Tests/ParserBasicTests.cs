using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Tests;

[TestFixtureSource(typeof(ParserTestParams), nameof(ParserTestParams.ParseModeParams))]
public class ParserBasicTests
{
    private readonly ParseMode ParseMode;

    public ParserBasicTests(ParseMode parseMode)
    {
        ParseMode = parseMode;
    }
    
    [Test]
    public void TestFileImport()
    {
        const string scss = "@import 'file';";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsFileImport("file");
        });
    }
    
    [Test]
    public void TestUrlImport()
    {
        const string scss = "@import url(https://localhost/file.scss);";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsUrlImport("https://localhost/file.scss");
        });
    }
    
    [Test]
    public void TestDebugStatement([Values("debug", "warn", "error")] string keyword)
    {
        var scss = $"@{keyword} 'abc';";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            var action = (ElementSequenceAssertBuilder el) => el.IsStringLiteral("abc");
            switch (keyword)
            {
                case "debug":
                    el.IsDebugStatement(action);
                    break;
                case "warn":
                    el.IsWarnStatement(action);
                    break;
                case "error":
                    el.IsErrorStatement(action);
                    break;
            }
        });
    }

    [Test]
    public void TestSimpleCallExpression()
    {
        var scss = "@debug fn(1);";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsDebugStatement(item =>
            {
                item.IsCallExpression("fn", arg =>
                {
                    arg.IsCallArgument(val =>
                    {
                        val.IsNumberLiteral(1);
                    });
                });
            });
        });
    }
    
    [Test]
    public void TestNamespacedCallExpression()
    {
        var scss = "@debug list.fn(1);";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsDebugStatement(item =>
            {
                item.IsCallExpression("list.fn", arg =>
                {
                    arg.IsCallArgument(val =>
                    {
                        val.IsNumberLiteral(1);
                    });
                });
            });
        });
    }

    [Test]
    public void TestVariableDeclarationSingle()
    {
        const string scss = "$var: 5;";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("var", arg => arg.IsNumberLiteral(5));
        });
    }
    
    [Test]
    public void TestPropertyDeclarationSingle()
    {
        const string scss = "width: 0;";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsPropertyDeclaration("width", arg => arg.IsNumberLiteral(0));
        });
    }
    
    [Test]
    public void TestPropertyDeclarationList()
    {
        const string scss = "padding: 0 10 20 30;";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsPropertyDeclarationList("padding", arg =>
            {
                arg.IsNumberLiteral(0);
                arg.IsNumberLiteral(10);
                arg.IsNumberLiteral(20);
                arg.IsNumberLiteral(30);
            });
        });
    }
    
    [Test]
    public void TestComplexPropertyDeclarationList()
    {
        const string scss = "box-shadow: 20px 10px, 40px 30px;";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsPropertyDeclaration("box-shadow", arg =>
            {
                arg.IsExpressionList(',', nestedItem =>
                {
                    nestedItem.IsExpressionList(' ', item1 =>
                    {
                        item1.IsDimensionLiteral(20, "px");
                        item1.IsDimensionLiteral(10, "px");
                    });
                    nestedItem.IsExpressionList(' ', item2 =>
                    {
                        item2.IsDimensionLiteral(40, "px");
                        item2.IsDimensionLiteral(30, "px");
                    });
                });
            });
        });
    }
    
    [Test]
    public void TestComplexPropertyDeclarationListWithBinaryExpressions()
    {
        const string scss = "box-shadow: 20px + 10px, 40px - 30px;";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsPropertyDeclaration("box-shadow", arg =>
            {
                arg.IsExpressionList(',', nestedItem =>
                {
                    nestedItem.IsBinaryExpression("+", item1 =>
                    {
                        item1.IsDimensionLiteral(20, "px");
                        item1.IsDimensionLiteral(10, "px");
                    });
                    nestedItem.IsBinaryExpression("-", item2 =>
                    {
                        item2.IsDimensionLiteral(40, "px");
                        item2.IsDimensionLiteral(30, "px");
                    });
                });
            });
        });
    }
    
    [Test]
    public void TestMapDeclarationSimple()
    {
        const string scss = "$a: ('key1': 'value1', 5: 30px);";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("a", decl =>
            {
                decl.IsMapExpression(entry =>
                {
                    entry.IsMapEntryExpression(item =>
                    {
                        item.IsStringLiteral("key1");
                        item.IsStringLiteral("value1");
                    });
                    entry.IsMapEntryExpression(item =>
                    {
                        item.IsNumberLiteral(5);
                        item.IsDimensionLiteral(30, "px");
                    });
                });
            });
        });
    }
    
    [Test]
    public void TestMapDeclarationWithBinaryExpressions()
    {
        const string scss = "$a: ((40 - 5): 30px, 'key1': (20 / 10));";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("a", decl =>
            {
                decl.IsMapExpression(entry =>
                {
                    entry.IsMapEntryExpression(item =>
                    {
                        item.IsBinaryExpression("-", operand =>
                        {
                            operand.IsNumberLiteral(40);
                            operand.IsNumberLiteral(5);
                        });
                        item.IsDimensionLiteral(30, "px");
                    });
                    entry.IsMapEntryExpression(item =>
                    {
                        item.IsStringLiteral("key1");
                        item.IsBinaryExpression("/", operand =>
                        {
                            operand.IsNumberLiteral(20);
                            operand.IsNumberLiteral(10);
                        });
                    });
                });
            });
        });
    }
    
    [Test]
    public void TestBinaryExpression([Values("+", "-", "*", "/")] string op)
    {
        var scss = $"$prop: $a {op} $b;";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("prop", val =>
            {
                val.IsBinaryExpression(op, arg3 =>
                {
                    arg3.IsVariable("a");
                    arg3.IsVariable("b");
                });
            });
        });
    }
    
    [Test]
    public void TestUnaryExpression([Values("-", "+")] string op)
    {
        var scss = $"$prop: {op}$a;";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("prop", val =>
            {
                val.IsUnaryExpression(op, arg3 =>
                {
                    arg3.IsVariable("a");
                });
            });
        });
    }
    
    [Test]
    public void TestNestedParenthesisExpression()
    {
        const string scss = "$prop: (($a * 100) + ($b * 200)) / 1000;";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("prop", val =>
            {
                val.IsBinaryExpression("/", arg1 =>
                {
                    arg1.IsBinaryExpression("+", arg2 =>
                    {
                        arg2.IsBinaryExpression("*", arg3 =>
                        {
                            arg3.IsVariable("a");
                            arg3.IsNumberLiteral(100);
                        });
                        arg2.IsBinaryExpression("*", arg3 =>
                        {
                            arg3.IsVariable("b");
                            arg3.IsNumberLiteral(200);
                        });
                    });
                    arg1.IsNumberLiteral(1000);
                });
            });
        });
    }

    [Test]
    public void TestInclusiveForStatement()
    {
        const string scss = "@for $i from 1 through 3 { @debug $i; }";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsForStatement()
                .WithVariable(v =>
                {
                    v.IsVariable("i");
                })
                .WithInclusiveRange(rangeItem =>
                {
                    rangeItem.IsNumberLiteral(1);
                    rangeItem.IsNumberLiteral(3);
                })
                .WithBody(bodyItem =>
                {
                    bodyItem.IsDebugStatement(debugItem =>
                    {
                        debugItem.IsVariable("i");
                    });
                });
        });
    }
    
    [Test]
    public void TestExclusiveForStatement()
    {
        const string scss = "@for $i from 1 to 3 { @debug $i; }";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsForStatement()
                .WithVariable(v =>
                {
                    v.IsVariable("i");
                })
                .WithExclusiveRange(rangeItem =>
                {
                    rangeItem.IsNumberLiteral(1);
                    rangeItem.IsNumberLiteral(3);
                })
                .WithBody(bodyItem =>
                {
                    bodyItem.IsDebugStatement(debugItem =>
                    {
                        debugItem.IsVariable("i");
                    });
                });
        });
    }

    [Test]
    public void TestBasicEachStatement()
    {
        const string scss = "@each $item in $list { @debug $i; }";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els,
            el =>
            {
                el.IsEachStatement()
                    .WithVariable(v =>
                    {
                        v.IsVariable("item");
                    })
                    .WithExpression(expr =>
                    {
                        expr.IsVariable("list");
                    })
                    .WithBody(bodyItem =>
                    {
                        bodyItem.IsDebugStatement(debugItem =>
                        {
                            debugItem.IsVariable("i");
                        });
                    });
            });
    }
    
    [Test]
    public void TestEachStatementWithComplexExpression()
    {
        const string scss = "@each $item in list.join($a, $b) { @debug $item; }";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els,
            el =>
            {
                el.IsEachStatement()
                    .WithVariable(v =>
                    {
                        v.IsVariable("item");
                    })
                    .WithExpression(expr =>
                    {
                        expr.IsCallExpression("list.join", arg =>
                        {
                            arg.IsCallArgument(val => val.IsVariable("a"));
                            arg.IsCallArgument(val => val.IsVariable("b"));
                        });
                    })
                    .WithBody(bodyItem =>
                    {
                        bodyItem.IsDebugStatement(debugItem =>
                        {
                            debugItem.IsVariable("item");
                        });
                    });
            });
    }
    
    [Test]
    public void TestEachStatementWithDestructuring()
    {
        const string scss = "@each $item1, $item2, $item3 in $list { @debug $i; }";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els,
            el =>
            {
                el.IsEachStatement()
                    .WithVariable(v =>
                    {
                        v.IsVariable("item1");
                        v.IsVariable("item2");
                        v.IsVariable("item3");
                    })
                    .WithExpression(expr =>
                    {
                        expr.IsVariable("list");
                    })
                    .WithBody(bodyItem =>
                    {
                        bodyItem.IsDebugStatement(debugItem =>
                        {
                            debugItem.IsVariable("i");
                        });
                    });
            });
    }
    
    [Test]
    public void TestExtendStatement()
    {
        const string scss = "@extend .def;";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els,
            el =>
            {
                el.IsExtendStatement(selector =>
                {
                    selector.IsClassSelector("def");
                });
            });
    }

    [Test]
    public void TestFunctionCallNestedInArgumentList()
    {
        const string scss = "@include mixin(" +
                            "if(" +
                                "$a, " +
                                "top($b) right($b) 0 left($b), " +
                                "$b" +
                            "), " +
                            "$c);";
        
        var els = Utils.ParseScss(scss, ParseMode);
        Element.AssertSequence(els, el =>
        {
            el.IsIncludeStatement("mixin", arg =>
            {
                arg.IsCallArgument(v => v.IsCallExpression("if", arg2 =>
                {
                    arg2.IsCallArgument(val => val.IsVariable("a"));
                    arg2.IsCallArgument(val =>
                    {
                        val.IsExpressionList(' ', exp =>
                        {
                            exp.IsCallExpression("top", arg3 =>
                            {
                                arg3.IsCallArgument(val2 => val2.IsVariable("b"));
                            });
                            exp.IsCallExpression("right", arg3 =>
                            {
                                arg3.IsCallArgument(val2 => val2.IsVariable("b"));
                            });
                            exp.IsNumberLiteral(0);
                            exp.IsCallExpression("left", arg3 =>
                            {
                                arg3.IsCallArgument(val2 => val2.IsVariable("b"));
                            });
                        });
                    });
                    arg2.IsCallArgument(val => val.IsVariable("b"));
                }));
                arg.IsCallArgument(v => v.IsVariable("c"));
            });
        });
    }

    [Test]
    public void TestNestedExpressionLists()
    {
        const string scss = "$a: (1 'a' 'b') (2 'c' 'd');";

        var els = Utils.ParseScss(scss, ParseMode);
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("a", decl =>
            {
                decl.IsExpressionList(' ', list =>
                {
                    list.IsExpressionList(' ', subList =>
                    {
                        subList.IsNumberLiteral(1);
                        subList.IsStringLiteral("a");
                        subList.IsStringLiteral("b");
                    });
                    list.IsExpressionList(' ', subList =>
                    {
                        subList.IsNumberLiteral(2);
                        subList.IsStringLiteral("c");
                        subList.IsStringLiteral("d");
                    });
                });
            });
        });
    }
}