using System.Globalization;
using Breezy.Dcm.Shinofa.Tokenization;
using NUnit.Framework.Constraints;

namespace Breezy.Dcm.Shinofa.Tests;

public class TokenizerCssTests
{
    [Test]
    public void TestSingleClass()
    {
        const string sass = """
                            .myClass {
                              background: red;
                            }
                            """;

        var tokens = Tokenizer.Tokenize(sass);
        
        Token.AssertSequence(tokens, token =>
        {
            token.IsDelim('.');
            token.IsIdent("myClass");
            token.IsWhitespace(1);
            token.IsA<LeftCurlyBracketToken>();
            token.IsWhitespace(3);
            
            token.IsIdent("background");
            token.IsA<ColonToken>();
            token.IsWhitespace(1);
            token.IsIdent("red");
            token.IsA<SemicolonToken>();
            token.IsWhitespace(1);
            
            token.IsA<RightCurlyBracketToken>();
        });
    }
    
    [Test]
    public void TestHash()
    {
        const string sass = """
                            #myId {
                              background: red;
                            }
                            """;

        var tokens = Tokenizer.Tokenize(sass);
        
        Token.AssertSequence(tokens, token =>
        {
            token.IsIdHash("myId");
            token.IsWhitespace(1);
            token.IsA<LeftCurlyBracketToken>();
            token.IsWhitespace(3);
            
            token.IsIdent("background");
            token.IsA<ColonToken>();
            token.IsWhitespace(1);
            token.IsIdent("red");
            token.IsA<SemicolonToken>();
            token.IsWhitespace(1);
            
            token.IsA<RightCurlyBracketToken>();
        });
    }
    
    [Test]
    public void TestKeyframes()
    {
        const string sass = """
                            @keyframes my-anim {
                              from {
                                color: red;
                              }
                              to {
                                color: blue;
                              }
                            }
                            """;

        var tokens = Tokenizer.Tokenize(sass);
        
        Token.AssertSequence(tokens, token =>
        {
            token.IsAtKeyword("keyframes");
            token.IsWhitespace();
            token.IsIdent("my-anim");
            token.IsWhitespace();
            token.IsA<LeftCurlyBracketToken>();
            token.IsWhitespace();
            
            token.IsIdent("from");
            token.IsWhitespace();
            token.IsA<LeftCurlyBracketToken>();
            token.IsWhitespace();
            
            token.IsIdent("color");
            token.IsA<ColonToken>();
            token.IsWhitespace();
            token.IsIdent("red");
            token.IsA<SemicolonToken>();
            token.IsWhitespace();
            
            token.IsA<RightCurlyBracketToken>();
            token.IsWhitespace();
            
            token.IsIdent("to");
            token.IsWhitespace();
            token.IsA<LeftCurlyBracketToken>();
            token.IsWhitespace();
            
            token.IsIdent("color");
            token.IsA<ColonToken>();
            token.IsWhitespace();
            token.IsIdent("blue");
            token.IsA<SemicolonToken>();
            token.IsWhitespace();
            
            token.IsA<RightCurlyBracketToken>();
            token.IsWhitespace();
            
            token.IsA<RightCurlyBracketToken>();
        });
    }
    
    [Test]
    public void TestInteger([Values("0", "1", "1000000", "-230", "+450")] string value)
    {
        var tokens = Tokenizer.Tokenize(value);
        Token.AssertSequence(tokens, token =>
        {
            token.IsInteger(double.Parse(value));
        });
    }
    
    [Test]
    public void TestNumber([Values("0.0", "1.25", "1000000.00001", "-230.4", "+450.65", ".6", "-.6", "+.6")] string value)
    {
        var tokens = Tokenizer.Tokenize(value);
        Token.AssertSequence(tokens, token =>
        {
            token.IsNumber(double.Parse(value));
        });
    }
    
    [Test]
    public void TestNumberExp([Values("0.0e1", "1.25e-1", "1000000.00001e2", "-230.4e-2", "+450.65e3", ".6e-3", "-.6e4", "+.6e-4")] string value)
    {
        var tokens = Tokenizer.Tokenize(value);
        Token.AssertSequence(tokens, token =>
        {
            token.IsNumber(double.Parse(value));
        });
    }

    [Test]
    public void TestDimensionInt()
    {
        var tokens = Tokenizer.Tokenize("360deg");
        Token.AssertSequence(tokens, token =>
        {
            token.IsDimension(360d, "deg");
        });
    }
    
    [Test]
    public void TestDimensionNumber()
    {
        var tokens = Tokenizer.Tokenize("33.3deg");
        Token.AssertSequence(tokens, token =>
        {
            token.IsDimension(33.3d, "deg");
        });
    }
    
    [Test]
    public void TestPercentage()
    {
        var tokens = Tokenizer.Tokenize("120%");
        Token.AssertSequence(tokens, token =>
        {
            token.IsPercentage(120d);
        });
    }
    
    [Test]
    public void TestSingleQuotedString([Values("abc", "\"", "\\\'", "123\\\nbce")] string value)
    {
        var tokens = Tokenizer.Tokenize($"'{value}'");
        
        value = value.Replace("\\", "");
        
        Token.AssertSequence(tokens, token =>
        {
            token.IsString(value);
        });
    }
    
    [Test]
    public void TestDoubleQuotedString([Values("abc", "\\\"", "\'", "123\\\nbce")] string value)
    {
        var tokens = Tokenizer.Tokenize($"\"{value}\"");
        
        value = value.Replace("\\", "");
        
        Token.AssertSequence(tokens, token =>
        {
            token.IsString(value);
        });
    }

    [Test]
    public void TestCdoCdcPair()
    {
        var scss = "<!-- test -->";
        var tokens = Tokenizer.Tokenize(scss);
        Token.AssertSequence(tokens, token =>
        {
            token.IsA<CdoToken>();
            token.IsWhitespace();
            token.IsIdent("test");
            token.IsWhitespace();
            token.IsA<CdcToken>();
        });
    }
    
}