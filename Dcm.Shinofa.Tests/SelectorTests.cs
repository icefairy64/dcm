namespace Breezy.Dcm.Shinofa.Tests;

[TestFixtureSource(typeof(ParserTestParams), nameof(ParserTestParams.ParseModeParams))]
public class SelectorTests
{
    private readonly ParseMode ParseMode;

    public SelectorTests(ParseMode parseMode)
    {
        ParseMode = parseMode;
    }
    
    [Test]
    public void TestClassSelector()
    {
        const string scss = ".abc {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel => sel.IsClassSelector("abc"));
        });
    }
    
    [Test]
    public void TestIdSelector()
    {
        const string scss = "#id {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel => sel.IsIdSelector("id"));
        });
    }
    
    [Test]
    public void TestTypeSelector()
    {
        const string scss = "a {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel => sel.IsTypeSelector("a"));
        });
    }
    
    [Test]
    public void TestAttributeSelectorSimple()
    {
        const string scss = "[attr] {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel => sel.IsAttributeSelector("attr"));
        });
    }
    
    [Test]
    public void TestAttributeSelectorWithValue([Values("=", "~=", "|=", "^=", "$=", "*=")] string op)
    {
        var scss = "[attr" + op + "\"abc\"] {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel => sel.IsAttributeSelector("attr", op, "abc"));
        });
    }
    
    [Test]
    public void TestPseudoClassSelectorSimple()
    {
        const string scss = "a:hover {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsCompoundSelector(inner =>
                    {
                        inner.IsTypeSelector("a");
                        inner.IsPseudoClassSelector("hover");
                    });
                });
        });
    }
    
    [Test]
    public void TestPseudoClassSelectorWithBody()
    {
        const string scss = "a:not(.visited) {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsCompoundSelector(inner =>
                    {
                        inner.IsTypeSelector("a");
                        inner.IsPseudoClassSelector("not", body => body.IsClassSelector("visited"));
                    });
                });
        });
    }
    
    [Test]
    public void TestPseudoElementSelectorSimple()
    {
        const string scss = "a::before {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsCompoundSelector(inner =>
                    {
                        inner.IsTypeSelector("a");
                        inner.IsPseudoElementSelector("before");
                    });
                });
        });
    }
    
    [Test]
    public void TestPseudoElementSelectorWithBody()
    {
        const string scss = "a::slotted(span) {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsCompoundSelector(inner =>
                    {
                        inner.IsTypeSelector("a");
                        inner.IsPseudoElementSelector("slotted", body => body.IsTypeSelector("span"));
                    });
                });
        });
    }
    
    [Test]
    public void TestUniversalSelector()
    {
        const string scss = "* {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel => sel.IsUniversalSelector());
        });
    }
    
    [Test]
    public void TestNestingSelector()
    {
        const string scss = "& {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel => sel.IsNestingSelector());
        });
    }
    
    [Test]
    public void TestCompoundSelector()
    {
        const string scss = ".cls[attr] {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsCompoundSelector(inner =>
                    {
                        inner.IsClassSelector("cls");
                        inner.IsAttributeSelector("attr");
                    });
                });
        });
    }
    
    [Test]
    public void TestListSelector()
    {
        const string scss = ".cls, [attr] {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsListSelector(inner =>
                    {
                        inner.IsClassSelector("cls");
                        inner.IsAttributeSelector("attr");
                    });
                });
        });
    }
    
    [Test]
    public void TestChildSelector()
    {
        const string scss = ".cls > [attr] {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsChildSelector(inner =>
                    {
                        inner.IsClassSelector("cls");
                        inner.IsAttributeSelector("attr");
                    });
                });
        });
    }
    
    [Test]
    public void TestDescendantSelector()
    {
        const string scss = ".cls [attr] {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsDescendantSelector(inner =>
                    {
                        inner.IsClassSelector("cls");
                        inner.IsAttributeSelector("attr");
                    });
                });
        });
    }
    
    [Test]
    public void TestNextSiblingSelector()
    {
        const string scss = ".cls + [attr] {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsNextSiblingSelector(inner =>
                    {
                        inner.IsClassSelector("cls");
                        inner.IsAttributeSelector("attr");
                    });
                });
        });
    }
    
    [Test]
    public void TestSubsequentSiblingSelector()
    {
        const string scss = ".cls ~ [attr] {}";
        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsRuleDeclaration()
                .WithSelector(sel =>
                {
                    sel.IsSubsequentSiblingSelector(inner =>
                    {
                        inner.IsClassSelector("cls");
                        inner.IsAttributeSelector("attr");
                    });
                });
        });
    }
}