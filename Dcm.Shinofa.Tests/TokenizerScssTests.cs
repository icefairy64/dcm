using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Tests;

public class TokenizerScssTests
{
    [Test]
    public void TestSimpleVariableAssignment()
    {
        var scss = "$my-var: 360deg;";
        var tokens = Tokenizer.Tokenize(scss);
        Token.AssertSequence(tokens, token =>
        {
            token.IsDelim('$');
            token.IsIdent("my-var");
            token.IsA<ColonToken>();
            token.IsWhitespace();
            token.IsDimension(360d, "deg");
            token.IsA<SemicolonToken>();
        });
    }
    
    [Test]
    public void TestDynamicVariableAssignment()
    {
        var scss = "$my-var: dynamic(360deg);";
        var tokens = Tokenizer.Tokenize(scss);
        Token.AssertSequence(tokens, token =>
        {
            token.IsDelim('$');
            token.IsIdent("my-var");
            token.IsA<ColonToken>();
            token.IsWhitespace();
            token.IsFunction("dynamic");
            token.IsDimension(360d, "deg");
            token.IsA<RightParenthesisToken>();
            token.IsA<SemicolonToken>();
        });
    }
}