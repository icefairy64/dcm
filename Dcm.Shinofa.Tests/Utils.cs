using Breezy.Dcm.Shinofa.Parsing;
using Breezy.Dcm.Shinofa.Processing;

namespace Breezy.Dcm.Shinofa.Tests;

public static class Utils
{
    public static void AssertIs<T>(object value, Action<T> action)
    {
        Assert.That(value, Is.InstanceOf<T>());
        if (value is T typedValue)
            action(typedValue);
    }

    public static IList<ISyntaxElement> ParseScss(string source, ParseMode parseMode)
    {
        var els = Parser.Parse(source);
        
        if (parseMode == ParseMode.SassWriterReEmit)
        {
            using var writer = new StringWriter();
            var visitor = new SassWriterVisitor(writer);

            foreach (var el in els)
                visitor.Visit(el);

            try
            {
                els = Parser.Parse(writer.ToString());
            }
            catch (Exception e)
            {
                throw new Exception($"Failed to parse re-emitted SCSS: {writer.ToString()}", e);
            }
        }

        return els;
    }
}