using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Tests;

public sealed partial class ElementSequenceAssertBuilder
{
    public void IsClassSelector(string className)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<ClassSelector>(el, cs =>
            {
                Assert.That(cs.ClassName, Is.EqualTo(className));
            });
        });
    }
    
    public void IsIdSelector(string id)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<IdSelector>(el, ids =>
            {
                Assert.That(ids.ElementId, Is.EqualTo(id));
            });
        });
    }
    
    public void IsTypeSelector(string type)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<TypeSelector>(el, ts =>
            {
                Assert.That(ts.TagName, Is.EqualTo(type));
            });
        });
    }

    public void IsAttributeSelector(string attr)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<AttributeSelector>(el, ats =>
            {
                Assert.That(ats.AttributeName, Is.EqualTo(attr));
                Assert.That(ats.Operator, Is.Null);
                Assert.That(ats.Value, Is.Null);
            });
        });
    }

    public void IsAttributeSelector(string attr, string op, string val)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<AttributeSelector>(el, ats =>
            {
                Assert.That(ats.AttributeName, Is.EqualTo(attr));
                Assert.That(ats.Operator, Is.EqualTo(op));
                Assert.That(ats.Value, Is.EqualTo(val));
            });
        });
    }

    public void IsNestingSelector()
    {
        var el = Advance();
        Asserts.Add(() => { Utils.AssertIs<NestingSelector>(el, _ => { }); });
    }

    public void IsUniversalSelector()
    {
        var el = Advance();
        Asserts.Add(() => { Utils.AssertIs<UniversalSelector>(el, _ => { }); });
    }

    public void IsPseudoClassSelector(string name)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<PseudoClassSelector>(el, pcs => { Assert.That(pcs.Name, Is.EqualTo(name)); });
        });
    }

    public void IsPseudoClassSelector(string name, Action<ElementSequenceAssertBuilder> bodyAction)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<ComplexPseudoClassSelector>(el, pcs =>
            {
                Assert.That(pcs.Name, Is.EqualTo(name));
                var builder = new ElementSequenceAssertBuilder(new ISyntaxElement[] { pcs.Body });
                bodyAction.Invoke(builder);
                builder.DoAssert();
            });
        });
    }

    public void IsPseudoElementSelector(string name)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<PseudoElementSelector>(el, pes => { Assert.That(pes.Name, Is.EqualTo(name)); });
        });
    }

    public void IsPseudoElementSelector(string name, Action<ElementSequenceAssertBuilder> bodyAction)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<ComplexPseudoElementSelector>(el, pes =>
            {
                Assert.That(pes.Name, Is.EqualTo(name));
                var builder = new ElementSequenceAssertBuilder(new ISyntaxElement[] { pes.Body });
                bodyAction.Invoke(builder);
                builder.DoAssert();
            });
        });
    }

    public void IsCompoundSelector(Action<ElementSequenceAssertBuilder> selAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<CompoundSelector>(el, cs =>
        {
            var builder = new ElementSequenceAssertBuilder(cs.Children.ToList());
            selAction.Invoke(builder);
            builder.DoAssert();
        }));
    }

    public void IsListSelector(Action<ElementSequenceAssertBuilder> selAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<ListSelector>(el, cs =>
        {
            var builder = new ElementSequenceAssertBuilder(cs.Children.ToList());
            selAction.Invoke(builder);
            builder.DoAssert();
        }));
    }
    
    public void IsDescendantSelector(Action<ElementSequenceAssertBuilder> selAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<DescendantSelector>(el, ds =>
        {
            var builder = new ElementSequenceAssertBuilder(ds.Children.ToList());
            selAction.Invoke(builder);
            builder.DoAssert();
        }));
    }
    
    public void IsChildSelector(Action<ElementSequenceAssertBuilder> selAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<ChildSelector>(el, cs =>
        {
            var builder = new ElementSequenceAssertBuilder(cs.Children.ToList());
            selAction.Invoke(builder);
            builder.DoAssert();
        }));
    }
    
    public void IsNextSiblingSelector(Action<ElementSequenceAssertBuilder> selAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<NextSiblingSelector>(el, ns =>
        {
            var builder = new ElementSequenceAssertBuilder(ns.Children.ToList());
            selAction.Invoke(builder);
            builder.DoAssert();
        }));
    }
    
    public void IsSubsequentSiblingSelector(Action<ElementSequenceAssertBuilder> selAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<SubsequentSiblingSelector>(el, ds =>
        {
            var builder = new ElementSequenceAssertBuilder(ds.Children.ToList());
            selAction.Invoke(builder);
            builder.DoAssert();
        }));
    }
}