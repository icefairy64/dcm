using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Tests;

[TestFixtureSource(typeof(ParserTestParams), nameof(ParserTestParams.ParseModeParams))]
public class ParserMixinDeclarationTests
{
    private readonly ParseMode ParseMode;

    public ParserMixinDeclarationTests(ParseMode parseMode)
    {
        ParseMode = parseMode;
    }
    
    [Test]
    public void TestNoArgumentDeclaration()
    {
        const string scss = """
                            @mixin abc {
                                width: 0;
                            }
                            """;

        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsMixinDeclaration("abc")
                .WithoutArguments()
                .WithBody(bodyEl =>
                {
                    bodyEl.IsPropertyDeclaration("width", val => val.IsNumberLiteral(0));
                });
        });
    }
    
    [Test]
    public void TestSingleArgumentDeclaration()
    {
        const string scss = """
                            @mixin abc($a) {
                                width: $a;
                            }
                            """;

        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsMixinDeclaration("abc")
                .WithArguments(arg =>
                {
                    arg.IsArgument("a");
                })
                .WithBody(bodyEl =>
                {
                    bodyEl.IsPropertyDeclaration("width", val => val.IsVariable("a"));
                });
        });
    }
    
    [Test]
    public void TestMultipleArgumentDeclaration()
    {
        const string scss = """
                            @mixin abc($a, $b) {
                                width: $a;
                            }
                            """;

        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsMixinDeclaration("abc")
                .WithArguments(arg =>
                {
                    arg.IsArgument("a");
                    arg.IsArgument("b");
                })
                .WithBody(bodyEl =>
                {
                    bodyEl.IsPropertyDeclaration("width", val => val.IsVariable("a"));
                });
        });
    }
    
    [Test]
    public void TestSingleArgumentDeclarationWithDefaultValue()
    {
        const string scss = """
                            @mixin abc($a: 10) {
                                width: $a;
                            }
                            """;

        var els = Utils.ParseScss(scss, ParseMode);
        
        Element.AssertSequence(els, el =>
        {
            el.IsMixinDeclaration("abc")
                .WithArguments(arg =>
                {
                    arg.IsArgument("a", val => val.IsNumberLiteral(10));
                })
                .WithBody(bodyEl =>
                {
                    bodyEl.IsPropertyDeclaration("width", val => val.IsVariable("a"));
                });
        });
    }
}