using Breezy.Dcm.Shinofa.Parsing;
using Breezy.Dcm.Shinofa.Processing;

namespace Breezy.Dcm.Shinofa.Tests;

public class DynamicVariableTests
{
    [Test]
    public void TestBasicDeclaration()
    {
        const string scss = "$abc: dynamic(10);";

        var els = Process(scss);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("abc", init =>
            {
                init.IsNumberLiteral(10);
            });
        });
    }
    
    [Test]
    public void TestDeclarationWithDependencies()
    {
        const string scss = "$abc: dynamic($def); $def: 20;";

        var els = Process(scss);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("def", init =>
            {
                init.IsNumberLiteral(20);
            });
            
            el.IsVariableDeclaration("abc", init =>
            {
                init.IsVariable("def");
            });
        });
    }
    
    [Test]
    public void TestHoisting()
    {
        const string scss = "@debug 'a';\n$abc: dynamic(10);";

        var els = Process(scss);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("abc", init =>
            {
                init.IsNumberLiteral(10);
            });
            
            el.IsDebugStatement(el2 => el2.IsStringLiteral("a"));
        });
    }
    
    [Test]
    public void TestBody()
    {
        const string scss = "$abc: dynamic(10); @mixin def { color: $abc; }";

        var els = Process(scss);
        
        Element.AssertSequence(els, el =>
        {
            el.IsVariableDeclaration("abc", init =>
            {
                init.IsNumberLiteral(10);
            });

            el.IsMixinDeclaration("def")
                .WithoutArguments()
                .WithBody(bodyEl =>
                {
                    bodyEl.IsPropertyDeclaration("color", decl =>
                    {
                        decl.IsVariable("abc");
                    });
                });
        });
    }

    private IList<ISyntaxElement> Process(string scss)
    {
        var visitor = new DynamicVariableVisitor(new SyntaxElementVisitor());
        var els = Parser.Parse(scss);
        
        visitor.Start();
        foreach (var el in els)
            visitor.Visit(el);
        visitor.End();

        using var writer = new StringWriter();
        visitor.Delegate = new SassWriterVisitor(writer);
        visitor.Mode = DynamicVariableVisitorMode.Replace;
        
        visitor.Start();
        foreach (var el in els)
            visitor.Visit(el);
        visitor.End();

        return Parser.Parse(writer.ToString());
    }
}