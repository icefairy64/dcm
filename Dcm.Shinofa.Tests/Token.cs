using Breezy.Dcm.Shinofa.Tokenization;
using NUnit.Framework.Constraints;

namespace Breezy.Dcm.Shinofa.Tests;

public static class Token
{
    public static IResolveConstraint IsWhitespace(int length)
    {
        var a = new InstanceOfTypeConstraint(typeof(WhitespaceToken));
        var b = new PropertyConstraint("Length", new EqualConstraint(length));
        return new AndConstraint(a, b);
    }
    
    public static IResolveConstraint IsDelim(char value)
    {
        var a = new InstanceOfTypeConstraint(typeof(DelimToken));
        var b = new PropertyConstraint("Value", new EqualConstraint(value));
        return new AndConstraint(a, b);
    }
    
    public static IResolveConstraint IsIdent(string value)
    {
        var a = new ExactTypeConstraint(typeof(IdentToken));
        var b = new PropertyConstraint("Value", new EqualConstraint(value));
        return new AndConstraint(a, b);
    }
    
    public static IResolveConstraint IsIdHash(string value)
    {
        var a = new ExactTypeConstraint(typeof(HashToken));
        var b = new PropertyConstraint("Value", new EqualConstraint(value));
        var c = new PropertyConstraint("TypeFlag", new EqualConstraint("id"));
        return new AndConstraint(a, new AndConstraint(b, c));
    }

    public static IResolveConstraint MatchSequence(
        IEnumerable<IResolveConstraint> constraints)
    {
        IConstraint constraint = new PredicateConstraint<IEnumerable<ISassToken>>((_) => true);
        return constraints
            .Select((tokenConstraint, idx) => new IndexerConstraint(new object[] { idx }, tokenConstraint.Resolve()))
            .Aggregate(constraint, (current, a) => new AndConstraint(current, a));
    }

    public static void AssertSequence(IList<ISassToken> tokens, Action<TokenSequenceAssertBuilder> assert)
    {
        var builder = new TokenSequenceAssertBuilder(tokens);
        assert.Invoke(builder);
        builder.Assert();
    }
}

public sealed class TokenSequenceAssertBuilder
{
    private readonly IList<ISassToken> Tokens;
    private readonly IList<Action> Asserts = new List<Action>();
    private int CurrentToken;

    public TokenSequenceAssertBuilder(IList<ISassToken> tokens)
    {
        Tokens = tokens;
    }

    public void IsWhitespace(int length)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.InstanceOf<WhitespaceToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Has.Property("Length").EqualTo(length), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsWhitespace()
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.InstanceOf<WhitespaceToken>(), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsDelim(char value)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.InstanceOf<DelimToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as DelimToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsIdent(string value)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<IdentToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as IdentToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsIdHash(string value)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<HashToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as HashToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as HashToken, Has.Property("TypeFlag").EqualTo("id"), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsAtKeyword(string value)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<AtKeywordToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as AtKeywordToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsDimension(double value, string unit)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<DimensionToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as DimensionToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as DimensionToken, Has.Property("Unit").EqualTo(unit), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsNumber(double value)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<NumberToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as NumberToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsInteger(double value)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<NumberToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as NumberToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as NumberToken, Has.Property("Type").EqualTo(NumberType.Integer), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsPercentage(double value)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<PercentageToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as PercentageToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsString(string value)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<StringToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as StringToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsFunction(string value)
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<FunctionToken>(), $"Element {token} at position {CurrentToken}"));
        Asserts.Add(() => NUnit.Framework.Assert.That(token as FunctionToken, Has.Property("Value").EqualTo(value), $"Element {token} at position {CurrentToken}"));
    }
    
    public void IsA<T>() where T : ISassToken
    {
        var token = Tokens[CurrentToken++];
        Asserts.Add(() => NUnit.Framework.Assert.That(token, Is.TypeOf<T>(), $"Element {token} at position {CurrentToken}"));
    }

    public void Assert()
    {
        NUnit.Framework.Assert.Multiple(() =>
        {
            foreach (var assert in Asserts)
                assert.Invoke();
            while (CurrentToken < Tokens.Count)
                NUnit.Framework.Assert.Fail($"Unexpected token {Tokens[CurrentToken++]}");
        });
    }
}