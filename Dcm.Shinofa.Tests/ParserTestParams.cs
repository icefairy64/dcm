namespace Breezy.Dcm.Shinofa.Tests;

public class ParserTestParams
{
    public static IEnumerable<TestFixtureData> ParseModeParams
    {
        get
        {
            yield return new TestFixtureData(ParseMode.Default);
            yield return new TestFixtureData(ParseMode.SassWriterReEmit);
        }
    }
}

public enum ParseMode
{
    Default,
    SassWriterReEmit
}