using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Tests;

public class Element
{
    public static void AssertSequence(IList<ISyntaxElement> elements, Action<ElementSequenceAssertBuilder> action)
    {
        var builder = new ElementSequenceAssertBuilder(elements);
        action.Invoke(builder);
        builder.DoAssert();
    }
}

public sealed partial class ElementSequenceAssertBuilder
{
    private readonly IList<ISyntaxElement> Elements;
    private readonly IList<Action> Asserts = new List<Action>();
    private int CurrentElementIndex;

    public ElementSequenceAssertBuilder(IList<ISyntaxElement> elements)
    {
        Elements = elements;
    }

    private ISyntaxElement Advance()
    {
        return Elements[CurrentElementIndex++];
    }

    public void IsNumberLiteral(double value)
    {
        var el = Advance();
        Utils.AssertIs<NumberLiteral>(el, nl => Assert.That(nl.Value, Is.EqualTo(value)));
    }

    public void IsDimensionLiteral(double value, string unit)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<DimensionLiteral>(el, dl =>
        {
            Assert.That(dl.Value, Is.EqualTo(value));
            Assert.That(dl.Unit, Is.EqualTo(unit));
        }));
    }
    
    public void IsStringLiteral(string value)
    {
        var el = Advance();
        Utils.AssertIs<StringLiteral>(el, nl => Assert.That(nl.Value, Is.EqualTo(value)));
    }

    public void IsIdentifier(string name)
    {
        var el = Advance();
        Asserts.Add(() => Assert.That(el, Is.InstanceOf<Identifier>()));
        Asserts.Add(() => Assert.That(el as Identifier, Has.Property("Name").EqualTo(name), $"Element {el} at position {CurrentElementIndex}"));
    }

    public void IsArgument(string name)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<Argument>(el, arg =>
            {
                Utils.AssertIs<PlainVariable>(arg.Name, val => Assert.That(val.Name, Is.EqualTo(name)));
                Assert.That(arg.DefaultValue, Is.Null);
            });
        });
    }
    
    public void IsArgument(string name, Action<ElementSequenceAssertBuilder> defaultValueAction)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<Argument>(el, arg =>
            {
                Utils.AssertIs<PlainVariable>(arg.Name, val => Assert.That(val.Name, Is.EqualTo(name)));
                var builder = new ElementSequenceAssertBuilder( arg.DefaultValue is null
                    ? Array.Empty<ISyntaxElement>()
                    : new [] { arg.DefaultValue });
                defaultValueAction.Invoke(builder);
                builder.DoAssert();
            });
        });
    }
    
    public void IsVariable(string name)
    {
        var el = Advance();
        Asserts.Add(() => Assert.That(el, Is.InstanceOf<PlainVariable>()));
        Asserts.Add(() => Assert.That(el as PlainVariable, Has.Property("Name").EqualTo(name), $"Element {el} at position {CurrentElementIndex}"));
    }

    public void IsFileImport(string filePath)
    {
        var el = Advance();
        Asserts.Add(() => Assert.That(el, Is.InstanceOf<ImportStatement>()));
        Asserts.Add(() => Assert.That((el as ImportStatement)?.Target, Is.InstanceOf<StringLiteral>()));
        Asserts.Add(() => Assert.That(((el as ImportStatement)?.Target as StringLiteral), Has.Property("Value").EqualTo(filePath), $"Element {el} at position {CurrentElementIndex}"));
    }
    
    public void IsUrlImport(string url)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<ImportStatement>(el, import =>
            {
                Utils.AssertIs<CallExpression>(import.Target, ce =>
                {
                    Utils.AssertIs<Identifier>(ce.Callee, cl => Assert.That(cl.Name, Is.EqualTo("url")));
                    Utils.AssertIs<StringLiteral>(ce.Arguments.Arguments[0].Value, arg =>
                        Assert.That(arg.Value, Is.EqualTo(url)));
                });
            });
        });
    }
    
    public void IsDebugStatement(Action<ElementSequenceAssertBuilder> argAction)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<DebugStatement>(el, ds =>
            {
                var builder = new ElementSequenceAssertBuilder(ds.Children.ToList());
                argAction.Invoke(builder);
                builder.DoAssert();
            });
        });
    }
    
    public void IsWarnStatement(Action<ElementSequenceAssertBuilder> argAction)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<WarnStatement>(el, ds =>
            {
                var builder = new ElementSequenceAssertBuilder(ds.Children.ToList());
                argAction.Invoke(builder);
                builder.DoAssert();
            });
        });
    }
    
    public void IsErrorStatement(Action<ElementSequenceAssertBuilder> argAction)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<ErrorStatement>(el, ds =>
            {
                var builder = new ElementSequenceAssertBuilder(ds.Children.ToList());
                argAction.Invoke(builder);
                builder.DoAssert();
            });
        });
    }

    public void IsExtendStatement(Action<ElementSequenceAssertBuilder> argAction)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<ExtendStatement>(el, es =>
            {
                var builder = new ElementSequenceAssertBuilder(es.Children.ToList());
                argAction.Invoke(builder);
                builder.DoAssert();
            });
        });
    }
    
    public void IsIncludeStatement(string name, Action<ElementSequenceAssertBuilder> argAction)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<IncludeStatement>(el, ins =>
            {
                Assert.That(ins.Target.Name, Is.EqualTo(name));
                var builder = new ElementSequenceAssertBuilder(ins.Arguments is null
                    ? Array.Empty<ISyntaxElement>()
                    : ins.Arguments.Children.ToList());
                argAction.Invoke(builder);
                builder.DoAssert();
            });
        });
    }

    public void IsVariableDeclaration(string name, Action<ElementSequenceAssertBuilder> argListAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<VariableDeclaration>(el, decl =>
        {
            Utils.AssertIs<PlainVariable>(decl.Variable, v => { Assert.That(v.Name, Is.EqualTo(name)); });
            var builder = new ElementSequenceAssertBuilder(new[] { decl.Expression });
            argListAction.Invoke(builder);
            builder.DoAssert();
        }));
    }

    public void IsVariableDeclarationList(string name, Action<ElementSequenceAssertBuilder> argListAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<VariableDeclaration>(el, decl =>
        {
            Utils.AssertIs<PlainVariable>(decl.Variable, v => { Assert.That(v.Name, Is.EqualTo(name)); });
            Utils.AssertIs<ExpressionList>(decl.Expression, list =>
            {
                var builder = new ElementSequenceAssertBuilder(list.Expressions.ToList());
                argListAction.Invoke(builder);
                builder.DoAssert();
            });
        }));
    }
    
    public void IsPropertyDeclaration(string name, Action<ElementSequenceAssertBuilder> argListAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<PropertyDeclaration>(el, decl =>
        {
            Assert.That(decl.Property.Name, Is.EqualTo(name));
            
            var builder = new ElementSequenceAssertBuilder(new [] { decl.Expression });
            argListAction.Invoke(builder);
            builder.DoAssert();
        }));
    }
    
    public void IsPropertyDeclarationList(string name, Action<ElementSequenceAssertBuilder> argListAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<PropertyDeclaration>(el, decl =>
        {
            Assert.That(decl.Property.Name, Is.EqualTo(name));
            
            Utils.AssertIs<ExpressionList>(decl.Expression, list =>
            {
                var builder = new ElementSequenceAssertBuilder(list.Expressions.ToList());
                argListAction.Invoke(builder);
                builder.DoAssert();
            });
        }));
    }

    public void IsExpressionList(char separator, Action<ElementSequenceAssertBuilder> itemAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<ExpressionList>(el, list =>
        {
            Assert.That(list.Separator, Is.EqualTo(separator));
            var builder = new ElementSequenceAssertBuilder(list.Expressions.ToList());
            itemAction.Invoke(builder);
            builder.DoAssert();
        }));
    }

    public void IsMapExpression(Action<ElementSequenceAssertBuilder> entryAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<MapExpression>(el, map =>
        {
            var builder = new ElementSequenceAssertBuilder(map.Entries.Cast<ISyntaxElement>().ToList());
            entryAction.Invoke(builder);
            builder.DoAssert();
        }));
    }

    public void IsMapEntryExpression(Action<ElementSequenceAssertBuilder> itemAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<MapEntryExpression>(el, me =>
        {
            var builder = new ElementSequenceAssertBuilder(new[] { me.Key, me.Value });
            itemAction.Invoke(builder);
            builder.DoAssert();
        }));
    }

    public void IsBinaryExpression(string op, Action<ElementSequenceAssertBuilder> argAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<BinaryExpression>(el, be =>
        {
            Assert.That(be.Operator, Is.EqualTo(op));
            var builder = new ElementSequenceAssertBuilder(be.Children.ToList());
            argAction.Invoke(builder);
            builder.DoAssert();
        }));
    }
    
    public void IsUnaryExpression(string op, Action<ElementSequenceAssertBuilder> argAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<UnaryExpression>(el, be =>
        {
            Assert.That(be.Operator, Is.EqualTo(op));
            var builder = new ElementSequenceAssertBuilder(be.Children.ToList());
            argAction.Invoke(builder);
            builder.DoAssert();
        }));
    }

    public void IsCallExpression(string callee, Action<ElementSequenceAssertBuilder> argAction)
    {
        var el = Advance();
        Asserts.Add(() => Utils.AssertIs<CallExpression>(el, ce =>
        {
            Utils.AssertIs<Identifier>(ce.Callee, ident => Assert.That(ident.Name, Is.EqualTo(callee)));
            var builder = new ElementSequenceAssertBuilder(ce.Arguments.Arguments.Cast<ISyntaxElement>().ToList());
            argAction.Invoke(builder);
            builder.DoAssert();
        }));
    }

    public void IsCallArgument(Action<ElementSequenceAssertBuilder> valAction)
    {
        var el = Advance();
        Asserts.Add(() =>
        {
            Utils.AssertIs<CallArgument>(el, arg =>
            {
                var builder = new ElementSequenceAssertBuilder(new[] { arg.Value });
                valAction.Invoke(builder);
                builder.DoAssert();
            });
        });
    }

    public MixinDeclarationAssertBuilder IsMixinDeclaration(string name)
    {
        var el = Advance();
        var builder = new MixinDeclarationAssertBuilder();
        Asserts.Add(() => Utils.AssertIs<MixinDeclaration>(el, md =>
        {
            Assert.That(md.Name.Name, Is.EqualTo(name));
            builder.Mixin = md;
            builder.DoAssert();
        }));
        return builder;
    }

    public ForStatementAssertBuilder IsForStatement()
    {
        var el = Advance();
        var builder = new ForStatementAssertBuilder();
        Asserts.Add(() => Utils.AssertIs<ForStatement>(el, fs =>
        {
            builder.For = fs;
            builder.DoAssert();
        }));
        return builder;
    }

    public EachStatementAssertBuilder IsEachStatement()
    {
        var el = Advance();
        var builder = new EachStatementAssertBuilder();
        Asserts.Add(() => Utils.AssertIs<EachStatement>(el, es =>
        {
            builder.Each = es;
            builder.DoAssert();
        }));
        return builder;
    }

    public RuleDeclarationAssertBuilder IsRuleDeclaration()
    {
        var el = Advance();
        var builder = new RuleDeclarationAssertBuilder();
        Asserts.Add(() => Utils.AssertIs<RuleDeclaration>(el, rd =>
        {
            builder.Declaration = rd;
            builder.DoAssert();
        }));
        return builder;
    }
    
    internal void DoAssert()
    {
        Assert.Multiple(() =>
        {
            foreach (var assert in Asserts)
                assert.Invoke();
            while (CurrentElementIndex < Elements.Count)
                Assert.Fail($"Unexpected element {Elements[CurrentElementIndex++]}");
        });
    }
}

public sealed class MixinDeclarationAssertBuilder
{
    internal MixinDeclaration? Mixin { get; set; }
    private readonly List<Action> Asserts = new();
    
    public MixinDeclarationAssertBuilder WithoutArguments()
    {
        Asserts.Add(() => Assert.That(Mixin?.Arguments, Is.Null));
        return this;
    }
    
    public MixinDeclarationAssertBuilder WithArguments(Action<ElementSequenceAssertBuilder> elAction)
    {
        Asserts.Add(() =>
        {
            if (Mixin is null)
                throw new Exception("No mixin declaration");
            Assert.That(Mixin.Arguments, Is.Not.Null);
            var builder = new ElementSequenceAssertBuilder(Mixin.Arguments!.Children.ToList());
            elAction.Invoke(builder);
            builder.DoAssert();
        });
        return this;
    }

    public MixinDeclarationAssertBuilder WithBody(Action<ElementSequenceAssertBuilder> elAction)
    {
        Asserts.Add(() =>
        {
            if (Mixin is null)
                throw new Exception("No mixin declaration");
            var builder = new ElementSequenceAssertBuilder(Mixin.Body.Children.ToList());
            elAction.Invoke(builder);
            builder.DoAssert();
        });
        return this;
    }
    
    internal void DoAssert()
    {
        Assert.Multiple(() =>
        {
            foreach (var assert in Asserts)
                assert.Invoke();
        });
    }
}

public sealed class ForStatementAssertBuilder
{
    internal ForStatement For;
    private readonly List<Action> Asserts = new();

    public ForStatementAssertBuilder WithVariable(Action<ElementSequenceAssertBuilder> varAction)
    {
        Asserts.Add(() =>
        {
            var variableBuilder = new ElementSequenceAssertBuilder(new ISyntaxElement[] { For.Variable });
            varAction.Invoke(variableBuilder);
            variableBuilder.DoAssert();
        });
        return this;
    }

    public ForStatementAssertBuilder WithExclusiveRange(Action<ElementSequenceAssertBuilder> rangeAction)
    {
        Asserts.Add(() =>
        {
            var rangeBuilder = new ElementSequenceAssertBuilder(new[] { For.From, For.To });
            rangeAction.Invoke(rangeBuilder);
            rangeBuilder.DoAssert();
            Assert.That(For.Inclusive, Is.False);
        });
        return this;
    }
    
    public ForStatementAssertBuilder WithInclusiveRange(Action<ElementSequenceAssertBuilder> rangeAction)
    {
        Asserts.Add(() =>
        {
            var rangeBuilder = new ElementSequenceAssertBuilder(new[] { For.From, For.To });
            rangeAction.Invoke(rangeBuilder);
            rangeBuilder.DoAssert();
            Assert.That(For.Inclusive, Is.True);
        });
        return this;
    }

    public ForStatementAssertBuilder WithBody(Action<ElementSequenceAssertBuilder> bodyItemAction)
    {
        Asserts.Add(() =>
        {
            bodyItemAction.Invoke(new ElementSequenceAssertBuilder(For.Body.Children.ToList()));
        });
        return this;
    }
    
    internal void DoAssert()
    {
        Assert.Multiple(() =>
        {
            foreach (var assert in Asserts)
                assert.Invoke();
        });
    }
}

public sealed class EachStatementAssertBuilder
{
    internal EachStatement Each;
    private readonly List<Action> Asserts = new();
    
    public EachStatementAssertBuilder WithVariable(Action<ElementSequenceAssertBuilder> varAction)
    {
        Asserts.Add(() =>
        {
            var variableBuilder = new ElementSequenceAssertBuilder(Each.VariableList.Expressions.ToList());
            varAction.Invoke(variableBuilder);
            variableBuilder.DoAssert();
        });
        return this;
    }
    
    public EachStatementAssertBuilder WithExpression(Action<ElementSequenceAssertBuilder> exprAction)
    {
        Asserts.Add(() =>
        {
            var expressionBuilder = new ElementSequenceAssertBuilder(new List<ISyntaxElement> { Each.Expression });
            exprAction.Invoke(expressionBuilder);
            expressionBuilder.DoAssert();
        });
        return this;
    }

    public EachStatementAssertBuilder WithBody(Action<ElementSequenceAssertBuilder> bodyItemAction)
    {
        Asserts.Add(() =>
        {
            bodyItemAction.Invoke(new ElementSequenceAssertBuilder(Each.Body.Children.ToList()));
        });
        return this;
    }
    
    internal void DoAssert()
    {
        Assert.Multiple(() =>
        {
            foreach (var assert in Asserts)
                assert.Invoke();
        });
    }
}

public sealed class RuleDeclarationAssertBuilder
{
    internal RuleDeclaration Declaration;
    private readonly List<Action> Asserts = new();

    public RuleDeclarationAssertBuilder WithSelector(Action<ElementSequenceAssertBuilder> selAction)
    {
        Asserts.Add(() =>
        {
            var builder = new ElementSequenceAssertBuilder(new List<ISyntaxElement> { Declaration.Selector });
            selAction(builder);
            builder.DoAssert();
        });
        return this;
    }

    public RuleDeclarationAssertBuilder WithBody(Action<ElementSequenceAssertBuilder> itemAction)
    {
        Asserts.Add(() =>
        {
            var builder = new ElementSequenceAssertBuilder(Declaration.Body.Children.ToList());
            itemAction(builder);
            builder.DoAssert();
        });
        return this;
    }
    
    internal void DoAssert()
    {
        Assert.Multiple(() =>
        {
            foreach (var assert in Asserts)
                assert.Invoke();
        });
    }
}