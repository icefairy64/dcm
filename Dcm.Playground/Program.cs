﻿using Breezy.Dcm.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddHostedService<MainService>();
    });

await host.RunConsoleAsync();

public class MainService : IHostedService
{
    private readonly ILogger<DcmBuild> BuildLogger;
    private readonly ILoggerFactory LoggerFactory;
    private readonly IHostApplicationLifetime ApplicationLifetime;

    public MainService(ILogger<DcmBuild> buildLogger, ILoggerFactory loggerFactory, IHostApplicationLifetime applicationLifetime)
    {
        BuildLogger = buildLogger;
        LoggerFactory = loggerFactory;
        ApplicationLifetime = applicationLifetime;
    }

    public async Task StartAsyncBuild(CancellationToken cancellationToken)
    {
        var build = DcmBuild.Application(BuildLogger, LoggerFactory);
        
        build.ClassLoadFilter = className =>
            !className.StartsWith("KitchenSink.view.pivot") &&
            !className.StartsWith("KitchenSink.view.d3") &&
            !className.StartsWith("KitchenSink.view.calendar") &&
            !className.StartsWith("KitchenSink.view.grid.Exporter") &&
            !className.StartsWith("KitchenSink.view.grid.BigData") &&
            !className.StartsWith("Ext.d3");
        
        await build.Build(new[] { new DirectoryInfo("vendor-ext") }, new DirectoryInfo("kitchensink"), "production", "neptune");
        ApplicationLifetime.StopApplication();
    }
    
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var build = DcmBuild.Generic(BuildLogger, LoggerFactory);
        
        build.ClassLoadFilter = className =>
            !className.StartsWith("KitchenSink.view.pivot") &&
            !className.StartsWith("KitchenSink.view.d3") &&
            !className.StartsWith("KitchenSink.view.calendar") &&
            !className.StartsWith("KitchenSink.view.grid.Exporter") &&
            !className.StartsWith("KitchenSink.view.grid.BigData") &&
            !className.StartsWith("Ext.d3");
        
        await build.GenerateTypeDefinitions(new[] { new DirectoryInfo("vendor-ext/packages"), new DirectoryInfo("vendor-ext/classic") }, "classic");
        ApplicationLifetime.StopApplication();
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}