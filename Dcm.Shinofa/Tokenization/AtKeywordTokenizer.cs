namespace Breezy.Dcm.Shinofa.Tokenization;

public class AtKeywordTokenizer : ITokenizer
{
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        if (reader.Peek() is not '@')
        {
            token = null;
            return false;
        }
        
        reader.Advance(1);

        if (Utils.IsIdentSequence(reader))
        {
            var value = Utils.ReadIdentSequence(reader);
            token = new AtKeywordToken(reader.PrevPosition, reader.AdvancedLength, value);
            return true;
        }

        token = new DelimToken(reader.PrevPosition, '@');
        return true;
    }
}

public record AtKeywordToken(int StartPosition, int Length, string Value) : ISassToken
{
    public string GetStringRepresentation()
    {
        return "@" + Value;
    }
}