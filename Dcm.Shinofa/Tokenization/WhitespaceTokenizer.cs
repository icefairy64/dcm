namespace Breezy.Dcm.Shinofa.Tokenization;

public class WhitespaceTokenizer : ITokenizer
{
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        var whitespaceSeen = false;

        while (reader.Peek() is '\r' or '\n' or '\t' or ' ')
        {
            whitespaceSeen = true;
            reader.Advance(1);
        }

        token = whitespaceSeen ? new WhitespaceToken(reader.PrevPosition, reader.AdvancedLength) : null;
        return whitespaceSeen;
    }
}

public record WhitespaceToken(int StartPosition, int Length) : ISassToken
{
    public string GetStringRepresentation()
    {
        return " ";
    }
}