using System.Globalization;
using System.Text;

namespace Breezy.Dcm.Shinofa.Tokenization;

public class NumberTokenizer : ITokenizer
{
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        if (!IsNumber(reader))
        {
            token = null;
            return false;
        }

        var (number, type) = ReadNumber(reader);

        if (Utils.IsIdentSequence(reader))
        {
            var unit = Utils.ReadIdentSequence(reader);
            token = new DimensionToken(reader.PrevPosition, reader.AdvancedLength, number, type, unit);
            return true;
        }

        if (reader.Peek() is '%')
        {
            reader.Advance(1);
            token = new PercentageToken(reader.PrevPosition, reader.AdvancedLength, number);
            return true;
        }

        token = new NumberToken(reader.PrevPosition, reader.AdvancedLength, number, type);
        return true;
    }

    private static bool IsNumber(SourceReader reader)
    {
        return reader.Peek() switch
        {
            { } x when char.IsDigit(x) => true,
            '.' when reader.Peek(1) is { } x && char.IsDigit(x) => true,
            '-' or '+' when (reader.Peek(1) is { } x && char.IsDigit(x)) ||
                            (reader.Peek(1) is '.' && reader.Peek(2) is { } y && char.IsDigit(y)) => true,
            _ => false
        };
    }

    private static (double, NumberType) ReadNumber(SourceReader reader)
    {
        var builder = new StringBuilder();
        var type = NumberType.Integer;

        var peekChar = reader.Peek();

        if (peekChar is '+' or '-')
        {
            reader.Advance(1);
            builder.Append(peekChar);
            peekChar = reader.Peek();
        }

        while (peekChar is { } x && char.IsDigit(x))
        {
            reader.Advance(1);
            builder.Append(peekChar);
            peekChar = reader.Peek();
        }

        if (reader.Peek() is '.' && reader.Peek(1) is { } x2 && char.IsDigit(x2))
        {
            builder.Append(reader.Peek());
            reader.Advance(1);
            builder.Append(reader.Peek());
            reader.Advance(1);
            type = NumberType.Number;

            peekChar = reader.Peek();
            while (peekChar is { } x && char.IsDigit(x))
            {
                reader.Advance(1);
                builder.Append(peekChar);
                peekChar = reader.Peek();
            }
        }

        if (reader.Peek() is 'e' or 'E' && ((reader.Peek(1) is { } x3 && char.IsDigit(x3)) ||
                                            (reader.Peek(1) is '+' or '-' && reader.Peek(2) is { } x4 &&
                                             char.IsDigit(x4))))
        {
            builder.Append(reader.Peek());
            reader.Advance(1);
            builder.Append(reader.Peek());
            reader.Advance(1);
            type = NumberType.Number;
            
            peekChar = reader.Peek();
            while (peekChar is { } x && char.IsDigit(x))
            {
                reader.Advance(1);
                builder.Append(peekChar);
                peekChar = reader.Peek();
            }
        }

        var value = builder.ToString();
        return (double.Parse(value, CultureInfo.InvariantCulture), type);
    }
}

public enum NumberType
{
    Integer,
    Number
}

public record NumberToken(int StartPosition, int Length, double Value, NumberType Type) : ISassToken
{
    public string GetStringRepresentation()
    {
        return Type == NumberType.Integer ? Value.ToString("0") : Value.ToString(CultureInfo.InvariantCulture);
    }
}

public record DimensionToken(int StartPosition, int Length, double Value, NumberType Type, string Unit) : ISassToken
{
    public string GetStringRepresentation()
    {
        return (Type == NumberType.Integer ? Value.ToString("0") : Value.ToString(CultureInfo.InvariantCulture))
            + Unit;
    }
}

public record PercentageToken(int StartPosition, int Length, double Value) : ISassToken
{
    public string GetStringRepresentation()
    {
        return Value.ToString(CultureInfo.InvariantCulture) + "%";
    }
}