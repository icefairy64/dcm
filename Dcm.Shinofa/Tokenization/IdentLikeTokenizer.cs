using System.Text;

namespace Breezy.Dcm.Shinofa.Tokenization;

public class IdentLikeTokenizer : ITokenizer
{
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        if (!Utils.IsIdentSequence(reader))
        {
            token = null;
            return false;
        }

        var result = Utils.ReadIdentSequence(reader);

        if (result.Equals("url", StringComparison.InvariantCultureIgnoreCase) && reader.Peek() is '(')
        {
            // Consume opening paren
            reader.Advance(1);

            for (var whitespaceCount = 0; whitespaceCount < 2; whitespaceCount++)
            {
                if (reader.Peek() is '\n' or '\t' or ' ')
                    reader.Advance(1);
                else
                    break;
            }

            if (reader.Peek() is '\'' or '"' || (reader.Peek() is '\n' or '\t' or ' ' && reader.Peek(1) is '\'' or '"'))
            {
                token = new FunctionToken(reader.PrevPosition, reader.AdvancedLength, result);
                return true;
            }

            token = ReadUrlToken(reader);
            return true;
        }

        if (reader.Peek() is '(')
        {
            reader.Advance(1);
            token = new FunctionToken(reader.PrevPosition, reader.AdvancedLength, result);
            return true;
        }

        token = new IdentToken(reader.PrevPosition, reader.AdvancedLength, result);
        return true;
    }

    private ISassToken ReadUrlToken(SourceReader reader)
    {
        var builder = new StringBuilder();

        do
        {
            if (reader.Peek() is not { } peekChar)
                throw new ParseException("EOF while consuming URL");
            
            reader.Advance(1);

            if (peekChar is ')')
                return new UrlToken(reader.PrevPosition, reader.AdvancedLength, builder.ToString());

            if (peekChar is '\n' or '\t' or ' ')
            {
                while (reader.Peek() is '\n' or '\t' or ' ')
                    reader.Advance(1);
                
                if (reader.Peek() is ')')
                    return new UrlToken(reader.PrevPosition, reader.AdvancedLength, builder.ToString());
                
                if (reader.Peek() is null)
                    throw new ParseException("EOF while consuming URL");

                return ReadRemnantsOfBadUrl(reader, builder);
            }

            if (peekChar is '\'' or '"' or '(' or (<= '\x8') or '\xb' or (>= '\xe' and <= '\x1f') or '\x7f')
                return ReadRemnantsOfBadUrl(reader, builder);

            if (peekChar is '\\')
            {
                if (reader.TryPeekSlice(0, 2, out var span) && Utils.IsEscape(span))
                {
                    reader.Advance(1);
                    builder.Append(Utils.ReadEscapedChar(reader));
                    continue;
                }

                return ReadRemnantsOfBadUrl(reader, builder);
            }

            builder.Append(peekChar);
        } while (true);
    }

    private BadUrlToken ReadRemnantsOfBadUrl(SourceReader reader, StringBuilder builder)
    {
        while (reader.Peek() is { } peekChar)
        {
            reader.Advance(1);

            if (peekChar is ')')
                break;
            
            if (reader.TryPeekSlice(-1, 2, out var span) && Utils.IsEscape(span)) Utils.ReadEscapedChar(reader);
        }
        
        return new BadUrlToken(reader.PrevPosition, reader.AdvancedLength, builder.ToString());
    }
}

public record IdentToken(int StartPosition, int Length, string Value) : ISassToken
{
    public virtual string GetStringRepresentation()
    {
        return Value;
    }
}

public record FunctionToken(int StartPosition, int Length, string Value) : IdentToken(StartPosition, Length, Value)
{
    public override string GetStringRepresentation()
    {
        return Value + "(";
    }
}

public record UrlToken(int StartPosition, int Length, string Value) : IdentToken(StartPosition, Length, Value)
{
    public override string GetStringRepresentation()
    {
        return "url(" + Value + ")";
    }
}

public record BadUrlToken(int StartPosition, int Length, string Value) : IdentToken(StartPosition, Length, Value)
{
    public override string GetStringRepresentation()
    {
        return "url(" + Value + ")";
    }
}