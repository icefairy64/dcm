namespace Breezy.Dcm.Shinofa.Tokenization;

public interface ITokenizer
{
    bool TryReadToken(SourceReader reader, out ISassToken? token);
}

public interface ISassToken
{
    int StartPosition { get; }
    int Length { get; }

    string GetStringRepresentation(string source)
    {
        return source.Substring(StartPosition, Length);
    }

    (int, int) GetSourcePosition(string source)
    {
        var line = 1;
        var col = 1;
        var pos = 0;
        
        while (pos <= StartPosition)
        {
            if (pos < source.Length && source[pos] == '\r')
            {
                line++;
                col = 0;
            }

            if (pos < source.Length && source[pos] == '\n')
            {
                line++;
                col = 0;
            }
            
            pos++;
            if (pos <= StartPosition)
                col++;
        }

        return (line, col);
    }

    string GetStringRepresentation();
}