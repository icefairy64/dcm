namespace Breezy.Dcm.Shinofa.Tokenization;

public class CdoCdcTokenizer : ITokenizer
{
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        if (reader.MatchConsume("<!--"))
        {
            token = new CdoToken(reader.PrevPosition, reader.AdvancedLength);
            return true;
        }
        
        if (reader.MatchConsume("-->"))
        {
            token = new CdcToken(reader.PrevPosition, reader.AdvancedLength);
            return true;
        }

        token = null;
        return false;
    }
}

public record CdoToken(int StartPosition, int Length) : ISassToken
{
    public string GetStringRepresentation()
    {
        return "<!--";
    }
}

public record CdcToken(int StartPosition, int Length) : ISassToken
{
    public string GetStringRepresentation()
    {
        return "-->";
    }
}