namespace Breezy.Dcm.Shinofa.Tokenization;

public static class Tokenizer
{
    public static IList<ISassToken> Tokenize(string source)
    {
        var tokenizer = new RootTokenizer();
        var reader = new SourceReader(source);
        var result = new List<ISassToken>();

        while (tokenizer.TryReadToken(reader, out var token) && token is not null)
        {
            result.Add(token);
            reader.Commit();
        }

        if (!reader.IsDone)
            throw new Exception("Failed to tokenize the entire source");

        return result;
    }
}