namespace Breezy.Dcm.Shinofa.Tokenization;

public class RootTokenizer : ITokenizer
{
    private readonly List<ITokenizer> Stack = new()
    {
        new CssCommentTokenizer(),
        new WhitespaceTokenizer(),
        new CdoCdcTokenizer(),
        new ParenthesisTokenizer(),
        new IdentLikeTokenizer(),
        new HashTokenizer(),
        new AtKeywordTokenizer(),
        new NumberTokenizer(),
        new StringTokenizer(),
        new PunctuationTokenizer()
    };
    
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        foreach (var tokenizer in Stack)
        {
            if (tokenizer.TryReadToken(reader, out token))
                return true;
            
            reader.Reset();
        }

        token = null;
        return false;
    }
}