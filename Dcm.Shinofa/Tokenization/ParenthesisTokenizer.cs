namespace Breezy.Dcm.Shinofa.Tokenization;

public class ParenthesisTokenizer : ITokenizer
{
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        switch (reader.Peek())
        {
            case '(':
                token = new LeftParenthesisToken(reader.Position);
                reader.Advance(1);
                return true;
            case ')':
                token = new RightParenthesisToken(reader.Position);
                reader.Advance(1);
                return true;
            case '{':
                token = new LeftCurlyBracketToken(reader.Position);
                reader.Advance(1);
                return true;
            case '}':
                token = new RightCurlyBracketToken(reader.Position);
                reader.Advance(1);
                return true;
            case '[':
                token = new LeftSquareBracketToken(reader.Position);
                reader.Advance(1);
                return true;
            case ']':
                token = new RightSquareBracketToken(reader.Position);
                reader.Advance(1);
                return true;
            default:
                token = null;
                return false;
        }
    }
}

public record LeftParenthesisToken(int StartPosition) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return "(";
    }
}

public record RightParenthesisToken(int StartPosition) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return ")";
    }
}

public record LeftCurlyBracketToken(int StartPosition) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return "{";
    }
}

public record RightCurlyBracketToken(int StartPosition) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return "}";
    }
}

public record LeftSquareBracketToken(int StartPosition) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return "[";
    }
}

public record RightSquareBracketToken(int StartPosition) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return "]";
    }
}