using System.Text;

namespace Breezy.Dcm.Shinofa.Tokenization;

public class CssCommentTokenizer : ITokenizer
{
    internal const string CommentStartPattern = "/*";
    internal const string CommentEndPattern = "*/";
    internal const string SingleLineCommentStartPattern = "//";

    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        if (reader.MatchConsume(SingleLineCommentStartPattern))
        {
            var builder = new StringBuilder();
            while (reader.Peek() is { } peekChar and not '\n')
            {
                builder.Append(peekChar);
                reader.Advance(1);
            }
            token = new CssSingleLineCommentToken(reader.PrevPosition, reader.AdvancedLength, builder.ToString());
            return true;
        }
        
        if (!reader.MatchConsume(CommentStartPattern))
        {
            token = null;
            return false;
        }

        var mlBuilder = new StringBuilder();
        while (!reader.MatchConsume(CommentEndPattern) && !reader.IsDone)
        {
            if (reader.Peek() is { } mlPeekChar)
                mlBuilder.Append(mlPeekChar);
            reader.Advance(1);
        }

        token = new CssCommentToken(reader.PrevPosition, reader.AdvancedLength, mlBuilder.ToString());
        return true;
    }
}

public record CssCommentToken(int StartPosition, int Length, string Body) : ISassToken
{
    public string GetStringRepresentation()
    {
        return CssCommentTokenizer.CommentStartPattern + " " + Body + " " + CssCommentTokenizer.CommentEndPattern;
    }
}

public record CssSingleLineCommentToken(int StartPosition, int Length, string Body) : ISassToken
{
    public string GetStringRepresentation()
    {
        return CssCommentTokenizer.SingleLineCommentStartPattern + " " + Body;
    }
}