namespace Breezy.Dcm.Shinofa.Tokenization;

public class PunctuationTokenizer : ITokenizer
{
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        switch (reader.Peek())
        {
            case null:
                token = null;
                return false;
            case ',':
                token = new CommaToken(reader.Position);
                reader.Advance(1);
                return true;
            case ':':
                token = new ColonToken(reader.Position);
                reader.Advance(1);
                return true;
            case ';':
                token = new SemicolonToken(reader.Position);
                reader.Advance(1);
                return true;
            case { } x:
                token = new DelimToken(reader.Position, x);
                reader.Advance(1);
                return true;
        }
    }
}

public record CommaToken(int StartPosition) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return ",";
    }
}

public record ColonToken(int StartPosition) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return ":";
    }
}

public record SemicolonToken(int StartPosition) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return ";";
    }
}

public record DelimToken(int StartPosition, char Value) : ISassToken
{
    public int Length => 1;
    public string GetStringRepresentation()
    {
        return Value.ToString();
    }
}