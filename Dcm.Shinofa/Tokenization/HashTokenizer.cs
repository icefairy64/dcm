namespace Breezy.Dcm.Shinofa.Tokenization;

public class HashTokenizer : ITokenizer
{
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        if (reader.Peek() is not '#')
        {
            token = null;
            return false;
        }
        
        reader.Advance(1);
        
        if (Utils.IsIdent(reader.Peek() ?? '~') || (reader.TryPeekSlice(0, 2, out var span) && Utils.IsEscape(span)))
        {
            var value = Utils.ReadIdentSequence(reader);
            token = new HashToken(reader.PrevPosition, reader.AdvancedLength, "id", value);
            return true;
        }

        token = new DelimToken(reader.PrevPosition, '#');
        return true;
    }
}

public record HashToken(int StartPosition, int Length, string TypeFlag, string Value) : ISassToken
{
    public string GetStringRepresentation()
    {
        return "#" + Value;
    }
}