namespace Breezy.Dcm.Shinofa.Tokenization;

public class ParseException : Exception
{
    public IReadOnlyDictionary<string, ISassToken?>? Tokens;
    
    public ParseException(string? message) : base(message)
    {
    }
}