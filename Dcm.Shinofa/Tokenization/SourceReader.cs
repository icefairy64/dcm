namespace Breezy.Dcm.Shinofa.Tokenization;

public sealed class SourceReader
{
    private readonly string Source;
    
    public int Position { get; private set; }
    public int PrevPosition { get; private set; }
    public int Row { get; private set; } = 1;
    public int Column { get; private set; } = 1;

    public bool IsDone => Position >= Source.Length;
    public int AdvancedLength => Position - PrevPosition;

    public SourceReader(string source)
    {
        Source = source;
    }

    public char? Peek() => Position >= Source.Length ? null : Source[Position];

    public char? Peek(int offset) => Position + offset >= Source.Length ? null : Source[Position + offset];

    public void Advance(int length)
    {
        for (var i = 0; i < length; i++)
        {
            if (Position + i >= Source.Length)
                break;
            
            if (Source[Position + i] == '\n')
            {
                Row++;
                Column = 1;
            }
            else
            {
                Column++;
            }
        }
        Position += length;
    }

    public void Commit()
    {
        PrevPosition = Position;
    }

    public void Reset()
    {
        Position = PrevPosition;
    }

    public bool Match(ReadOnlySpan<char> pattern)
    {
        if (Position + pattern.Length > Source.Length)
            return false;
        var sourceSpan = (ReadOnlySpan<char>)Source;
        var sourcePatternSpan = sourceSpan.Slice(Position, pattern.Length);
        return sourcePatternSpan.SequenceEqual(pattern);
    }
    
    public bool MatchConsume(ReadOnlySpan<char> pattern)
    {
        var result = Match(pattern);
        if (result)
            Advance(pattern.Length);
        return result;
    }
    
    public bool TryPeekSlice(int offset, int length, out ReadOnlySpan<char> span)
    {
        if (Position + offset + length > Source.Length)
        {
            span = default;
            return false;
        }

        span = ((ReadOnlySpan<char>)Source).Slice(Position + offset, length);
        return true;
    }

    public bool TryReadSlice(int length, out ReadOnlySpan<char> span)
    {
        var result = TryPeekSlice(0, length, out span);
        if (result)
            Advance(length);
        return result;
    }
}