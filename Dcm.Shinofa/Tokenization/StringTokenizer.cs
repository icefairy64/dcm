using System.Text;

namespace Breezy.Dcm.Shinofa.Tokenization;

public class StringTokenizer : ITokenizer
{
    public bool TryReadToken(SourceReader reader, out ISassToken? token)
    {
        var peekChar = reader.Peek();
        if (peekChar is not ('\'' or '"'))
        {
            token = null;
            return false;
        }

        var builder = new StringBuilder();

        do
        {
            reader.Advance(1);
            switch (reader.Peek())
            {
                case null:
                    token = new StringToken(reader.PrevPosition, reader.AdvancedLength, builder.ToString(), peekChar.Value);
                    return true;
                
                case { } x when x == peekChar:
                    reader.Advance(1);
                    token = new StringToken(reader.PrevPosition, reader.AdvancedLength, builder.ToString(), peekChar.Value);
                    return true;
                
                case '\n':
                    reader.Advance(-1);
                    token = new BadStringToken(reader.PrevPosition, reader.AdvancedLength);
                    return true;
                
                case '\\' when reader.Peek(1) is null:
                    break;
                
                case '\\' when reader.Peek(1) is '\n':
                    reader.Advance(1);
                    builder.Append('\n');
                    break;
                
                case '\\' when reader.TryPeekSlice(0, 2, out var span) && Utils.IsEscape(span):
                    reader.Advance(1);
                    var escapedChar = Utils.ReadEscapedChar(reader);
                    builder.Append(escapedChar);
                    break;
                
                case { } x:
                    builder.Append(x);
                    break;
            }
        } while (true);
    }
}

public record StringToken(int StartPosition, int Length, string Value, char quoteChar) : ISassToken
{
    public string GetStringRepresentation()
    {
        return quoteChar + Value + quoteChar;
    }
}

public record BadStringToken(int StartPosition, int Length) : ISassToken
{
    public string GetStringRepresentation()
    {
        return "'BADSTRING'";
    }
}