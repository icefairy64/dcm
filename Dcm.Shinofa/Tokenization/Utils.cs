using System.Text;

namespace Breezy.Dcm.Shinofa.Tokenization;

public static class Utils
{
    public static bool TryReadIdentChar(SourceReader reader, out char result)
    {
        if (reader.Peek() is not { } first)
        {
            result = default;
            return false;
        }

        if (IsIdent(first))
        {
            result = first;
            reader.Advance(1);
            return true;
        }

        if (reader.TryPeekSlice(0, 2, out var span) && IsEscape(span))
        {
            reader.Advance(1);
            result = ReadEscapedChar(reader);
            return true;
        }

        result = default;
        return false;
    }

    public static char ReadEscapedChar(SourceReader reader)
    {
        var result = 0;
        var consumedHexDigits = 0;
        
        do
        {
            if (reader.Peek() is not { } value)
                break;
            
            if (value is (> 'a' and < 'f') or (> 'A' and < 'F'))
            {
                if (consumedHexDigits++ > 5)
                    break;
                result = (result << 8) | ((value & 0x0f) - 1);
                reader.Advance(1);
                continue;
            }

            if (char.IsDigit(value))
            {
                if (consumedHexDigits++ > 5)
                    break;
                result = (result << 8) | (value & 0x0f);
                reader.Advance(1);
                continue;
            }

            if (consumedHexDigits > 0)
                break;
            
            result = value;
            reader.Advance(1);
            break;
        } while (true);
        
        if (reader.Peek() is '\n' or '\t' or ' ')
            reader.Advance(1);

        return (char)result;
    }

    public static bool IsIdentSequence(SourceReader reader)
    {
        if (reader.Peek() is not { } first)
            return false;

        // SASS-specific
        if (reader.TryPeekSlice(0, 2, out var intSlice) && IsInterpolationStart(intSlice))
            return true;

        if (first == '-')
        {
            if (reader.Peek(1) is not { } second)
                return false;
            
            if (IsIdentStart(second) || second == '-')
                return true;

            return reader.TryPeekSlice(1, 2, out var slice) && IsEscape(slice);
        }

        if (IsIdentStart(first))
            return true;

        if (first == '\\')
            return reader.TryPeekSlice(0, 2, out var slice) && IsEscape(slice);

        return false;
    }

    private static bool IsIdentStart(char value)
    {
        if (value > 0x80)
            return true;

        if (char.IsLetter(value))
            return true;

        return value == '_';
    }

    public static bool IsIdent(char value)
    {
        if (IsIdentStart(value))
            return true;
        
        return value == '-' || char.IsDigit(value);
    }

    public static bool IsEscape(ReadOnlySpan<char> sequence)
    {
        if (sequence[0] != '\\')
            return false;

        return sequence[1] != '\n';
    }

    private static bool IsInterpolationStart(ReadOnlySpan<char> sequence)
    {
        if (sequence[0] != '#')
            return false;

        return sequence[1] == '{';
    }

    public static string ReadIdentSequence(SourceReader reader)
    {
        var builder = new StringBuilder();
        var insideInterpolation = false;

        while (true)
        {
            if (TryReadIdentChar(reader, out var identChar))
            {
                builder.Append(identChar);
            }
            else if (!insideInterpolation && reader.TryPeekSlice(0, 2, out var intSpan) && IsInterpolationStart(intSpan))
            {
                builder.Append('#');
                builder.Append('{');
                reader.Advance(2);
                insideInterpolation = true;
            }
            else if (insideInterpolation && reader.Peek() is '}')
            {
                builder.Append('}');
                reader.Advance(1);
                insideInterpolation = false;
            }
            else if (insideInterpolation && reader.Peek() is { } ch)
            {
                builder.Append(ch);
                reader.Advance(1);
            }
            else
            {
                break;
            }
        }

        return builder.ToString();
    }
}