using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class ExtendStatementParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not AtKeywordToken { Value: "extend" })
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        if (SelectorUtils.ReadSelector(reader) is not { } selector)
            throw new ParseException("Could not read selector to @extend");
        
        if (reader.Peek() is SemicolonToken)
            reader.Advance(1);

        result = new ExtendStatement(selector);
        return true;
    }
}