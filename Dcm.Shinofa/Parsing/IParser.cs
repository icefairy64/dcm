namespace Breezy.Dcm.Shinofa.Parsing;

public interface IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result);
}