using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class PropertyDeclarationParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not IdentToken id)
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);
        
        if (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        if (reader.Peek() is not ColonToken)
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);
        
        if (reader.Peek() is WhitespaceToken)
            reader.Advance(1);
        
        var isImportant = false;
        var exclReached = false;
        var done = false;
        
        var expressionTokens = new List<ISassToken>();
        var expressionParser = new ExpressionParser();

        while (!done)
        {
            switch (reader.Peek())
            {
                case DelimToken { Value: '!' }:
                    reader.Advance(1);
                    exclReached = true;
                    if (reader.Peek() is IdentToken { Value: "important" })
                    {
                        isImportant = true;
                        reader.Advance(1);
                    }
                    else
                    {
                        throw new ParseException($"Unexpected token {reader.Peek()}");
                    }

                    break;

                case SemicolonToken:
                    done = true;
                    reader.Advance(1);
                    break;
                
                case WhitespaceToken when exclReached:
                    reader.Advance(1);
                    break;
                
                case RightCurlyBracketToken:
                    // Assume that this is the last property in the body
                    done = true;
                    break;
                
                case { } token:
                    if (exclReached)
                        throw new ParseException($"Unexpected token {token} after exclamations");
                    expressionTokens.Add(token);
                    reader.Advance(1);
                    break;

                default:
                    // Maybe this is not 100% correct
                    done = true;
                    break;
            }
        }

        var expressionReader = new TokenReader(expressionTokens);
        
        if (!expressionParser.TryParse(expressionReader, out var expression))
        {
            result = null;
            return false;
        }

        result = new PropertyDeclaration(new Identifier(id.Value),
            expression ?? throw new ParseException("Not an expression"),
            isImportant);
        return true;
    }
}