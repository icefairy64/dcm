namespace Breezy.Dcm.Shinofa.Parsing;

public class RootParser : IParser
{
    private static readonly List<IParser> Stack = new()
    {
        new RuleDeclarationParser(),
        new VariableDeclarationParser(),
        new FunctionDeclarationParser(),
        new ReturnStatementParser(),
        new IfStatementParser(),
        new ForStatementParser(),
        new EachStatementParser(),
        new DebugStatementParser(),
        new ImportStatementParser(),
        new IncludeStatementParser(),
        new PropertyDeclarationParser(),
        new ExtendStatementParser(),
        new AtRuleParser(),
        new ComponentValueParser(),
        new SimpleBlockParser()
    };
    
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        while (!reader.IsDone)
        {
            foreach (var parser in Stack)
            {
                if (parser.TryParse(reader, out result))
                    return true;
                
                reader.Reset();
            }
        }

        result = null;
        return false;
    }
}