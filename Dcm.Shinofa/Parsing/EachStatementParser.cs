using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class EachStatementParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not AtKeywordToken { Value: "each" })
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var exprParser = new ExpressionParser();

        if (!exprParser.TryParse(reader, out var listOrExpr) || listOrExpr is null)
            throw new ParseException("Could not parse @each variable list");

        var exprList = listOrExpr switch
        {
            ExpressionList list => list,
            not null => new ExpressionList(new [] { listOrExpr }, ','),
            _ => throw new ParseException($"Unexpected syntax element {listOrExpr.GetType().Name}")
        };
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        if (reader.Peek() is not IdentToken { Value: "in" })
            throw new ParseException("Expected 'in' keyword");
        
        reader.Advance(1);

        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);
        
        if (!exprParser.TryParse(reader, out var expr) || expr is null)
            throw new ParseException("Could not parse @each iterable expression");
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var bodyParser = new BodyParser();

        if (!bodyParser.TryParse(reader, out var untypedBody) || untypedBody is not Body body)
            throw new ParseException("Could not parse @each body");

        result = new EachStatement(exprList, expr, body);
        return true;
    }
}