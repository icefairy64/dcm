using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class VariableDeclarationParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not DelimToken { Value: '$' })
        {
            result = null;
            return false;
        }

        reader.Advance(1);

        if (reader.Peek() is not IdentToken { Value: var name })
        {
            result = null;
            return false;
        }

        var variable = new PlainVariable(name);
        
        reader.Advance(1);
        
        if (reader.Peek() is not ColonToken)
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var isDefault = false;
        var isGlobal = false;
        var exclReached = false;
        var done = false;

        var expressionTokens = new List<ISassToken>();
        var expressionParser = new ExpressionParser();

        while (!done)
        {
            switch (reader.Peek())
            {
                case DelimToken { Value: '!' }:
                    reader.Advance(1);
                    exclReached = true;
                    if (reader.Peek() is IdentToken { Value: "default" })
                    {
                        isDefault = true;
                        reader.Advance(1);
                    }
                    else if (reader.Peek() is IdentToken { Value: "global" })
                    {
                        isGlobal = true;
                        reader.Advance(1);
                    }
                    else
                    {
                        throw new ParseException($"Unexpected token {reader.Peek()}");
                    }

                    break;

                case SemicolonToken:
                    done = true;
                    reader.Advance(1);
                    break;
                
                case WhitespaceToken when exclReached:
                    reader.Advance(1);
                    break;
                
                case { } token:
                    if (exclReached)
                        throw new ParseException($"Unexpected token {token} after exclamations");
                    expressionTokens.Add(token);
                    reader.Advance(1);
                    break;

                default:
                    // Maybe it is not 100% correct
                    done = true;
                    break;
            }
        }

        var expressionReader = new TokenReader(expressionTokens);
        
        if (!expressionParser.TryParse(expressionReader, out var expression))
        {
            throw new ParseException("Failed to parse variable declarator expression")
            {
                Tokens = new Dictionary<string, ISassToken?>
                {
                    { "First", expressionTokens.First() },
                    { "Last", expressionTokens.Last() },
                    { "Stopped at", expressionReader.Peek() }
                }
            };
        }
        
        result = new VariableDeclaration(variable,
            expression ?? throw new Exception("Not an element"), isGlobal,
            isDefault);
        return true;
    }
}