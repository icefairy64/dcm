namespace Breezy.Dcm.Shinofa.Parsing;

public record IfStatement(ISyntaxElement Expression, Body Body, ISyntaxElement? Else) 
    : Node(new List<ISyntaxElement?> { Expression, Body, Else }.OfType<ISyntaxElement>().ToList());