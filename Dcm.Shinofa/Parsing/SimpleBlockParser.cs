using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class SimpleBlockParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        var startToken = reader.Peek();

        if (startToken is not (LeftCurlyBracketToken or LeftSquareBracketToken or LeftParenthesisToken))
        {
            result = null;
            return false;
        }

        result = Utils.ReadSimpleBlock(reader);
        return true;
    }
}