using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public static class SelectorUtils
{
    public static ISelector? ReadSelector(TokenReader reader)
    {
        var compoundList = new List<ISelector>();
        
        while (reader.Peek() is not WhitespaceToken)
        {
            var immediateResult = reader.Peek() switch
            {
                DelimToken { Value: '&' } =>
                    new NestingSelector(reader.ConsumeToList(1)),
                
                DelimToken { Value: '*' } =>
                    new UniversalSelector(reader.ConsumeToList(1)),
                
                FunctionToken => null,
                
                IdentToken id =>
                    new TypeSelector(reader.ConsumeToList(1), id.Value),
                
                DelimToken { Value: '.' } when reader.Peek(1) is IdentToken id =>
                    new ClassSelector(reader.ConsumeToList(2), id.Value),
                
                HashToken hash =>
                    new IdSelector(reader.ConsumeToList(1), hash.Value),
                
                ColonToken when reader.Peek(1) is ColonToken && reader.Peek(2) is FunctionToken { Value: var name } =>
                    ReadPseudoElementSelector(reader, name, reader.ConsumeToList(3), true),
                
                ColonToken when reader.Peek(1) is ColonToken && reader.Peek(2) is IdentToken { Value: var name } =>
                    ReadPseudoElementSelector(reader, name, reader.ConsumeToList(3)),
                
                ColonToken when reader.Peek(1) is FunctionToken { Value: var name } =>
                    ReadPseudoClassSelector(reader, name, reader.ConsumeToList(2), true),
                
                ColonToken when reader.Peek(1) is IdentToken { Value: var name } =>
                    ReadPseudoClassSelector(reader, name, reader.ConsumeToList(2)),
                
                LeftSquareBracketToken =>
                    ReadAttributeSelector(reader),
                
                _ => null
            };

            if (immediateResult is null)
                break;

            compoundList.Add(immediateResult);
        }

        var left = compoundList switch
        {
            [] => null,
            [var singleSelector] => singleSelector,
            _ => new CompoundSelector(compoundList)
        };

        if (left is null)
            return null;
        
        if (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        switch (reader.Peek())
        {
            case DelimToken { Value: '>' or '+' or '~' } combinatorToken:
            {
                reader.Advance(1);
                if (reader.Peek() is WhitespaceToken)
                    reader.Advance(1);
                var right = ReadSelector(reader);
                if (right is null)
                    return null;
                return combinatorToken.Value switch
                {
                    '>' => new ChildSelector(left, right),
                    '+' => new NextSiblingSelector(left, right),
                    '~' => new SubsequentSiblingSelector(left, right),
                    _ => null
                };
            }
            default:
            {
                var right = ReadSelector(reader);
                return right is null ? left : new DescendantSelector(left, right);
            }
        }
    }

    public static ISelector? ReadSelectorList(TokenReader reader)
    {
        var selectors = new List<ISelector>();
        do
        {
            
            if (reader.Peek() is WhitespaceToken or CommaToken)
            {
                reader.Advance(1);
                continue;
            }

            var selector = ReadSelector(reader);
            
            if (selector is null)
                return selectors switch
                {
                    [] => null,
                    [var singleSelector] => singleSelector,
                    _ => new ListSelector(selectors)
                };
            selectors.Add(selector);
        } while (true);
    }

    private static ISelector ReadPseudoElementSelector(TokenReader reader, string name, IReadOnlyList<ISassToken> tokens, bool isFunction = false)
    {
        if (!isFunction)
            return new PseudoElementSelector(tokens, name);
        
        var fullTokens = new List<ISassToken>(tokens);
        var body = ReadSelectorList(reader);
        if (body is null)
            throw new ParseException("Could not read pseudo-element selector body");
        if (reader.Peek() is not RightParenthesisToken)
            throw new ParseException("Right parenthesis expected when reading pseudo-element selector");
        fullTokens.AddRange(reader.ConsumeToEnumerable(1));
        return new ComplexPseudoElementSelector(fullTokens, name, body);
    }
    
    private static ISelector ReadPseudoClassSelector(TokenReader reader, string name, IReadOnlyList<ISassToken> tokens, bool isFunction = false)
    {
        if (!isFunction)
            return new PseudoClassSelector(tokens, name);
        
        var fullTokens = new List<ISassToken>(tokens);

        if (name is "is" or "has" or "not" or "where")
        {
            var body = ReadSelectorList(reader);
            if (body is null)
                throw new ParseException("Could not read pseudo-element selector body");
            if (reader.Peek() is not RightParenthesisToken)
                throw new ParseException("Left parenthesis expected when reading pseudo-element selector");
            fullTokens.AddRange(reader.ConsumeToEnumerable(1));
            return new ComplexPseudoClassSelector(fullTokens, name, body);
        }

        var bodyTokens = new List<ISassToken>();

        while (reader.Peek() is not RightParenthesisToken or null)
        {
            bodyTokens.Add(reader.Peek()!);
            reader.Advance(1);
        }
        
        if (reader.Peek() is not RightParenthesisToken)
            throw new ParseException("Right parenthesis expected when reading pseudo-class selector");
        fullTokens.AddRange(reader.ConsumeToEnumerable(1));
        
        return new ContentfulPseudoClassSelector(fullTokens, name, bodyTokens);
    }

    private static ISelector ReadAttributeSelector(TokenReader reader)
    {
        var tokens = new List<ISassToken>(reader.ConsumeToEnumerable(1));
        if (reader.Peek() is not IdentToken id)
            throw new ParseException("Ident token expected");
        tokens.AddRange(reader.ConsumeToEnumerable(1));

        if (reader.Peek() is RightSquareBracketToken)
        {
            tokens.AddRange(reader.ConsumeToEnumerable(1));
            return new AttributeSelector(tokens, id.Value, null, null, null);
        }
        
        var op = reader.Peek() switch
        {
            DelimToken { Value: '=' } => "=",
            DelimToken { Value: '~' or '|' or '^' or '*' or '$' } leftDelim when reader.Peek(1) is DelimToken { Value: '=' } =>
                $"{leftDelim.Value}=",
            _ => throw new ParseException($"Unexpected token {reader.Peek()?.GetType().Name ?? "null"} {reader.Peek()?.GetStringRepresentation()}")
        };
        
        tokens.AddRange(reader.ConsumeToEnumerable(op.Length));

        var value = reader.Peek() switch
        {
            StringToken { Value: var v } => v,
            IdentToken { Value: var v } => v,
            _ => throw new ParseException($"Unexpected token {reader.Peek()?.GetType().Name ?? "null"} {reader.Peek()?.GetStringRepresentation()}")
        };
        
        tokens.AddRange(reader.ConsumeToEnumerable(1));

        switch (reader.Peek())
        {
            case WhitespaceToken when reader.Peek(1) is IdentToken { Value: "i" or "s" } caseToken && reader.Peek(2) is RightSquareBracketToken:
            {
                tokens.AddRange(reader.ConsumeToEnumerable(3));
                return new AttributeSelector(tokens, id.Value, op, value, caseToken.Value == "s");
            }
            case RightSquareBracketToken:
            {
                tokens.AddRange(reader.ConsumeToEnumerable(1));
                return new AttributeSelector(tokens, id.Value, op, value, null);
            }
            default: throw new ParseException($"Unexpected token {reader.Peek()}");
        }
    }
}