using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class TokenReader
{
    private IReadOnlyList<ISassToken> Tokens;
    
    public int Position { get; private set; }
    public int PrevPosition { get; private set; }
    
    public int AdvancedLength => Position - PrevPosition;
    public bool IsDone => Position >= Tokens.Count;
    public IEnumerable<ISassToken> AdvancedTokens => Tokens.Skip(PrevPosition).Take(AdvancedLength);

    public TokenReader(IReadOnlyList<ISassToken> tokens)
    {
        Tokens = tokens;
    }

    public ISassToken? Peek()
    {
        return Position >= Tokens.Count ? null : Tokens[Position];
    }
    
    public ISassToken? Peek(int offset)
    {
        return Position + offset >= Tokens.Count ? null : Tokens[Position + offset];
    }
    
    public ISassToken? PeekAt(int position)
    {
        return position >= Tokens.Count ? null : Tokens[position];
    }

    public void Advance(int count)
    {
        Position += count;
    }

    public void Commit()
    {
        PrevPosition = Position;
    }

    public void Reset()
    {
        Position = PrevPosition;
    }

    public TokenReader CreateBranch()
    {
        return new TokenReader(Tokens)
        {
            Position = Position,
            PrevPosition = Position
        };
    }
    
    public TokenReader CreateBranchUntil(Predicate<ISassToken> predicate)
    {
        var startPos = Position;
        int endPos;
        for (endPos = startPos; endPos < Tokens.Count; endPos++)
        {
            if (predicate(Tokens[endPos]))
                break;
        }
        return new TokenReader(Tokens.Skip(startPos).Take(endPos - startPos).ToList());
    }

    public IReadOnlyList<ISassToken> ConsumeToList(int count)
    {
        var result = Tokens.Skip(Position).Take(count).ToList();
        Advance(count);
        return result;
    }
    
    public IEnumerable<ISassToken> ConsumeToEnumerable(int count)
    {
        var result = Tokens.Skip(Position).Take(count);
        Advance(count);
        return result;
    }
}