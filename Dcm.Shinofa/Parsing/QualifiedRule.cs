using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public record QualifiedRule(IEnumerable<ISassToken> Tokens, IEnumerable<IComponentValue> Prelude, SimpleBlock Value) : IParsedTokenGroup;