namespace Breezy.Dcm.Shinofa.Parsing;

public abstract record Node(IReadOnlyList<ISyntaxElement> Children) : ISyntaxElement;