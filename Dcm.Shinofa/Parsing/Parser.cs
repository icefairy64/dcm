using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public static class Parser
{
    public static IList<ISyntaxElement> Parse(string source)
    {
        var parser = new RootParser();
        var tokens = Tokenizer.Tokenize(source).AsReadOnly();
        var reader = new TokenReader(tokens);
        var result = new List<ISyntaxElement>();

        try
        {
            while (parser.TryParse(reader, out var group) && group is not null)
            {
               
                result.Add(group);
                while (reader.Peek() is WhitespaceToken)
                    reader.Advance(1);
                reader.Commit();
            }
        }
        catch (ParseException e)
        {
            var startToken = reader.PeekAt(reader.PrevPosition);
            var startPos = startToken?.GetSourcePosition(source);
            var curToken = reader.PeekAt(reader.Position);
            var curPos = curToken?.GetSourcePosition(source);
            foreach (var el in result)
                Console.WriteLine(el);
            Console.WriteLine();
            Console.WriteLine($"Prev position: {reader.PrevPosition} {startPos}, current: {reader.Position} {curPos}");
            if (e.Tokens is { } reportedTokens)
            {
                Console.WriteLine("Extra data:");
                foreach (var (key, value) in reportedTokens)
                {
                    if (value is null)
                        continue;
                    var pos = value.GetSourcePosition(source);
                    Console.WriteLine($"{key}: {value.GetType().Name} {pos}");
                }
            }
            throw;
        }

        if (!reader.IsDone)
            throw new Exception("Failed to parse the entire source");

        return result;
    }
}