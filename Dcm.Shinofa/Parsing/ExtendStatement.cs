namespace Breezy.Dcm.Shinofa.Parsing;

public record ExtendStatement(ISelector Selector)
    : Node(new [] { Selector });