using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class ReturnStatementParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not AtKeywordToken { Value: "return" } returnToken)
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var expressionParser = new ExpressionParser();

        if (!expressionParser.TryParse(reader, out var expression) || expression is null)
        {
            throw new ParseException("Could not parse @return value")
            {
                Tokens = new Dictionary<string, ISassToken?>
                {
                    { "Return", returnToken }
                }
            };
        }
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        result = new ReturnStatement(expression);
        return true;
    }
}