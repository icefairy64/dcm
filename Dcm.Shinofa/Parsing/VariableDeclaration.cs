namespace Breezy.Dcm.Shinofa.Parsing;

public record VariableDeclaration(ISyntaxElement Variable, ISyntaxElement Expression, bool Global, bool Default) 
    : Node(new List<ISyntaxElement> { Variable, Expression });

public record PlainVariable(string Name) : ISyntaxElement;

public record Identifier(string Name) : ISyntaxElement;