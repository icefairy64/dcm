using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public record SimpleBlock(IEnumerable<ISassToken> Tokens, ISassToken AssociatedToken, IEnumerable<IComponentValue> Value) : IComponentValue;