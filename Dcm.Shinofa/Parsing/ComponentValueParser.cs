namespace Breezy.Dcm.Shinofa.Parsing;

public class ComponentValueParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is null)
        {
            result = null;
            return false;
        }

        result = Utils.ReadComponentValue(reader);
        return true;
    }
}