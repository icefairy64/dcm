namespace Breezy.Dcm.Shinofa.Parsing;

public record Body(IReadOnlyList<ISyntaxElement> Children) : Node(Children);