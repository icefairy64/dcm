using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public interface IParsedTokenGroup : ISyntaxElement
{
    IEnumerable<ISassToken> Tokens { get; }
}