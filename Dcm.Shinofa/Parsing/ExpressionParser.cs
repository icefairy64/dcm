using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class ExpressionParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        var isMap = false;

        ISyntaxElement? expr = null;

        if (reader.Peek() is LeftParenthesisToken && reader.Peek(1) is RightParenthesisToken)
        {
            // TODO: generalize?
            result = new ArgumentList(Array.Empty<Argument>());
            return true;
        }

        if (reader.Peek() is LeftParenthesisToken)
        {
            // Scan for map declaration signs
            var offset = 1;
            var level = 0;
            while (level >= 0 && reader.Peek(offset) is not null)
            {
                if (reader.Peek(offset) is ColonToken && level == 0)
                {
                    // Looks like a map declaration
                    isMap = true;
                    reader.Advance(1);
                    break;
                }

                if (reader.Peek(offset) is LeftParenthesisToken)
                    level++;

                if (reader.Peek(offset) is RightParenthesisToken)
                    level--;

                offset++;
            }
        }

        var branch = reader.CreateBranch();
        
        if (expr is null && (!TryReadExpression(reader, out expr) || expr is null))
        {
            if (!isMap && branch.Peek() is LeftParenthesisToken)
            {
                // Maybe it is a list?
                var level = 0;
            
                var innerReader = branch.CreateBranchUntil(token =>
                {
                    switch (token)
                    {
                        case RightParenthesisToken when --level == 0:
                            return true;
                        case LeftParenthesisToken:
                            level++;
                            break;
                    }
                    return false;
                });
            
                innerReader.Advance(1);
                
                while (innerReader.Peek() is WhitespaceToken)
                    innerReader.Advance(1);

                if (TryParse(innerReader, out expr) && expr is not null)
                {
                    branch.Advance(innerReader.AdvancedLength + (innerReader.Peek() is null ? 0 : 1));
                    reader = branch;
                }
            }

            if (expr is null)
            {
                result = null;
                return false;
            }
        }

        var expressions = new List<ISyntaxElement> { expr };
        List<ISyntaxElement>? commaSeparatedExpressions = null;
        ISyntaxElement? mapKey = null;

        do
        {
            while (reader.Peek() is WhitespaceToken)
                reader.Advance(1);

            if (reader.Peek() is LeftParenthesisToken)
            {
                var level = 0;
                var innerReader = reader.CreateBranchUntil(token =>
                {
                    switch (token)
                    {
                        case RightParenthesisToken when --level == 0:
                            return true;
                        case LeftParenthesisToken:
                            level++;
                            break;
                    }
                    return false;
                });
                
                innerReader.Advance(1);
                
                while (innerReader.Peek() is WhitespaceToken)
                    innerReader.Advance(1);
                
                if (TryParse(innerReader, out var innerResult) && innerResult is { } innerList)
                {
                    expressions.Add(innerList);
                    expr = innerList;
                    reader.Advance(innerReader.AdvancedLength + (innerReader.Peek() is null ? 0 : 1));
                    continue;
                }
            }
            
            if (TryReadExpression(reader, out var nextExpr) && nextExpr is not null)
            {
                expressions.Add(nextExpr);
                expr = nextExpr;
                continue;
            }

            if (reader.Peek() is CommaToken)
            {
                reader.Advance(1);
                if (mapKey is not null)
                {
                    expressions.Remove(expr);
                    expressions.Add(new MapEntryExpression(mapKey, expr));
                    mapKey = null;
                }
                commaSeparatedExpressions ??= new List<ISyntaxElement>();
                var itemExpr = expressions.Count == 1
                    ? expressions.First()
                    : new ExpressionList(expressions, ' ');
                commaSeparatedExpressions.Add(itemExpr);
                expressions = new List<ISyntaxElement>();
                continue;
            }

            if (reader.Peek() is ColonToken)
            {
                reader.Advance(1);
                if (mapKey is not null)
                    throw new ParseException("Unexpected colon after map key definition");
                mapKey = expr;
                expressions.Remove(expr);
                continue;
            }
            
            break;
        } while (true);

        if (isMap)
        {
            if (reader.Peek() is not RightParenthesisToken)
                throw new ParseException("Missing closing bracket for a map expression");
            reader.Advance(1);
        }
        
        if (mapKey is not null)
        {
            expressions.Remove(expr);
            expressions.Add(new MapEntryExpression(mapKey, expr));
        }

        if (commaSeparatedExpressions is not null)
        {
            var itemExpr = expressions.Count == 1
                ? expressions.First()
                : new ExpressionList(expressions, ' ');
            commaSeparatedExpressions.Add(itemExpr);
            if (commaSeparatedExpressions.All(item => item is MapEntryExpression))
                result = new MapExpression(commaSeparatedExpressions.OfType<MapEntryExpression>().ToList());
            else
                result = new ExpressionList(commaSeparatedExpressions, ',');
            return true;
        }

        if (expressions.Count > 1)
        {
            result = new ExpressionList(expressions, ' ');
            return true;
        }
        
        if (expressions.Count == 1)
        {
            result = expr;
            return true;
        }

        result = null;
        return false;
    }
    
    internal bool TryReadExpression(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is DelimToken { Value: '-' or '+' } opToken)
        {
            reader.Advance(1);
            if (!TryReadExpression(reader, out var inner))
            {
                result = null;
                return false;
            }

            result = new UnaryExpression(inner ?? throw new ParseException("Not an element"), opToken.Value.ToString());
            return true;
        }

        ISyntaxElement? left = null;

        if (reader.Peek() is LeftParenthesisToken)
        {
            reader.Advance(1);
            if (!TryReadExpression(reader, out left))
            {
                result = null;
                return false;
            }

            if (reader.Peek() is not RightParenthesisToken)
            {
                result = null;
                return false;
            }
                
            reader.Advance(1);
            
            while (reader.Peek() is WhitespaceToken)
                reader.Advance(1);
        }
        
        left ??= ReadItem(reader);

        if (left is null)
        {
            result = null;
            return false;
        }

        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        if (reader.Peek() is SemicolonToken or null)
        {
            reader.Advance(1);
            result = left;
            return true;
        }
        
        var op = ReadOperator(reader);

        if (op is "...")
        {
            left = new SpreadExpression(left);
            op = ReadOperator(reader);
        }

        if (op is not null)
        {
            while (reader.Peek() is WhitespaceToken)
                reader.Advance(1);

            var expressionParser = new ExpressionParser();

            if (expressionParser.TryReadExpression(reader, out var right))
            {
                result = new BinaryExpression(left,
                    right ?? throw new ParseException("Not an element"),
                    op);
                return true;
            }
        }

        result = left;
        return true;
    }

    private static string? ReadOperator(TokenReader reader)
    {
        var result = reader.Peek() switch
        {
            DelimToken { Value: '=' } when reader.Peek(1) is DelimToken { Value: '=' } => "==",
            DelimToken { Value: '!' } when reader.Peek(1) is DelimToken { Value: '=' } => "!=",
            DelimToken { Value: '+' } when reader.Peek(1) is DelimToken { Value: '=' } => "+=",
            DelimToken { Value: '-' } when reader.Peek(1) is DelimToken { Value: '=' } => "-=",
            DelimToken { Value: '.' } when
                reader.Peek(1) is DelimToken { Value: '.' }
                && reader.Peek(2) is DelimToken { Value: '.' } => "...",
            DelimToken { Value: var delimChar } when reader.Peek(1) is WhitespaceToken => delimChar.ToString(),
            IdentToken { Value: "and" or "or" } id => id.Value, 
            _ => null
        };
        if (result == null)
            return null;
        reader.Advance(result.Length);
        return result;
    }

    private ISyntaxElement? ReadItem(TokenReader reader)
    {
        var (result, advance) = reader.Peek() switch
        {
            DelimToken { Value: '$' } => (ReadVariable(reader), 0),
            FunctionToken functionToken => (ReadFunctionCall(reader, functionToken), 0),
            StringToken { Value: var stringValue } => (new StringLiteral(stringValue), 1),
            DimensionToken { Value: var numValue, Unit: var unit, Type: var numType } => (new DimensionLiteral(numValue,
                unit, numType is NumberType.Integer), 1),
            NumberToken { Value: var numValue, Type: var numType } => (new NumberLiteral(numValue,
                numType is NumberType.Integer), 1),
            PercentageToken { Value: var numValue } => (new PercentageLiteral(numValue), 1),
            HashToken { Value: var hashValue } => (new ColorLiteral(hashValue), 1),
            IdentToken { Value: var identValue } when reader.Peek(1) is DelimToken { Value: '.' } => (ReadFunctionCall(reader, identValue), 0),
            IdentToken { Value: var identValue } when identValue.ToLower() is "true" => (new BooleanLiteral(true), 1),
            IdentToken { Value: var identValue } when identValue.ToLower() is "false" => (new BooleanLiteral(false), 1),
            IdentToken { Value: var identValue } when identValue.ToLower() is "none" => (new NoneLiteral(), 1),
            IdentToken { Value: var identValue } when identValue.ToLower() is "null" => (new NullLiteral(), 1),
            IdentToken { Value: "in" } => (null, 0),
            IdentToken { Value: "to" } => (null, 0),
            IdentToken { Value: "through" } => (null, 0),
            IdentToken { Value: var identValue } => (new Identifier(identValue), 1),
            _ => (null, 0)
        };
        reader.Advance(advance);
        return result;
    }

    private ISyntaxElement ReadVariable(TokenReader reader)
    {
        reader.Advance(1);
        if (reader.Peek() is not IdentToken identToken)
            throw new ParseException($"Unexpected token {reader.Peek()} in a variable");
        reader.Advance(1);
        return new PlainVariable(identToken.Value);
    }

    private ISyntaxElement ReadFunctionCall(TokenReader reader, FunctionToken functionToken)
    {
        reader.Advance(1);
        var argumentListParser = new ArgumentListParser { Call = true };
        if (!argumentListParser.TryParse(reader, out var listResult))
            throw new ParseException($"Failed to parse function call arguments for {functionToken.Value}");
        return new CallExpression(new Identifier(functionToken.Value), listResult as CallArgumentList ?? throw new ParseException("Not an argument list"));
    }
    
    private ISyntaxElement ReadFunctionCall(TokenReader reader, string ns)
    {
        reader.Advance(2);
        if (reader.Peek() is IdentToken { Value: var ident } && reader.Peek(1) is DelimToken { Value: '.' })
            return ReadFunctionCall(reader, ns + "." + ident);
        if (reader.Peek() is not FunctionToken functionToken)
            throw new ParseException("Not a function token");
        reader.Advance(1);
        var argumentListParser = new ArgumentListParser { Call = true };
        if (!argumentListParser.TryParse(reader, out var listResult))
            throw new ParseException($"Failed to parse function call arguments for {functionToken.Value}");
        return new CallExpression(new Identifier(ns + "." + functionToken.Value), listResult as CallArgumentList ?? throw new ParseException("Not an argument list"));
    }
}