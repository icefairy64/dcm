using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public record AtRule(IEnumerable<ISassToken> Tokens, string Name, IEnumerable<IComponentValue> Prelude, SimpleBlock Value) : IParsedTokenGroup;

public record ImportStatement(ISyntaxElement Target) : Node(new [] { Target });

public record IncludeStatement(Identifier Target, CallArgumentList? Arguments)
    : Node(Arguments is null
        ? new [] { Target }
        : new ISyntaxElement[] { Target, Arguments });
        
public record ErrorStatement(ISyntaxElement Message) : Node(new [] { Message });

public record WarnStatement(ISyntaxElement Message) : Node(new [] { Message });

public record DebugStatement(ISyntaxElement Message) : Node(new [] { Message });