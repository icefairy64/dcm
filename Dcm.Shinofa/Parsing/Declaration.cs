using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public record Declaration(IEnumerable<ISassToken> Tokens, string Name, IEnumerable<IComponentValue> Value, bool Important) : IParsedTokenGroup;