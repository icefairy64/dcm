using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class RuleDeclarationParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        var selectorList = SelectorUtils.ReadSelectorList(reader);
        if (selectorList is null)
        {
            result = null;
            return false;
        }
        
        if (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        if (reader.Peek() is not LeftCurlyBracketToken)
        {
            result = null;
            return false;
        }
        
        var bodyParser = new BodyParser();
        if (!bodyParser.TryParse(reader, out var body))
        {
            result = null;
            return false;
        }

        if (body is not Body narrowedBody)
            throw new Exception();

        result = new RuleDeclaration(selectorList, narrowedBody);
        return true;
    }
}