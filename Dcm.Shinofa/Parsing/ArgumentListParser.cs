using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class ArgumentListParser : IParser
{
    public bool Call { get; set; }
    
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        return Call
            ? TryParseCall(reader, out result)
            : TryParseDecl(reader, out result);
    }
    
    public bool TryParseDecl(TokenReader reader, out ISyntaxElement? result)
    {
        var arguments = new List<Argument>();
        
        while (true)
        {
            if (ReadDeclArgument(reader) is { } argument)
            {
                arguments.Add(argument);
            }
            else
            {
                result = new ArgumentList(arguments);
                return true;
            }
        }
    }
    
    public bool TryParseCall(TokenReader reader, out ISyntaxElement? result)
    {
        var arguments = new List<CallArgument>();
        
        while (true)
        {
            if (ReadCallArgument(reader) is { } argument)
            {
                arguments.Add(argument);
            }
            else
            {
                result = new CallArgumentList(arguments);
                return true;
            }
        }
    }

    private Argument? ReadDeclArgument(TokenReader reader)
    {
        if (reader.Peek() is RightParenthesisToken)
        {
            reader.Advance(1);
            return null;
        }

        if (reader.Peek() is CommaToken)
            reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken or CssSingleLineCommentToken or CssCommentToken)
            reader.Advance(1);

        var isVariable = reader.Peek() is DelimToken { Value: '$' };
        
        if (isVariable)
            reader.Advance(1);

        if (reader.Peek() is not IdentToken { Value: var varName })
            throw new ParseException($"Unexpected token {reader.Peek()}")
            {
                Tokens = new Dictionary<string, ISassToken?>
                {
                    { "Token", reader.Peek() }
                }
            };

        ISyntaxElement variable = isVariable ? new PlainVariable(varName) : new Identifier(varName);
        
        reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var isSpread = false;

        if (reader.Peek() is DelimToken { Value: '.' } && reader.Peek(1) is DelimToken { Value: '.' }
                                                       && reader.Peek(2) is DelimToken { Value: '.' })
        {
            isSpread = true;
            reader.Advance(3);
        }

        if (reader.Peek() is not ColonToken)
            return new Argument(variable, null, isSpread);
        
        reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var expressionTokens = new List<ISassToken>();

        while (reader.Peek() is not (CommaToken or RightParenthesisToken or null))
        {
            expressionTokens.Add(reader.Peek()!);
            reader.Advance(1);
        }
        
        var expressionParser = new ExpressionParser();
        var expressionReader = new TokenReader(expressionTokens);

        if (!expressionParser.TryParse(expressionReader, out var exprElement) || exprElement is null)
            throw new ParseException("Could not parse value expression");

        return new Argument(variable, exprElement, isSpread);
    }
    
    private CallArgument? ReadCallArgument(TokenReader reader)
    {
        if (reader.Peek() is null)
            return null;
        
        if (reader.Peek() is RightParenthesisToken or SemicolonToken)
        {
            reader.Advance(1);
            return null;
        }

        if (reader.Peek() is CommaToken)
            reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        PlainVariable? arg = null;

        if (reader.Peek() is DelimToken { Value: '$' } && reader.Peek(1) is IdentToken { Value: var argName } && reader.Peek(2) is ColonToken)
        {
            // Has an argument name
            reader.Advance(3);
            arg = new PlainVariable(argName);
            
            if (reader.Peek() is WhitespaceToken)
                reader.Advance(1);
        }

        var expressionTokens = new List<ISassToken>();
        var level = 0;
        while (reader.Peek() is not (RightParenthesisToken or CommaToken or null) || level > 0)
        {
            expressionTokens.Add(reader.Peek() ?? throw new ParseException("Unexpected EOF")
            {
                Tokens = new Dictionary<string, ISassToken?>
                {
                    { "Expression start", expressionTokens.First() },
                    { "Expression end", expressionTokens.Last() }
                }
            });
            if (reader.Peek() is LeftParenthesisToken or FunctionToken)
                level++;
            if (reader.Peek() is RightParenthesisToken)
                level--;
            reader.Advance(1);
        }

        /*if (reader.Peek() is RightParenthesisToken && expressionTokens.Count > 0 &&
            expressionTokens[0] is FunctionToken)
        {
            expressionTokens.Add(reader.Peek() ?? throw new ParseException("Unexpected EOF"));
            reader.Advance(1);
        }*/

        var expressionParser = new ExpressionParser();
        var expressionReader = new TokenReader(expressionTokens);
        return expressionParser.TryParse(expressionReader, out var result) ? new CallArgument(arg, result!) : null;
    }
}