using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class IncludeStatementParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not AtKeywordToken { Value: "include" })
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);
        
        if (reader.Peek() is WhitespaceToken)
            reader.Advance(1);
        
        if (reader.Peek() is IdentToken { Value: var identValue } && reader.Peek(1) is SemicolonToken)
        {
            reader.Advance(2);
            result = new IncludeStatement(new Identifier(identValue), null);
            return true;
        }
        
        // Has arguments

        if (reader.Peek() is not FunctionToken { Value: var target })
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);

        var listParser = new ArgumentListParser
        {
            Call = true
        };

        if (!listParser.TryParse(reader, out var listElement) || listElement is null)
        {
            result = null;
            return false;
        }

        if (listElement is not CallArgumentList argumentList)
        {
            result = null;
            return false;
        }

        if (reader.Peek() is SemicolonToken)
            reader.Advance(1);

        result = new IncludeStatement(new Identifier(target), argumentList);
        return true;
    }
}