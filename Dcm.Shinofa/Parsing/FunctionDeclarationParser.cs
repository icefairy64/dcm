using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class FunctionDeclarationParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        var isMixin = false;
        
        switch (reader.Peek())
        {
            case AtKeywordToken { Value: "function" }:
                break;
            case AtKeywordToken { Value: "mixin" }:
                isMixin = true;
                break;
            default:
                result = null;
                return false;
        }
        
        reader.Advance(1);

        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var (name, argumentList) = reader.Peek() switch
        {
            FunctionToken => ReadSignatureWithAttributes(reader),
            IdentToken when reader.Peek(1) is WhitespaceToken && reader.Peek(2) is LeftParenthesisToken =>
                ReadSignatureWithAttributes(reader),
            IdentToken => (ReadSignatureWithoutAttributes(reader), null),
            _ => throw new ParseException($"Unexpected token {reader.Peek()}")
        };
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        if (reader.Peek() is not LeftCurlyBracketToken)
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);

        var bodyParser = new BodyParser();
        if (!bodyParser.TryParse(reader, out var body))
        {
            result = null;
            return false;
        }

        if (body is Body narrowedBody)
        {
            result = isMixin
                ? new MixinDeclaration(new Identifier(name), argumentList, narrowedBody)
                : new FunctionDeclaration(new Identifier(name), argumentList ?? throw new ParseException("No argument list for function declaration"), narrowedBody);
            return true;
        }

        result = null;
        return false;
    }

    private static string ReadSignatureWithoutAttributes(TokenReader reader)
    {
        if (reader.Peek() is not IdentToken identToken)
            throw new ParseException("Not an ident token");
        
        reader.Advance(1);
        
        return identToken.Value;
    }

    private static (string, ArgumentList?) ReadSignatureWithAttributes(TokenReader reader)
    {
        var fnName = "";

        if (reader.Peek() is FunctionToken functionToken)
        {
            fnName = functionToken.Value;
            reader.Advance(1);
        }
        else if (reader.Peek() is IdentToken identToken && reader.Peek(1) is WhitespaceToken && reader.Peek(2) is LeftParenthesisToken)
        {
            fnName = identToken.Value;
            reader.Advance(3);
        }

        var argumentListParser = new ArgumentListParser();

        if (!argumentListParser.TryParse(reader, out var listEl))
            throw new ParseException("Could not read mixin declaration signature");
        
        if (listEl is not ArgumentList argumentList)
            throw new ParseException("Not an argument list");

        return (fnName, argumentList);
    }
}