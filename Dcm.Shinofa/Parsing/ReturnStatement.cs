namespace Breezy.Dcm.Shinofa.Parsing;

public record ReturnStatement(ISyntaxElement Expression) : Node(new List<ISyntaxElement> { Expression });