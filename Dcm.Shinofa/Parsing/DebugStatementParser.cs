using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class DebugStatementParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not AtKeywordToken { Value: "debug" or "warn" or "error" } atToken)
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);

        if (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var expressionParser = new ExpressionParser();

        if (!expressionParser.TryParse(reader, out var exprElement) || exprElement is null)
        {
            result = null;
            return false;
        }
        
        if (reader.Peek() is SemicolonToken)
            reader.Advance(1);

        result = atToken.Value switch
        {
            "debug" => new DebugStatement(exprElement),
            "warn" => new WarnStatement(exprElement),
            "error" => new ErrorStatement(exprElement),
            _ => throw new ParseException($"Unexpected at-keyword {atToken.Value}")
        };

        return true;
    }
}