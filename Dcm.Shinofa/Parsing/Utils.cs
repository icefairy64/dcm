using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public static class Utils
{
    public static IComponentValue ReadComponentValue(TokenReader reader)
    {
        return reader.Peek() switch
        {
            LeftCurlyBracketToken or LeftSquareBracketToken or LeftParenthesisToken => ReadSimpleBlock(reader),
            FunctionToken => ReadFunction(reader),
            { } token => ReadSingleTokenComponentValue(reader, token),
            _ => throw new ParseException("EOF while reading component value")
        };
    }

    public static SimpleBlock ReadSimpleBlock(TokenReader reader)
    {
        var startToken = reader.Peek();
        if (startToken is not (LeftCurlyBracketToken or LeftSquareBracketToken or LeftParenthesisToken))
            throw new ParseException("Not a block");
        
        reader.Advance(1);
        var list = new List<IComponentValue>();

        do
        {
            switch (reader.Peek())
            {
                case RightCurlyBracketToken when startToken is LeftCurlyBracketToken:
                    reader.Advance(1);
                    return new SimpleBlock(reader.AdvancedTokens, startToken, list);
                
                case RightSquareBracketToken when startToken is LeftSquareBracketToken:
                    reader.Advance(1);
                    return new SimpleBlock(reader.AdvancedTokens, startToken, list);
                
                case RightParenthesisToken when startToken is LeftParenthesisToken:
                    reader.Advance(1);
                    return new SimpleBlock(reader.AdvancedTokens, startToken, list);
                
                case null:
                    return new SimpleBlock(reader.AdvancedTokens, startToken, list);
                
                default:
                    list.Add(ReadComponentValue(reader));
                    break;
            }
        } while (true);
    }
    
    public static Function ReadFunction(TokenReader reader)
    {
        var value = new List<IComponentValue>();
        if (reader.Peek() is not FunctionToken name)
            throw new ParseException("Not a function");
        reader.Advance(1);

        do
        {
            switch (reader.Peek())
            {
                case null:
                    return new Function(reader.AdvancedTokens, name.Value, value);
                
                case RightParenthesisToken:
                    reader.Advance(1);
                    return new Function(reader.AdvancedTokens, name.Value, value);
                
                default:
                    value.Add(ReadComponentValue(reader));
                    break;
            }
        } while (true);
    }

    public static SingleTokenComponentValue ReadSingleTokenComponentValue(TokenReader reader, ISassToken token)
    {
        reader.Advance(1);
        return new SingleTokenComponentValue(token);
    }

    public static Declaration? ReadDeclaration(TokenReader reader)
    {
        if (reader.Peek() is not IdentToken ident)
            throw new ParseException("Not a declaration");
        
        reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        if (reader.Peek() is not ColonToken)
            return null;

        var important = false;
        var value = new List<IComponentValue>();

        while (reader.Peek() is not null)
            value.Add(ReadComponentValue(reader));

        if (value.Count > 2 && value[-2] is SingleTokenComponentValue { Value: DelimToken { Value: '!' } } &&
            value[-1] is SingleTokenComponentValue { Value: IdentToken { Value: var lastIdentValue } } &&
            lastIdentValue.Equals("important", StringComparison.InvariantCultureIgnoreCase))
        {
            important = true;
            value.RemoveAt(value.Count - 1);
            value.RemoveAt(value.Count - 1);
        }
        
        while (value.Last() is SingleTokenComponentValue { Value: WhitespaceToken })
            value.RemoveAt(value.Count - 1);

        return new Declaration(reader.AdvancedTokens, ident.Value, value, important);
    }

    public static IEnumerable<IParsedTokenGroup> ReadDeclarationList(TokenReader reader)
    {
        var declarations = new List<IParsedTokenGroup>();

        do
        {
            switch (reader.Peek())
            {
                case WhitespaceToken or SemicolonToken:
                    reader.Advance(1);
                    break;
                
                case null:
                    return declarations;
                
                case AtKeywordToken:
                    declarations.Add(ReadAtRule(reader));
                    break;
                
                case IdentToken:
                    var list = new List<ISassToken>();
                    do
                    {
                        list.Add(reader.Peek()!);
                        reader.Advance(1);
                    } while (reader.Peek() is not (SemicolonToken or null));

                    var childReader = new TokenReader(list);
                    if (ReadDeclaration(childReader) is { } declaration)
                        declarations.Add(declaration);
                    
                    break;
                
                default:
                    // TODO: re-read the spec - we need to check the next input token
                    ReadComponentValue(reader);
                    return declarations;
            }
        } while (true);
    }

    public static IEnumerable<IParsedTokenGroup> ReadStyleBlockContents(TokenReader reader)
    {
        var declarations = new List<IParsedTokenGroup>();
        var rules = new List<IParsedTokenGroup>();
        
        do
        {
            switch (reader.Peek())
            {
                case WhitespaceToken or SemicolonToken:
                    reader.Advance(1);
                    break;
                
                case null:
                    declarations.AddRange(rules);
                    return declarations;
                
                case AtKeywordToken:
                    rules.Add(ReadAtRule(reader));
                    break;
                
                case IdentToken:
                    var list = new List<ISassToken>();
                    do
                    {
                        list.Add(reader.Peek()!);
                        reader.Advance(1);
                    } while (reader.Peek() is not (SemicolonToken or null));

                    var childReader = new TokenReader(list);
                    if (ReadDeclaration(childReader) is { } declaration)
                        declarations.Add(declaration);
                    
                    break;
                
                case DelimToken { Value: '&' }:
                    if (ReadQualifiedRule(reader) is { } qualifiedRule)
                        rules.Add(qualifiedRule);
                    break;
                    
                default:
                    // TODO: re-read the spec - we need to check the next input token
                    ReadComponentValue(reader);
                    declarations.AddRange(rules);
                    return declarations;
            }
        } while (true);
    }

    public static AtRule ReadAtRule(TokenReader reader)
    {
        if (reader.Peek() is not AtKeywordToken atToken)
            throw new ParseException("Not an at-rule");

        var prelude = new List<IComponentValue>();
        SimpleBlock? value = null;

        do
        {
            switch (reader.Peek())
            {
                case SemicolonToken:
                    reader.Advance(1);
                    if (value is null)
                        throw new ParseException($"No value for at-rule {atToken}");
                    return new AtRule(reader.AdvancedTokens, atToken.Value, prelude, value);
                
                case null:
                    if (value is null)
                        throw new ParseException($"No value for at-rule {atToken}");
                    return new AtRule(reader.AdvancedTokens, atToken.Value, prelude, value);
                
                case LeftCurlyBracketToken:
                    value = ReadSimpleBlock(reader);
                    return new AtRule(reader.AdvancedTokens, atToken.Value, prelude, value);
                
                default:
                    prelude.Add(ReadComponentValue(reader));
                    break;
            }
        } while (true);
    }
    
    public static QualifiedRule? ReadQualifiedRule(TokenReader reader)
    {
        if (reader.Peek() is not AtKeywordToken atToken)
            throw new ParseException("Not an at-rule");

        var prelude = new List<IComponentValue>();
        SimpleBlock? value = null;

        do
        {
            switch (reader.Peek())
            {
                case null:
                    return null;
                
                case LeftCurlyBracketToken:
                    value = ReadSimpleBlock(reader);
                    return new QualifiedRule(reader.AdvancedTokens, prelude, value);
                
                default:
                    prelude.Add(ReadComponentValue(reader));
                    break;
            }
        } while (true);
    }

    public static IEnumerable<IParsedTokenGroup> ReadRuleList(TokenReader reader, bool topLevel)
    {
        var rules = new List<IParsedTokenGroup>();

        do
        {
            switch (reader.Peek())
            {
                case WhitespaceToken:
                    reader.Advance(1);
                    break;
                
                case null:
                    return rules;
                
                case CdcToken or CdoToken:
                    if (!topLevel && ReadQualifiedRule(reader) is { } qualifiedRule)
                        rules.Add(qualifiedRule);
                    break;
                
                case AtKeywordToken:
                    rules.Add(ReadAtRule(reader));
                    break;
                
                default:
                    if (ReadQualifiedRule(reader) is { } qualifiedRule2)
                        rules.Add(qualifiedRule2);
                    break;
            }
        } while (true);
    }
}