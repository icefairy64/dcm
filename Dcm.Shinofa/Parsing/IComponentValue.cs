using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public interface IComponentValue : IParsedTokenGroup
{
    
}

public record SingleTokenComponentValue(ISassToken Value) : IComponentValue
{
    public IEnumerable<ISassToken> Tokens => new[] { Value };
}