namespace Breezy.Dcm.Shinofa.Parsing;

public record ForStatement(PlainVariable Variable, ISyntaxElement From, ISyntaxElement To, Body Body, bool Inclusive)
    : Node(new [] { Variable, From, To, Body });