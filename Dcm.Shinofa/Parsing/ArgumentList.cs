namespace Breezy.Dcm.Shinofa.Parsing;

public record ArgumentList(IReadOnlyList<Argument> Arguments) : Node(Arguments);

public record CallArgumentList(IReadOnlyList<CallArgument> Arguments) : Node(Arguments);

public record Argument(ISyntaxElement Name, ISyntaxElement? DefaultValue, bool IsSpread) 
    : Node(new List<ISyntaxElement?>{ Name, DefaultValue }.OfType<ISyntaxElement>().ToList());
    
public record CallArgument(ISyntaxElement? Name, ISyntaxElement Value) 
    : Node(new List<ISyntaxElement?>{ Name, Value }.OfType<ISyntaxElement>().ToList());