using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public interface ISelector : ISyntaxElement
{
    
}

public interface IParsedSelector : ISelector, IParsedTokenGroup
{
    
}

public record ClassSelector(IEnumerable<ISassToken> Tokens, string ClassName) : IParsedSelector;

public record TypeSelector(IEnumerable<ISassToken> Tokens, string TagName) : IParsedSelector;

public record IdSelector(IEnumerable<ISassToken> Tokens, string ElementId) : IParsedSelector;

public record NestingSelector(IEnumerable<ISassToken> Tokens) : IParsedSelector;

public record UniversalSelector(IEnumerable<ISassToken> Tokens) : IParsedSelector;

public record AttributeSelector(
    IEnumerable<ISassToken> Tokens,
    string AttributeName,
    string? Operator,
    string? Value,
    bool? CaseSensitive) : IParsedSelector;

// Pseudo-classes
    
public record PseudoClassSelector(IEnumerable<ISassToken> Tokens, string Name) : IParsedSelector;

public record ComplexPseudoClassSelector(IEnumerable<ISassToken> Tokens, string Name, ISelector Body) :
    Node(new ISyntaxElement[] { Body }),
    IParsedSelector;

public record ContentfulPseudoClassSelector(IEnumerable<ISassToken> Tokens, string Name, IEnumerable<ISassToken> BodyTokens) : PseudoClassSelector(Tokens, Name);

// Pseudo-elements

public record PseudoElementSelector(IEnumerable<ISassToken> Tokens, string Name) : IParsedSelector;

public record ComplexPseudoElementSelector(IEnumerable<ISassToken> Tokens, string Name, ISelector Body) :
    Node(new ISyntaxElement[] { Body }),
    IParsedSelector;

// Combinators

public record CompoundSelector(IReadOnlyList<ISelector> Selectors) :
    Node(Selectors),
    ISelector;

public record ChildSelector(ISelector Parent, ISelector Child) :
    Node(new ISyntaxElement[] { Parent, Child }),
    ISelector;

public record DescendantSelector(ISelector Parent, ISelector Child) :
    Node(new ISyntaxElement[] { Parent, Child }),
    ISelector;

public record NextSiblingSelector(ISelector First, ISelector Next) :
    Node(new ISyntaxElement[] { First, Next }),
    ISelector;

public record SubsequentSiblingSelector(ISelector First, ISelector Next) :
    Node(new ISyntaxElement[] { First, Next }),
    ISelector;

public record ListSelector(IReadOnlyList<ISelector> Selectors) :
    Node(Selectors),
    ISelector;