using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class AtRuleParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not AtKeywordToken)
        {
            result = null;
            return false;
        }

        result = Utils.ReadAtRule(reader);
        return true;
    }
}