using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public record Function(IEnumerable<ISassToken> Tokens, string Name, IEnumerable<IComponentValue> Value) : IComponentValue;