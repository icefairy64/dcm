using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class ImportStatementParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not AtKeywordToken { Value: "import" })
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);
        
        if (reader.Peek() is WhitespaceToken)
            reader.Advance(1);
        
        ISyntaxElement? target = reader.Peek() switch
        {
            UrlToken { Value: var value } =>
                new CallExpression(new Identifier("url"), new CallArgumentList(new [] { new CallArgument(null, new StringLiteral(value)) })),
            FunctionToken { Value: "url" } when reader.Peek(1) is StringToken { Value: var url } =>
                new CallExpression(new Identifier("url"), new CallArgumentList(new [] { new CallArgument(null, new StringLiteral(url)) })),
            StringToken { Value: var value } =>
                new StringLiteral(value),
            _ => null
        };

        if (target is null)
        {
            result = null;
            return false;
        }
        
        if (reader.Peek() is FunctionToken)
            reader.Advance(2);
        
        reader.Advance(1);

        if (reader.Peek() is not SemicolonToken)
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);

        result = new ImportStatement(target);
        return true;
    }
}