using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class ForStatementParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not AtKeywordToken { Value: "for" })
        {
            result = null;
            return false;
        }
        
        reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);
        
        // Read variable

        if (reader.Peek() is not DelimToken { Value: '$' })
            throw new ParseException("Not a variable");
        
        reader.Advance(1);

        if (reader.Peek() is not IdentToken { Value: var varName })
            throw new ParseException("Not a variable");
        
        reader.Advance(1);
        
        // Read "from"
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        if (reader.Peek() is not IdentToken { Value: "from" })
            throw new ParseException("Not a 'from' declaration");
        
        reader.Advance(1);
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var exprParser = new ExpressionParser();
        if (!exprParser.TryReadExpression(reader, out var fromExpr) || fromExpr is null)
            throw new ParseException("Invalid 'from' declaration");
        
        // Read "through" / "to"
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        ISyntaxElement toExpr;
        bool inclusive;

        if (reader.Peek() is IdentToken { Value: "through" })
        {
            reader.Advance(1);
            while (reader.Peek() is WhitespaceToken)
                reader.Advance(1);
            if (!exprParser.TryParse(reader, out var readExpr) || readExpr is null)
                throw new ParseException("Invalid 'through' declaration");
            inclusive = true;
            toExpr = readExpr;
        }
        else if (reader.Peek() is IdentToken { Value: "to" })
        {
            reader.Advance(1);
            while (reader.Peek() is WhitespaceToken)
                reader.Advance(1);
            if (!exprParser.TryParse(reader, out var readExpr) || readExpr is null)
                throw new ParseException("Invalid 'to' declaration");
            inclusive = false;
            toExpr = readExpr;
        }
        else
        {
            throw new ParseException($"Not a 'through' or 'to' declaration: {reader.Peek()}");
        }
        
        // Read body
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var bodyParser = new BodyParser();
        if (!bodyParser.TryParse(reader, out var body) || body is null)
            throw new ParseException("Invalid body");

        result = new ForStatement(new PlainVariable(varName), fromExpr, toExpr,
            body as Body ?? throw new ParseException("Invalid body"), inclusive);
        return true;
    }
}