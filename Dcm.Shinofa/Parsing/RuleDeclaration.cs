using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public record RuleDeclaration(ISelector Selector, Body Body) : Node(new ISyntaxElement[] { Selector, Body });