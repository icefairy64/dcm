using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public record StringLiteral(string Value) : ISyntaxElement;
public record NumberLiteral(double Value, bool Integer) : ISyntaxElement;
public record DimensionLiteral(double Value, string Unit, bool Integer) : ISyntaxElement;
public record PercentageLiteral(double Value) : ISyntaxElement;
public record BooleanLiteral(bool Value) : ISyntaxElement;
public record ColorLiteral(string Value) : ISyntaxElement;
public record NoneLiteral() : ISyntaxElement;
public record NullLiteral() : ISyntaxElement;