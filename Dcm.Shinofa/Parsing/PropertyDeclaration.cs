namespace Breezy.Dcm.Shinofa.Parsing;

public record PropertyDeclaration(Identifier Property, ISyntaxElement Expression, bool Important)
    : Node(new ISyntaxElement[] { Property, Expression });