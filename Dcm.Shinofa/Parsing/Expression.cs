namespace Breezy.Dcm.Shinofa.Parsing;

public record Expression(IReadOnlyList<ISyntaxElement> Children) : Node(Children);

public record BinaryExpression(ISyntaxElement Left, ISyntaxElement Right, string Operator)
    : Expression(new List<ISyntaxElement> { Left, Right });
    
public record UnaryExpression(ISyntaxElement Operand, string Operator)
    : Expression(new List<ISyntaxElement> { Operand });

public record SpreadExpression(ISyntaxElement Operand)
    : Expression(new List<ISyntaxElement> { Operand });

public record CallExpression(ISyntaxElement Callee, CallArgumentList Arguments)
    : Expression(new List<ISyntaxElement> { Callee, Arguments });
    
public record ExpressionList(IReadOnlyList<ISyntaxElement> Expressions, char Separator)
    : Node(Expressions);
    
public record MapEntryExpression(ISyntaxElement Key, ISyntaxElement Value)
    : Expression(new [] { Key, Value });
    
public record MapExpression(IReadOnlyList<MapEntryExpression> Entries)
    : Expression(Entries);