using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class BodyParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is LeftCurlyBracketToken)
            reader.Advance(1);
        
        var prevReader = reader;
        reader = reader.CreateBranch();
        var rootParser = new RootParser();
        var declarations = new List<ISyntaxElement>();

        var lastKnownGoodToken = reader.Peek();
        
        while (reader.Peek() is not RightCurlyBracketToken)
        {
            if (reader.Peek() is WhitespaceToken)
            {
                reader.Advance(1);
                reader.Commit();
                continue;
            }

            var curToken = reader.Peek();
            
            if (!rootParser.TryParse(reader, out var declaration) || declaration is null)
                throw new ParseException($"Failed to parse a block-scoped declaration, token: {reader.Peek()}")
                {
                    Tokens = new Dictionary<string, ISassToken?>
                    {
                        { "Last good", lastKnownGoodToken },
                        { "Before parse", curToken },
                        { "Current", reader.PeekAt(reader.Position) ?? reader.PeekAt(reader.Position - 1) },
                    }
                };

            lastKnownGoodToken = curToken;
            
            reader.Commit();
            
            declarations.Add(declaration);
        }
        
        reader.Advance(1);
        
        prevReader.Advance(reader.Position - prevReader.Position);

        result = new Body(declarations);
        return true;
    }
}