using Breezy.Dcm.Shinofa.Tokenization;

namespace Breezy.Dcm.Shinofa.Parsing;

public class IfStatementParser : IParser
{
    public bool TryParse(TokenReader reader, out ISyntaxElement? result)
    {
        if (reader.Peek() is not (AtKeywordToken { Value: "if" } or IdentToken { Value: "if"}))
        {
            result = null;
            return false;
        }

        reader.Advance(1);

        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var expressionTokens = new List<ISassToken>();

        while (reader.Peek() is not LeftCurlyBracketToken)
        {
            expressionTokens.Add(reader.Peek() ?? throw new ParseException("EOF while parsing an expression"));
            reader.Advance(1);
        }
        
        reader.Advance(1);

        var expressionParser = new ExpressionParser();
        var expressionReader = new TokenReader(expressionTokens);
        if (!expressionParser.TryParse(expressionReader, out var expression) || expression is null)
        {
            result = null;
            return false;
        }
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        var bodyParser = new BodyParser();
        if (!bodyParser.TryParse(reader, out var body) || body is null)
        {
            result = null;
            return false;
        }
        
        while (reader.Peek() is WhitespaceToken)
            reader.Advance(1);

        if (reader.Peek() is AtKeywordToken { Value: "else" })
        {
            reader.Advance(1);
            
            while (reader.Peek() is WhitespaceToken)
                reader.Advance(1);

            if (reader.Peek() is IdentToken { Value: "if" })
            {
                var ifParser = new IfStatementParser();
                if (!ifParser.TryParse(reader, out var elseIf) || elseIf is null)
                {
                    result = null;
                    return false;
                }

                result = new IfStatement(expression, body as Body ?? throw new ParseException("Not a body"), elseIf);
                return true;
            }

            if (!bodyParser.TryParse(reader, out var elseBody) || elseBody is null)
            {
                result = null;
                return false;
            }
            
            result = new IfStatement(expression, body as Body ?? throw new ParseException("Not a body"), elseBody);
            return true;
        }
        
        result = new IfStatement(expression, body as Body ?? throw new ParseException("Not a body"), null);
        return true;
    }
}