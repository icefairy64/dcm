namespace Breezy.Dcm.Shinofa.Parsing;

public record FunctionDeclaration(Identifier Name, ArgumentList Arguments, Body Body)
    : Node(new List<ISyntaxElement> { Name, Arguments, Body });