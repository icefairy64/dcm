namespace Breezy.Dcm.Shinofa.Parsing;

public record MixinDeclaration(Identifier Name, ArgumentList? Arguments, Body Body)
    : Node(Arguments is null
        ? new List<ISyntaxElement> { Name, Body }
        : new List<ISyntaxElement> { Name, Arguments, Body });