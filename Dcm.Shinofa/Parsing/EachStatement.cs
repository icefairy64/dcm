namespace Breezy.Dcm.Shinofa.Parsing;

public record EachStatement(ExpressionList VariableList, ISyntaxElement Expression, Body Body)
    : Node(new [] { VariableList, Expression, Body });