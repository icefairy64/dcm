using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Processing;

public class ImportCollectorVisitor : ChainingSyntaxElementVisitor
{
    private readonly List<ISyntaxElement> FImports = new();

    public IReadOnlyList<ISyntaxElement> Imports => FImports.AsReadOnly();
    
    public ImportCollectorVisitor(SyntaxElementVisitor @delegate) : base(@delegate)
    {
    }

    public override void VisitImportStatement(ImportStatement importStatement)
    {
        FImports.Add(importStatement.Target);
        base.VisitImportStatement(importStatement);
    }
}