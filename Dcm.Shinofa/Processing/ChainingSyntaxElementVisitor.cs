using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Processing;

public class ChainingSyntaxElementVisitor : SyntaxElementVisitor
{
    public SyntaxElementVisitor Delegate { get; set; }

    public ChainingSyntaxElementVisitor(SyntaxElementVisitor @delegate)
    {
        Delegate = @delegate;
    }

    public override void VisitNoneLiteral(NoneLiteral noneLiteral)
    {
        Delegate.VisitNoneLiteral(noneLiteral);
    }

    public override void VisitStringLiteral(StringLiteral stringLiteral)
    {
        Delegate.VisitStringLiteral(stringLiteral);
    }

    public override void VisitNumberLiteral(NumberLiteral numberLiteral)
    {
        Delegate.VisitNumberLiteral(numberLiteral);
    }

    public override void VisitDimensionLiteral(DimensionLiteral dimensionLiteral)
    {
        Delegate.VisitDimensionLiteral(dimensionLiteral);
    }

    public override void VisitPercentageLiteral(PercentageLiteral percentageLiteral)
    {
        Delegate.VisitPercentageLiteral(percentageLiteral);
    }

    public override void VisitBooleanLiteral(BooleanLiteral booleanLiteral)
    {
        Delegate.VisitBooleanLiteral(booleanLiteral);
    }

    public override void VisitNullLiteral(NullLiteral nullLiteral)
    {
        Delegate.VisitNullLiteral(nullLiteral);
    }

    public override void VisitAtRule(AtRule atRule)
    {
        Delegate.VisitAtRule(atRule);
    }

    public override void VisitImportStatement(ImportStatement importStatement)
    {
        Delegate.VisitImportStatement(importStatement);
    }

    public override void VisitIncludeStatement(IncludeStatement includeStatement)
    {
        Delegate.VisitIncludeStatement(includeStatement);
    }

    public override void VisitErrorStatement(ErrorStatement errorStatement)
    {
        Delegate.VisitErrorStatement(errorStatement);
    }

    public override void VisitWarnStatement(WarnStatement warnStatement)
    {
        Delegate.VisitWarnStatement(warnStatement);
    }

    public override void VisitDebugStatement(DebugStatement debugStatement)
    {
        Delegate.VisitDebugStatement(debugStatement);
    }

    public override void VisitReturnStatement(ReturnStatement returnStatement)
    {
        Delegate.VisitReturnStatement(returnStatement);
    }

    public override void VisitUnaryExpression(UnaryExpression unaryExpression)
    {
        Delegate.VisitUnaryExpression(unaryExpression);
    }

    public override void VisitBinaryExpression(BinaryExpression binaryExpression)
    {
        Delegate.VisitBinaryExpression(binaryExpression);
    }

    public override void VisitCallExpression(CallExpression callExpression)
    {
        Delegate.VisitCallExpression(callExpression);
    }

    public override void VisitExpressionList(ExpressionList expressionList)
    {
        Delegate.VisitExpressionList(expressionList);
    }

    public override void VisitVariableDeclaration(VariableDeclaration variableDeclaration)
    {
        Delegate.VisitVariableDeclaration(variableDeclaration);
    }

    public override void VisitPlainVariable(PlainVariable plainVariable)
    {
        Delegate.VisitPlainVariable(plainVariable);
    }

    public override void VisitIdentifier(Identifier identifier)
    {
        Delegate.VisitIdentifier(identifier);
    }

    public override void VisitArgumentList(ArgumentList argumentList)
    {
        Delegate.VisitArgumentList(argumentList);
    }

    public override void VisitCallArgumentList(CallArgumentList callArgumentList)
    {
        Delegate.VisitCallArgumentList(callArgumentList);
    }

    public override void VisitArgument(Argument argument)
    {
        Delegate.VisitArgument(argument);
    }

    public override void VisitCallArgument(CallArgument callArgument)
    {
        Delegate.VisitCallArgument(callArgument);
    }

    public override void VisitSimpleBlock(SimpleBlock simpleBlock)
    {
        Delegate.VisitSimpleBlock(simpleBlock);
    }

    public override void VisitIfStatement(IfStatement ifStatement)
    {
        Delegate.VisitIfStatement(ifStatement);
    }

    public override void VisitBody(Body body)
    {
        Delegate.VisitBody(body);
    }

    public override void VisitSingleTokenComponentValue(SingleTokenComponentValue singleTokenComponentValue)
    {
        Delegate.VisitSingleTokenComponentValue(singleTokenComponentValue);
    }

    public override void VisitPropertyDeclaration(PropertyDeclaration propertyDeclaration)
    {
        Delegate.VisitPropertyDeclaration(propertyDeclaration);
    }

    public override void VisitMixinDeclaration(MixinDeclaration mixinDeclaration)
    {
        Delegate.VisitMixinDeclaration(mixinDeclaration);
    }

    public override void VisitFunctionDeclaration(FunctionDeclaration functionDeclaration)
    {
        Delegate.VisitFunctionDeclaration(functionDeclaration);
    }
}