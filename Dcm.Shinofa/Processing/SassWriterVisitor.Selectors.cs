using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Processing;

public partial class SassWriterVisitor
{
    public override void VisitClassSelector(ClassSelector classSelector)
    {
        Writer.Write('.');
        Writer.Write(classSelector.ClassName);
    }

    public override void VisitIdSelector(IdSelector idSelector)
    {
        Writer.Write('#');
        Writer.Write(idSelector.ElementId);
    }

    public override void VisitTypeSelector(TypeSelector typeSelector)
    {
        Writer.Write(typeSelector.TagName);
    }

    public override void VisitAttributeSelector(AttributeSelector attributeSelector)
    {
        Writer.Write('[');
        Writer.Write(attributeSelector.AttributeName);
        if (attributeSelector.Operator is { } op && attributeSelector.Value is { } val)
        {
            Writer.Write(op);
            Writer.Write('"');
            Writer.Write(val);
            Writer.Write('"');
        }
        Writer.Write(']');
    }

    public override void VisitNestingSelector(NestingSelector nestingSelector)
    {
        Writer.Write('&');
    }

    public override void VisitUniversalSelector(UniversalSelector universalSelector)
    {
        Writer.Write('*');
    }

    public override void VisitComplexPseudoClassSelector(ComplexPseudoClassSelector complexPseudoClassSelector)
    {
        Writer.Write(':');
        Writer.Write(complexPseudoClassSelector.Name);
        Writer.Write('(');
        Visit(complexPseudoClassSelector.Body);
        Writer.Write(')');
    }

    public override void VisitContentfulPseudoClassSelector(ContentfulPseudoClassSelector contentfulPseudoClassSelector)
    {
        Writer.Write(':');
        Writer.Write(contentfulPseudoClassSelector.Name);
        Writer.Write('(');
        foreach (var token in contentfulPseudoClassSelector.BodyTokens)
            Writer.Write(token.GetStringRepresentation());
        Writer.Write(')');
    }

    public override void VisitPseudoClassSelector(PseudoClassSelector pseudoClassSelector)
    {
        Writer.Write(':');
        Writer.Write(pseudoClassSelector.Name);
    }

    public override void VisitPseudoElementSelector(PseudoElementSelector pseudoElementSelector)
    {
        Writer.Write("::");
        Writer.Write(pseudoElementSelector.Name);
    }

    public override void VisitComplexPseudoElementSelector(ComplexPseudoElementSelector complexPseudoElementSelector)
    {
        Writer.Write("::");
        Writer.Write(complexPseudoElementSelector.Name);
        Writer.Write('(');
        Visit(complexPseudoElementSelector.Body);
        Writer.Write(')');
    }

    public override void VisitChildSelector(ChildSelector childSelector)
    {
        Visit(childSelector.Parent);
        Writer.Write(" > ");
        Visit(childSelector.Child);
    }

    public override void VisitDescendantSelector(DescendantSelector descendantSelector)
    {
        Visit(descendantSelector.Parent);
        Writer.Write(' ');
        Visit(descendantSelector.Child);
    }

    public override void VisitNextSiblingSelector(NextSiblingSelector nextSiblingSelector)
    {
        Visit(nextSiblingSelector.First);
        Writer.Write(" + ");
        Visit(nextSiblingSelector.Next);
    }

    public override void VisitSubsequentSiblingSelector(SubsequentSiblingSelector subsequentSiblingSelector)
    {
        Visit(subsequentSiblingSelector.First);
        Writer.Write(" ~ ");
        Visit(subsequentSiblingSelector.Next);
    }

    public override void VisitListSelector(ListSelector listSelector)
    {
        foreach (var selector in listSelector.Selectors)
        {
            Visit(selector);
            if (selector != listSelector.Selectors[^1])
                Writer.Write(", ");
        }
    }
}