using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Processing;

public class DynamicVariableVisitor : ChainingSyntaxElementVisitor
{
    public DynamicVariableVisitorMode Mode { get; set; }

    private readonly Dictionary<string, ISyntaxElement> LastVariableInitializers = new();
    private readonly List<VariableDeclaration> GeneratedPrelude = new();

    private int BodyDepth = 0;
    
    public DynamicVariableVisitor(SyntaxElementVisitor @delegate) : base(@delegate)
    {
    }

    public override void VisitVariableDeclaration(VariableDeclaration variableDeclaration)
    {
        if (variableDeclaration is
            {
                Variable: PlainVariable
                {
                    Name: var varName
                }
            })
        {
            switch (Mode)
            {
                case DynamicVariableVisitorMode.Collect when BodyDepth == 0:
                    LastVariableInitializers[varName] = variableDeclaration.Expression;
                    break;
                case DynamicVariableVisitorMode.Replace when LastVariableInitializers.ContainsKey(varName):
                    return;
                case DynamicVariableVisitorMode.Replace:
                    throw new Exception($"Unknown dynamic variable {varName}");
            }
        }
        
        base.VisitVariableDeclaration(variableDeclaration);
    }

    public override void VisitBody(Body body)
    {
        BodyDepth++;
        base.VisitBody(body);
        BodyDepth--;
    }

    public override void Start()
    {
        if (Mode == DynamicVariableVisitorMode.Replace)
        {
            foreach (var el in GeneratedPrelude)
                base.VisitVariableDeclaration(el);
        }

        base.Start();
    }

    public override void End()
    {
        if (Mode == DynamicVariableVisitorMode.Collect)
        {
            var variables = LastVariableInitializers
                    .Where(pair => pair.Value is CallExpression { Callee: Identifier { Name: "dynamic" } })
                    .Select(pair => pair.Key)
                    .ToList();
            
            var count = variables.Count;
            do
            {
                count = variables.Count;
                
                var newVariables = new List<string>();
                foreach (var varName in variables)
                {
                    var init = LastVariableInitializers[varName];
                    var visitor = new DynamicVariableDependencyVisitor();
                    visitor.Visit(init);
                    newVariables.AddRange(visitor.Dependencies);
                    newVariables.Add(varName);
                }

                variables = newVariables.Distinct().ToList();
            } while (variables.Count != count);

            var notHoistedVariables = LastVariableInitializers.Keys
                    .Where(varName => !variables.Contains(varName))
                    .ToList();

            foreach (var varName in notHoistedVariables)
                LastVariableInitializers.Remove(varName);

            foreach (var varName in variables)
            {
                var init = LastVariableInitializers[varName];
                var newInit = init switch
                {
                    CallExpression
                    {
                        Callee: Identifier
                        {
                            Name: "dynamic"
                        },
                        Arguments:
                        {
                            Arguments: [var arg]
                        }
                    } => arg,
                    not null => init,
                    _ => throw new ArgumentOutOfRangeException()
                };
                var prelude = new VariableDeclaration(new PlainVariable(varName), newInit, false, false);
                GeneratedPrelude.Add(prelude);
            }
        }
        
        base.End();
    }
}

internal class DynamicVariableDependencyVisitor : SyntaxElementVisitor
{
    public readonly HashSet<string> Dependencies = new();
    
    public override void VisitPlainVariable(PlainVariable plainVariable)
    {
        Dependencies.Add(plainVariable.Name);
        base.VisitPlainVariable(plainVariable);
    }
}

public enum DynamicVariableVisitorMode
{
    Collect,
    Replace
}