using System.Globalization;
using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Processing;

public partial class SassWriterVisitor : SyntaxElementVisitor
{
    private TextWriter Writer;

    public SassWriterVisitor(TextWriter writer)
    {
        Writer = writer;
    }

    public override void VisitNoneLiteral(NoneLiteral noneLiteral)
    {
        Writer.Write("none");
    }

    public override void VisitStringLiteral(StringLiteral stringLiteral)
    {
        Writer.Write("'");
        Writer.Write(stringLiteral.Value);
        Writer.Write("'");
    }

    public override void VisitNumberLiteral(NumberLiteral numberLiteral)
    {
        Writer.Write(numberLiteral.Integer ? numberLiteral.Value.ToString("0") : numberLiteral.Value.ToString("0.0"));
    }

    public override void VisitDimensionLiteral(DimensionLiteral dimensionLiteral)
    {
        Writer.Write(dimensionLiteral.Integer ? dimensionLiteral.Value.ToString("0") : dimensionLiteral.Value.ToString("0.0"));
        Writer.Write(dimensionLiteral.Unit);
    }

    public override void VisitPercentageLiteral(PercentageLiteral percentageLiteral)
    {
        Writer.Write(percentageLiteral.Value.ToString(CultureInfo.InvariantCulture));
    }

    public override void VisitBooleanLiteral(BooleanLiteral booleanLiteral)
    {
        Writer.Write(booleanLiteral.Value ? "true" : "false");
    }

    public override void VisitColorLiteral(ColorLiteral colorLiteral)
    {
        Writer.Write('#');
        Writer.Write(colorLiteral.Value);
    }

    public override void VisitNullLiteral(NullLiteral nullLiteral)
    {
        Writer.Write("null");
    }

    public override void VisitAtRule(AtRule atRule)
    {
        Writer.Write("@");
        Writer.Write(atRule.Name);
        Writer.Write(" ");

        foreach (var preludeItem in atRule.Prelude)
        {
            Visit(preludeItem);
            Writer.Write(" ");
        }

        VisitSimpleBlock(atRule.Value);
    }

    public override void VisitImportStatement(ImportStatement importStatement)
    {
        Writer.Write("@import ");
        Visit(importStatement.Target);
        Writer.Write(";\n");
    }

    public override void VisitIncludeStatement(IncludeStatement includeStatement)
    {
        Writer.Write("@include ");
        VisitIdentifier(includeStatement.Target);
        if (includeStatement.Arguments is { } args)
        {
            Writer.Write('(');
            VisitCallArgumentList(args);
            Writer.Write(')');
        }
        Writer.Write(";\n");
    }

    public override void VisitErrorStatement(ErrorStatement errorStatement)
    {
        Writer.Write("@error ");
        Visit(errorStatement.Message);
        Writer.Write(";\n");
    }

    public override void VisitWarnStatement(WarnStatement warnStatement)
    {
        Writer.Write("@warn ");
        Visit(warnStatement.Message);
        Writer.Write(";\n");
    }

    public override void VisitDebugStatement(DebugStatement debugStatement)
    {
        Writer.Write("@debug ");
        Visit(debugStatement.Message);
        Writer.Write(";\n");
    }

    public override void VisitReturnStatement(ReturnStatement returnStatement)
    {
        Writer.Write("@return ");
        Visit(returnStatement.Expression);
        Writer.Write(";\n");
    }

    public override void VisitUnaryExpression(UnaryExpression unaryExpression)
    {
        Writer.Write(unaryExpression.Operator);
        if (unaryExpression.Operand is BinaryExpression)
            Writer.Write("(");
        Visit(unaryExpression.Operand);
        if (unaryExpression.Operand is BinaryExpression)
            Writer.Write(")");
    }

    public override void VisitBinaryExpression(BinaryExpression binaryExpression)
    {
        if (binaryExpression.Left is BinaryExpression)
            Writer.Write("(");
        Visit(binaryExpression.Left);
        if (binaryExpression.Left is BinaryExpression)
            Writer.Write(")");
        
        Writer.Write(" ");
        Writer.Write(binaryExpression.Operator);
        Writer.Write(" ");
        
        if (binaryExpression.Right is BinaryExpression)
            Writer.Write("(");
        Visit(binaryExpression.Right);
        if (binaryExpression.Right is BinaryExpression)
            Writer.Write(")");
    }

    public override void VisitCallExpression(CallExpression callExpression)
    {
        Visit(callExpression.Callee);
        Writer.Write("(");
        if (callExpression.Callee is Identifier { Name: "url" } 
                && callExpression.Arguments.Arguments[0] is { Value: StringLiteral sl })
            Writer.Write(sl.Value);
        else
            VisitCallArgumentList(callExpression.Arguments);

        Writer.Write(")");
    }

    public override void VisitExpressionList(ExpressionList expressionList)
    {
        foreach (var expression in expressionList.Expressions)
        {
            if (expression is ExpressionList)
                Writer.Write('(');
            Visit(expression);
            if (expression is ExpressionList)
                Writer.Write(')');
            if (expression != expressionList.Expressions[^1])
                Writer.Write(expressionList.Separator);
        }
    }

    public override void VisitVariableDeclaration(VariableDeclaration variableDeclaration)
    {
        Visit(variableDeclaration.Variable);
        Writer.Write(": ");
        Visit(variableDeclaration.Expression);
        Writer.Write(variableDeclaration.Global ? " !global" : "");
        Writer.Write(variableDeclaration.Default ? " !default" : "");
        Writer.Write(";\n");
    }

    public override void VisitPlainVariable(PlainVariable plainVariable)
    {
        Writer.Write("$");
        Writer.Write(plainVariable.Name);
    }

    public override void VisitIdentifier(Identifier identifier)
    {
        Writer.Write(identifier.Name);
    }

    public override void VisitArgumentList(ArgumentList argumentList)
    {
        foreach (var argument in argumentList.Arguments)
        {
            VisitArgument(argument);
            if (argument != argumentList.Arguments[^1])
                Writer.Write(",");
        }
    }

    public override void VisitCallArgumentList(CallArgumentList callArgumentList)
    {
        foreach (var argument in callArgumentList.Arguments)
        {
            VisitCallArgument(argument);
            if (argument != callArgumentList.Arguments[^1])
                Writer.Write(",");
        }
    }

    public override void VisitArgument(Argument argument)
    {
        Visit(argument.Name);
        if (argument.DefaultValue is { } defaultValue)
        {
            Writer.Write(":");
            Visit(defaultValue);
        }
    }

    public override void VisitCallArgument(CallArgument callArgument)
    {
        if (callArgument.Name is { } name)
        {
            Visit(name);
            Writer.Write(":");
        }
        Visit(callArgument.Value);
    }

    public override void VisitSimpleBlock(SimpleBlock simpleBlock)
    {
        foreach (var item in simpleBlock.Value)
        {
            Visit(item);
            Writer.Write(";\n");
        }
    }

    public override void VisitIfStatement(IfStatement ifStatement)
    {
        VisitIfStatement(ifStatement, false);
    }
    
    private void VisitIfStatement(IfStatement ifStatement, bool elseIf)
    {
        Writer.Write("@if (");
        Visit(ifStatement.Expression);
        Writer.Write(") {\n");
        Visit(ifStatement.Body);
        Writer.Write("}\n");
        
        if (ifStatement.Else is not { } elseBlock)
            return;
        
        Writer.Write("@else ");
        switch (elseBlock)
        {
            case IfStatement elseIfStatement:
                VisitIfStatement(elseIfStatement, true);
                break;
            case Body elseBody:
                Writer.Write("{\n");
                VisitBody(elseBody);
                Writer.Write("}\n");
                break;
            default:
                throw new Exception($"Unsupported else block type {elseBlock.GetType()}");
        }
    }

    public override void VisitBody(Body body)
    {
        foreach (var item in body.Children)
        {
            Visit(item);
            Writer.Write("\n");
        }
    }

    public override void VisitSingleTokenComponentValue(SingleTokenComponentValue singleTokenComponentValue)
    {
        Writer.Write(singleTokenComponentValue.Value.GetStringRepresentation());
    }

    public override void VisitPropertyDeclaration(PropertyDeclaration propertyDeclaration)
    {
        VisitIdentifier(propertyDeclaration.Property);
        Writer.Write(": ");
        Visit(propertyDeclaration.Expression);
        if (propertyDeclaration.Important)
        {
            Writer.Write(" !important");
        }
        Writer.Write(";\n");
    }

    public override void VisitMixinDeclaration(MixinDeclaration mixinDeclaration)
    {
        Writer.Write("@mixin ");
        VisitIdentifier(mixinDeclaration.Name);
        if (mixinDeclaration.Arguments is { } argumentList)
        {
            Writer.Write("(");
            VisitArgumentList(argumentList);
            Writer.Write(")");
        }

        Writer.Write(" {\n");
        VisitBody(mixinDeclaration.Body);
        Writer.Write("}\n");
    }

    public override void VisitFunctionDeclaration(FunctionDeclaration functionDeclaration)
    {
        Writer.Write("@function ");
        VisitIdentifier(functionDeclaration.Name);
        Writer.Write("(");
        VisitArgumentList(functionDeclaration.Arguments);
        Writer.Write(") {\n");
        VisitBody(functionDeclaration.Body);
        Writer.Write("}\n");
    }

    public override void VisitMapEntryExpression(MapEntryExpression mapEntryExpression)
    {
        Visit(mapEntryExpression.Key);
        Writer.Write(": ");
        Visit(mapEntryExpression.Value);
    }

    public override void VisitMapExpression(MapExpression mapExpression)
    {
        Writer.Write("(");
        foreach (var entry in mapExpression.Entries)
        {
            VisitMapEntryExpression(entry);
            if (entry != mapExpression.Entries[^1])
                Writer.Write(", ");
        }
        Writer.Write(")");
    }

    public override void VisitForStatement(ForStatement forStatement)
    {
        Writer.Write("@for ");
        Visit(forStatement.Variable);
        Writer.Write(" from ");
        Visit(forStatement.From);
        Writer.Write(forStatement.Inclusive ? " through " : " to ");
        Visit(forStatement.To);
        Writer.Write(" {\n");
        VisitBody(forStatement.Body);
        Writer.Write("}\n");
    }

    public override void VisitEachStatement(EachStatement eachStatement)
    {
        Writer.Write("@each ");
        Visit(eachStatement.VariableList);
        Writer.Write(" in ");
        Visit(eachStatement.Expression);
        Writer.Write(" {\n");
        Visit(eachStatement.Body);
        Writer.Write("}\n");
    }

    public override void VisitExtendStatement(ExtendStatement extendStatement)
    {
        Writer.Write("@extend ");
        Visit(extendStatement.Selector);
        Writer.Write(";\n");
    }

    public override void VisitRuleDeclaration(RuleDeclaration ruleDeclaration)
    {
        Visit(ruleDeclaration.Selector);
        Writer.Write(" {\n");
        VisitBody(ruleDeclaration.Body);
        Writer.Write("}\n");
    }
}