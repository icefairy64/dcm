using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Processing;

public interface ISyntaxElementVisitor
{
    public void Visit(ISyntaxElement syntaxElement);
}