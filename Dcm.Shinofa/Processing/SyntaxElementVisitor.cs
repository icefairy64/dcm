using Breezy.Dcm.Shinofa.Parsing;

namespace Breezy.Dcm.Shinofa.Processing;

public class SyntaxElementVisitor : ISyntaxElementVisitor
{
    public virtual void Start()
    {
        
    }

    public virtual void End()
    {
        
    }
    
    public void Visit(ISyntaxElement syntaxElement)
    {
        switch (syntaxElement)
        {
            case NoneLiteral noneLiteral: 
                VisitNoneLiteral(noneLiteral);
                break;
            
            case StringLiteral stringLiteral:
                VisitStringLiteral(stringLiteral);
                break;
            
            case NumberLiteral numberLiteral:
                VisitNumberLiteral(numberLiteral);
                break;
            
            case DimensionLiteral dimensionLiteral:
                VisitDimensionLiteral(dimensionLiteral);
                break;

            case PercentageLiteral percentageLiteral:
                VisitPercentageLiteral(percentageLiteral);
                break;

            case BooleanLiteral booleanLiteral:
                VisitBooleanLiteral(booleanLiteral);
                break;

            case ColorLiteral colorLiteral:
                VisitColorLiteral(colorLiteral);
                break;
            
            case NullLiteral nullLiteral:
                VisitNullLiteral(nullLiteral);
                break;
            
            case AtRule atRule:
                VisitAtRule(atRule);
                break;
            
            case ImportStatement importStatement:
                VisitImportStatement(importStatement);
                break;

            case IncludeStatement includeStatement:
                VisitIncludeStatement(includeStatement);
                break;

            case ErrorStatement errorStatement:
                VisitErrorStatement(errorStatement);
                break;

            case WarnStatement warnStatement:
                VisitWarnStatement(warnStatement);
                break;

            case DebugStatement debugStatement:
                VisitDebugStatement(debugStatement);
                break;
            
            case ReturnStatement returnStatement:
                VisitReturnStatement(returnStatement);
                break;
            
            case UnaryExpression unaryExpression:
                VisitUnaryExpression(unaryExpression);
                break;
            
            case BinaryExpression binaryExpression:
                VisitBinaryExpression(binaryExpression);
                break;
            
            case CallExpression callExpression:
                VisitCallExpression(callExpression);
                break;
            
            case ExpressionList expressionList:
                VisitExpressionList(expressionList);
                break;

            case VariableDeclaration variableDeclaration:
                VisitVariableDeclaration(variableDeclaration);
                break;

            case PlainVariable plainVariable:
                VisitPlainVariable(plainVariable);
                break;

            case Identifier identifier:
                VisitIdentifier(identifier);
                break;

            case ArgumentList argumentList:
                VisitArgumentList(argumentList);
                break;

            case CallArgumentList callArgumentList:
                VisitCallArgumentList(callArgumentList);
                break;

            case Argument argument:
                VisitArgument(argument);
                break;

            case CallArgument callArgument:
                VisitCallArgument(callArgument);
                break;

            case SimpleBlock simpleBlock:
                VisitSimpleBlock(simpleBlock);
                break;
            
            case IfStatement ifStatement:
                VisitIfStatement(ifStatement);
                break;
            
            case Body body:
                VisitBody(body);
                break;
            
            case SingleTokenComponentValue singleTokenComponentValue:
                VisitSingleTokenComponentValue(singleTokenComponentValue);
                break;
            
            case PropertyDeclaration propertyDeclaration:
                VisitPropertyDeclaration(propertyDeclaration);
                break;
            
            case MixinDeclaration mixinDeclaration:
                VisitMixinDeclaration(mixinDeclaration);
                break;
            
            case FunctionDeclaration functionDeclaration:
                VisitFunctionDeclaration(functionDeclaration);
                break;
            
            case MapEntryExpression mapEntryExpression:
                VisitMapEntryExpression(mapEntryExpression);
                break;
            
            case MapExpression mapExpression:
                VisitMapExpression(mapExpression);
                break;
            
            case ForStatement forStatement:
                VisitForStatement(forStatement);
                break;
            
            case EachStatement eachStatement:
                VisitEachStatement(eachStatement);
                break;
            
            case ExtendStatement extendStatement:
                VisitExtendStatement(extendStatement);
                break;
            
            case RuleDeclaration ruleDeclaration:
                VisitRuleDeclaration(ruleDeclaration);
                break;
            
            case ClassSelector classSelector:
                VisitClassSelector(classSelector);
                break;
            
            case IdSelector idSelector:
                VisitIdSelector(idSelector);
                break;
            
            case TypeSelector typeSelector:
                VisitTypeSelector(typeSelector);
                break;
            
            case AttributeSelector attributeSelector:
                VisitAttributeSelector(attributeSelector);
                break;
            
            case NestingSelector nestingSelector:
                VisitNestingSelector(nestingSelector);
                break;
            
            case UniversalSelector universalSelector:
                VisitUniversalSelector(universalSelector);
                break;
            
            case ComplexPseudoClassSelector complexPseudoClassSelector:
                VisitComplexPseudoClassSelector(complexPseudoClassSelector);
                break;
            
            case ContentfulPseudoClassSelector contentfulPseudoClassSelector:
                VisitContentfulPseudoClassSelector(contentfulPseudoClassSelector);
                break;
            
            case PseudoClassSelector pseudoClassSelector:
                VisitPseudoClassSelector(pseudoClassSelector);
                break;
            
            case PseudoElementSelector pseudoElementSelector:
                VisitPseudoElementSelector(pseudoElementSelector);
                break;
            
            case ComplexPseudoElementSelector complexPseudoElementSelector:
                VisitComplexPseudoElementSelector(complexPseudoElementSelector);
                break;
            
            case CompoundSelector compoundSelector:
                VisitCompoundSelector(compoundSelector);
                break;
            
            case ChildSelector childSelector:
                VisitChildSelector(childSelector);
                break;
            
            case DescendantSelector descendantSelector:
                VisitDescendantSelector(descendantSelector);
                break;
            
            case NextSiblingSelector nextSiblingSelector:
                VisitNextSiblingSelector(nextSiblingSelector);
                break;
            
            case SubsequentSiblingSelector subsequentSiblingSelector:
                VisitSubsequentSiblingSelector(subsequentSiblingSelector);
                break;
            
            case ListSelector listSelector:
                VisitListSelector(listSelector);
                break;
            
            default:
                throw new ArgumentException($"Invalid syntax element type {syntaxElement.GetType()}");
        }
    }

       
    public virtual void VisitNoneLiteral(NoneLiteral noneLiteral) 
    {
        //Your implementation here
    }

    public virtual void VisitStringLiteral(StringLiteral stringLiteral) 
    {
        //Your implementation here
    }

    public virtual void VisitNumberLiteral(NumberLiteral numberLiteral) 
    {
        //Your implementation here
    }

    public virtual void VisitDimensionLiteral(DimensionLiteral dimensionLiteral) 
    {
        //Your implementation here
    }

    public virtual void VisitPercentageLiteral(PercentageLiteral percentageLiteral) 
    {
        //Your implementation here
    }

    public virtual void VisitBooleanLiteral(BooleanLiteral booleanLiteral) 
    {
        //Your implementation here
    }
    
    public virtual void VisitColorLiteral(ColorLiteral colorLiteral) 
    {
        //Your implementation here
    }

    public virtual void VisitNullLiteral(NullLiteral nullLiteral) 
    {
        //Your implementation here
    }

    public virtual void VisitAtRule(AtRule atRule) 
    {
        foreach (var preludeItem in atRule.Prelude)
            Visit(preludeItem);

        foreach (var bodyItem in atRule.Value.Value)
            Visit(bodyItem);
    }

    public virtual void VisitImportStatement(ImportStatement importStatement) 
    {
        Visit(importStatement.Target);
    }

    public virtual void VisitIncludeStatement(IncludeStatement includeStatement) 
    {
        VisitIdentifier(includeStatement.Target);
    }

    public virtual void VisitErrorStatement(ErrorStatement errorStatement) 
    {
        Visit(errorStatement.Message);
    }

    public virtual void VisitWarnStatement(WarnStatement warnStatement) 
    {
        Visit(warnStatement.Message);
    }

    public virtual void VisitDebugStatement(DebugStatement debugStatement) 
    {
        Visit(debugStatement.Message);
    }

    public virtual void VisitReturnStatement(ReturnStatement returnStatement) 
    {
        Visit(returnStatement.Expression);
    }

    public virtual void VisitUnaryExpression(UnaryExpression unaryExpression)
    {
        Visit(unaryExpression.Operand);
    }

    public virtual void VisitBinaryExpression(BinaryExpression binaryExpression)
    {
        Visit(binaryExpression.Left);
        Visit(binaryExpression.Right);
    }

    public virtual void VisitCallExpression(CallExpression callExpression)
    {
        Visit(callExpression.Callee);
        VisitCallArgumentList(callExpression.Arguments);
    }

    public virtual void VisitExpressionList(ExpressionList expressionList)
    {
        foreach (var expression in expressionList.Expressions)
            Visit(expression);
    }
    
    public virtual void VisitVariableDeclaration(VariableDeclaration variableDeclaration) 
    {
        Visit(variableDeclaration.Variable);
        Visit(variableDeclaration.Expression);
    }

    public virtual void VisitPlainVariable(PlainVariable plainVariable) 
    {
        //Your implementation here
    }

    public virtual void VisitIdentifier(Identifier identifier) 
    {
        //Your implementation here
    }

    public virtual void VisitArgumentList(ArgumentList argumentList) 
    {
        foreach (var argument in argumentList.Arguments)
            VisitArgument(argument);
    }

    public virtual void VisitCallArgumentList(CallArgumentList callArgumentList) 
    {
        foreach (var argument in callArgumentList.Arguments)
            VisitCallArgument(argument);
    }

    public virtual void VisitArgument(Argument argument) 
    {
        Visit(argument.Name);
        if (argument.DefaultValue is {} defaultValue)
            Visit(defaultValue);
    }

    public virtual void VisitCallArgument(CallArgument callArgument) 
    {
        if (callArgument.Name is {} name)
            Visit(name);
        Visit(callArgument.Value);
    }

    public virtual void VisitSimpleBlock(SimpleBlock simpleBlock) 
    {
        foreach (var item in simpleBlock.Value)
            Visit(item);
    }

    public virtual void VisitIfStatement(IfStatement ifStatement) 
    {
        Visit(ifStatement.Expression);
        VisitBody(ifStatement.Body);
        if (ifStatement.Else is {} elseBlock)
            Visit(elseBlock);
    }

    public virtual void VisitBody(Body body) 
    {
        foreach (var item in body.Children)
            Visit(item);
    }

    public virtual void VisitSingleTokenComponentValue(SingleTokenComponentValue singleTokenComponentValue) 
    {
        //Your implementation here
    }

    public virtual void VisitPropertyDeclaration(PropertyDeclaration propertyDeclaration) 
    {
        VisitIdentifier(propertyDeclaration.Property);
        Visit(propertyDeclaration.Expression);
    }

    public virtual void VisitMixinDeclaration(MixinDeclaration mixinDeclaration) 
    {
        Visit(mixinDeclaration.Name);
        if (mixinDeclaration.Arguments is {} argumentList)
            VisitArgumentList(argumentList);
        VisitBody(mixinDeclaration.Body);
    }

    public virtual void VisitFunctionDeclaration(FunctionDeclaration functionDeclaration) 
    {
        VisitIdentifier(functionDeclaration.Name);
        VisitArgumentList(functionDeclaration.Arguments);
        VisitBody(functionDeclaration.Body);
    }

    public virtual void VisitMapEntryExpression(MapEntryExpression mapEntryExpression)
    {
        Visit(mapEntryExpression.Key);
        Visit(mapEntryExpression.Value);
    }

    public virtual void VisitMapExpression(MapExpression mapExpression)
    {
        foreach (var entry in mapExpression.Entries)
            VisitMapEntryExpression(entry);
    }

    public virtual void VisitForStatement(ForStatement forStatement)
    {
        Visit(forStatement.Variable);
        Visit(forStatement.From);
        Visit(forStatement.To);
        Visit(forStatement.Body);
    }

    public virtual void VisitEachStatement(EachStatement eachStatement)
    {
        Visit(eachStatement.VariableList);
        Visit(eachStatement.Expression);
        Visit(eachStatement.Body);
    }

    public virtual void VisitExtendStatement(ExtendStatement extendStatement)
    {
        Visit(extendStatement.Selector);
    }

    public virtual void VisitRuleDeclaration(RuleDeclaration ruleDeclaration)
    {
        Visit(ruleDeclaration.Selector);
        Visit(ruleDeclaration.Body);
    }
    
    public virtual void VisitClassSelector(ClassSelector classSelector) 
    {
        //Your implementation here
    }

    public virtual void VisitIdSelector(IdSelector idSelector) 
    {
        //Your implementation here
    }

    public virtual void VisitTypeSelector(TypeSelector typeSelector) 
    {
        //Your implementation here
    }

    public virtual void VisitAttributeSelector(AttributeSelector attributeSelector) 
    {
        //Your implementation here
    }

    public virtual void VisitNestingSelector(NestingSelector nestingSelector) 
    {
        //Your implementation here
    }

    public virtual void VisitUniversalSelector(UniversalSelector universalSelector) 
    {
        //Your implementation here
    }

    public virtual void VisitComplexPseudoClassSelector(ComplexPseudoClassSelector complexPseudoClassSelector)
    {
        Visit(complexPseudoClassSelector.Body);
    }

    public virtual void VisitContentfulPseudoClassSelector(ContentfulPseudoClassSelector contentfulPseudoClassSelector)
    {
        //Your implementation here
    }

    public virtual void VisitPseudoClassSelector(PseudoClassSelector pseudoClassSelector)
    {
        //Your implementation here
    }

    public virtual void VisitPseudoElementSelector(PseudoElementSelector pseudoElementSelector)
    {
        //Your implementation here
    }

    public virtual void VisitComplexPseudoElementSelector(ComplexPseudoElementSelector complexPseudoElementSelector)
    {
        Visit(complexPseudoElementSelector.Body);
    }

    public virtual void VisitCompoundSelector(CompoundSelector compoundSelector)
    {
        foreach (var selector in compoundSelector.Selectors)
            Visit(selector);
    }

    public virtual void VisitChildSelector(ChildSelector childSelector)
    {
        Visit(childSelector.Parent);
        Visit(childSelector.Child);
    }

    public virtual void VisitDescendantSelector(DescendantSelector descendantSelector)
    {
        Visit(descendantSelector.Parent);
        Visit(descendantSelector.Child);
    }

    public virtual void VisitNextSiblingSelector(NextSiblingSelector nextSiblingSelector)
    {
        Visit(nextSiblingSelector.First);
        Visit(nextSiblingSelector.Next);
    }

    public virtual void VisitSubsequentSiblingSelector(SubsequentSiblingSelector subsequentSiblingSelector)
    {
        Visit(subsequentSiblingSelector.First);
        Visit(subsequentSiblingSelector.Next);
    }

    public virtual void VisitListSelector(ListSelector listSelector)
    {
        foreach (var selector in listSelector.Selectors)
            Visit(selector);
    }
}