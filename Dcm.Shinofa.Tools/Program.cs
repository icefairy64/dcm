﻿using System.Globalization;
using System.Text;
using Breezy.Dcm.Shinofa.Parsing;
using Breezy.Dcm.Shinofa.Tokenization;

if (args.Length == 0)
{
    Console.WriteLine("Action is required\n");
    Console.WriteLine("Usage: ");
    Console.WriteLine("\tdcm-tools <action>\n");
    Console.WriteLine("Actions:");
    Console.WriteLine("\ttokenize [filename]");
    Console.WriteLine("\tparse [filename]");
    return;
}

switch (args[0])
{
    case "tokenize":
        await Tokenize(args[1..]);
        break;
    
    case "parse":
        await Parse(args[1..]);
        break;
    
    case "parse-all":
        await ParseAll(args[1..]);
        break;
    
    default:
        Console.WriteLine($"Unknown action {args[0]}");
        break;
}

return;

async Task Tokenize(IReadOnlyList<string> args)
{
    await using var stream = args.Count == 0 ? Console.OpenStandardInput() : File.OpenRead(args[0]);
    var content = await ReadStream(stream);
    var tokens = Tokenizer.Tokenize(content);
    foreach (var token in tokens)
        Console.WriteLine(token);
}

async Task Parse(IReadOnlyList<string> args)
{
    await using var stream = args.Count == 0 ? Console.OpenStandardInput() : File.OpenRead(args[0]);
    var content = await ReadStream(stream);
    var groups = Parser.Parse(content);
    
    foreach (var group in groups)
        Console.WriteLine(group);
    
    Console.WriteLine();

    foreach (var group in groups)
    {
        RenderTree(group);
        Console.WriteLine();
    }
}

async Task ParseAll(IReadOnlyList<string> args)
{
    var (successful, failed) = await TryParseRec(args[0]);
    Console.WriteLine();
    Console.WriteLine($"Successful: {successful}, failed: {failed}");
}

async Task<(int, int)> TryParseRec(string rootPath)
{
    var root = new DirectoryInfo(rootPath);
    var successful = 0;
    var failed = 0;
    
    foreach (var childDir in root.EnumerateDirectories())
    {
        var (s, f) = await TryParseRec(childDir.FullName);
        successful += s;
        failed += f;
    }

    foreach (var file in root.EnumerateFiles("*.scss"))
    {
        if (await TryParseFile(file))
        {
            Console.WriteLine($"[S] {file.FullName}");
            successful++;
        }
        else
        {
            Console.WriteLine($"[F] {file.FullName}");
            failed++;
        }
    }

    return (successful, failed);
}

async Task<bool> TryParseFile(FileInfo file)
{
    await using var stream = file.OpenRead();
    var content = await ReadStream(stream);
    try
    {
        Parser.Parse(content);
        return true;
    }
    catch (Exception e)
    {
        Console.WriteLine($"Failed with {e.GetType().Name}: {e.Message}");
        Console.WriteLine(e.StackTrace);
        return false;
    }
}

void RenderTree(ISyntaxElement el, int indentLevel = 0)
{
    if (indentLevel > 0)
    {
        for (var l = 0; l < indentLevel - 1; l++)
             Console.Write("    ");
        Console.Write("|-- ");
    }
    Console.WriteLine($"{el.GetType().Name} {GetElementDetails(el)}");
    
    if (el is not Node node)
        return;
    
    foreach (var child in node.Children)
        RenderTree(child, indentLevel + 1);
}

string GetElementDetails(ISyntaxElement el)
{
    return el switch
    {
        ClassSelector cs => cs.ClassName,
        PropertyDeclaration pd => $"[{pd.Property.Name}]",
        Identifier id => id.Name,
        NumberLiteral nl => nl.Value.ToString(CultureInfo.InvariantCulture),
        StringLiteral sl => sl.Value,
        DimensionLiteral dl => $"{dl.Value}{dl.Unit}",
        PlainVariable pv => pv.Name,
        BinaryExpression be => be.Operator,
        UnaryExpression ue => ue.Operator,
        _ => ""
    };
}

async Task<string> ReadStream(Stream stream)
{
    var reader = new StreamReader(stream);
    return await reader.ReadToEndAsync();
}