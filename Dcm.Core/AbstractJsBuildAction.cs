﻿using System.Collections.Concurrent;
using Breezy.Dcm.Core.Classes;
using Breezy.Dcm.Core.Strict;
using Esprima;
using Esprima.Ast;
using Esprima.Utils;
using Microsoft.Extensions.Logging;

namespace Breezy.Dcm.Core;

public class AbstractJsBuildAction<TConfig> where TConfig : BuildConfiguration
{
    protected readonly ILogger Logger;
    protected readonly ILoggerFactory LoggerFactory;

    public TConfig Configuration { get; }
    
    public Predicate<string>? ClassLoadFilter { get; set; }
    
    protected readonly ClassExamineVisitor ClassExamineVisitor = new();
    
    private static readonly ParserOptions ParserOptions = new()
    {
        Comments = true
    };

    public AbstractJsBuildAction(TConfig configuration, ILogger logger, ILoggerFactory loggerFactory)
    {
        Configuration = configuration;
        Logger = logger;
        LoggerFactory = loggerFactory;
    }

    protected async Task<IClassDeclaration?> ParseJs(ClassFile jsFile, CancellationToken token)
    {
        try
        {
            var content = await File.ReadAllTextAsync(jsFile.Path.FullName, token);
            var parser = new JavaScriptParser(ParserOptions);
            var script = parser.ParseScript(content);

            var visitorStack = new ChainingVisitor
            {
                Visitors = new List<AstVisitor>
                {
                    new GlobalReplaceVisitor(),
                    new ProtectedGlobalsVisitor()
                }
            };

            script = visitorStack.Visit(script) as Script ?? throw new Exception("Visitor did not return a Script");
            
            return ExtClassDeclarationDetector.ReadDeclaration(jsFile, script, parser);
        }
        catch (Exception e)
        {
            throw new Exception($"Failed to parse class {jsFile.Path.FullName}", e);
        }
    }

    protected virtual IEnumerable<ClassFile> JsClassPath
    {
        get
        {
            var jsFiles = new List<ClassFile>();
            foreach (var pkg in Configuration.Packages)
            {
                if (pkg.ClasspathFiles is not null)
                    jsFiles.AddRange(pkg.ClasspathFiles);
                if (pkg.OverrideFiles is not null)
                    jsFiles.AddRange(pkg.OverrideFiles);
            }

            return jsFiles;
        }
    }

    protected virtual IReadOnlyList<DefineVisitor> ExamineDefineVisitors =>
        new List<DefineVisitor>
        {
            ClassExamineVisitor
        };
    
    protected virtual IReadOnlyList<IClassDeclarationVisitor> ExamineVisitors => ArraySegment<IClassDeclarationVisitor>.Empty;

    protected async Task ExamineClasses()
    {
        var jsFiles = JsClassPath;
        var declarations = new ConcurrentQueue<IClassDeclaration>();

        await Parallel.ForEachAsync(jsFiles, async (file, token) =>
        {
            var declaration = await ParseJs(file, token);
            if (declaration is not null)
                declarations.Enqueue(declaration);
        });
        
        var chainedDefineVisitor = new ChainingDefineVisitor
        {
            Visitors = ExamineDefineVisitors
        };

        var chainedVisitor = new ChainingClassDeclarationVisitor
        {
            Visitors = ExamineVisitors
        };

        foreach (var declaration in declarations)
        {
            if (declaration.ClassName is not null && ClassLoadFilter?.Invoke(declaration.ClassName) == false)
                continue;

            switch (declaration)
            {
                case ExtClassDefineDeclaration defineDeclaration:
                    defineDeclaration.Accept(chainedDefineVisitor);
                    break;
                case ExtClassAssignmentDeclaration assignmentDeclaration:
                    ClassExamineVisitor.VisitAssignmentDeclaration(assignmentDeclaration);
                    assignmentDeclaration.AcceptClassDeclarationVisitor(chainedVisitor);
                    break;
                case ExtClassApplyDeclaration applyDeclaration:
                    ClassExamineVisitor.VisitApplyDeclaration(applyDeclaration);
                    applyDeclaration.AcceptClassDeclarationVisitor(chainedVisitor);
                    break;
                case ExtApplicationDeclaration applicationDeclaration:
                    ClassExamineVisitor.VisitApplicationDeclaration(applicationDeclaration);
                    applicationDeclaration.AcceptClassDeclarationVisitor(chainedVisitor);
                    break;
            }
        }
    }
    
    protected IList<ISet<string>> BuildDependencyOrder(IReadOnlyList<string> roots)
    {
        var dependencyGraph = new List<string>();
        var visitedClassesSet = new HashSet<string>();
        
        foreach (var root in roots)
            Crawl(root, dependencyGraph, visitedClassesSet);

        //
        
        var visitedClassesIndex = visitedClassesSet.ToArray();
        var dict = new Dictionary<string, int>();

        foreach (var root in roots)
        {
            dict[root] = Array.IndexOf(visitedClassesIndex, root);
        }
        
        for (var idx = 0; idx < visitedClassesIndex.Length; idx++)
            dict[visitedClassesIndex[idx]] = idx;

        var indexedDependencyGraph = dependencyGraph.Select(x => dict[x]).ToArray();

        //

        var indexArray = new int[visitedClassesIndex.Length];
        for (var idx = 0; idx < indexArray.Length; idx++)
            indexArray[idx] = idx;

        var batches = new List<ISet<string>>();
        var includedClasses = new HashSet<int>();
        var usesQueue = new HashSet<int>();

        while (true)
        {
            var noDepsClasses = new HashSet<int>(indexArray);
            for (var idx = 0; idx < indexedDependencyGraph.Length; idx += 2)
            {
                var clsIndex = indexedDependencyGraph[idx];
                if (clsIndex >= 0)
                    noDepsClasses.Remove(clsIndex);
            }

            var batch = new HashSet<string>();
            for (var idx = 1; idx < indexedDependencyGraph.Length; idx += 2)
            {
                var clsIndex = indexedDependencyGraph[idx];
                if (noDepsClasses.Contains(clsIndex))
                {
                    var className = visitedClassesIndex[clsIndex];
                    batch.Add(className);
                    includedClasses.Add(clsIndex);
                    indexedDependencyGraph[idx] = -1;
                    indexedDependencyGraph[idx - 1] = -1;
                    if (ClassExamineVisitor.GetEffectiveLateDependencies(className) is { } uses)
                    {
                        foreach (var dep in uses)
                            usesQueue.Add(dict[dep]);
                    }
                }
            }

            var availableUsed = usesQueue.Intersect(noDepsClasses);
            foreach (var clsIndex in availableUsed)
            {
                var className = visitedClassesIndex[clsIndex];
                batch.Add(className);
                includedClasses.Add(clsIndex);
            }
            usesQueue.ExceptWith(noDepsClasses);

            var availableNoDeps = noDepsClasses.Where(c => !includedClasses.Contains(c));
            foreach (var clsIndex in availableNoDeps)
            {
                var className = visitedClassesIndex[clsIndex];
                batch.Add(className);
                includedClasses.Add(clsIndex);
            }

            if (batch.Count == 0)
                break;

            batches.Add(batch);
        }
        
        for (var idx = 0; idx < indexedDependencyGraph.Length; idx += 2)
        {
            var srcIndex = indexedDependencyGraph[idx];
            var targetIndex = indexedDependencyGraph[idx + 1];
            if (srcIndex >= 0 && targetIndex >= 0)
            {
                if (targetIndex >= visitedClassesIndex.Length)
                {
                    Logger.LogWarning("Target dependency index {} is out of bounds", targetIndex);
                    continue;
                }
                
                if (srcIndex >= visitedClassesIndex.Length)
                {
                    Logger.LogWarning("Source dependency index {} is out of bounds", srcIndex);
                    continue;
                }

                Logger.LogWarning("Dependency {} missing for class {}", visitedClassesIndex[targetIndex], visitedClassesIndex[srcIndex]);
            }
        }

        return batches;
    }
    
    /// <summary>
    /// Crawls the dependencies of the given class.
    /// </summary>
    /// <param name="entry">Class name</param>
    /// <param name="result">Flat list of [entry, dependency] pairs</param>
    /// <param name="visited">Already visited classes</param>
    /// <param name="caller">Name of the class that invoked this method</param>
    protected void Crawl(string entry, IList<string> result, ISet<string> visited, string? caller = null)
    {
        var deps = ClassExamineVisitor?.GetEffectiveDependencies(entry);

        if (deps is null)
        {
            Logger.LogWarning("Class {entry} is not found but is declared as a dependency for {caller}", entry, caller);
            deps = Array.Empty<string>();
        }

        foreach (var dep in deps)
        {
            result.Add(entry);
            result.Add(dep);
        }
        
        foreach (var dep in deps)
        {
            if (visited.Contains(dep))
                continue;
            visited.Add(dep);
            Crawl(dep, result, visited, entry);
        }
        
        var uses = ClassExamineVisitor?.GetEffectiveLateDependencies(entry);
        if (uses is not null)
        {
            foreach (var dep in uses)
            {
                if (visited.Contains(dep))
                    continue;
                visited.Add(dep);
                Crawl(dep, result, visited, entry);
            }
        }

        var overrides = ClassExamineVisitor?.GetOverrides(entry);
        if (overrides is not null)
        {
            foreach (var cls in overrides)
            {
                var dep = cls.ClassName ?? cls.File.ClassName;
                result.Add(dep);
                result.Add(entry);
            }
            
            foreach (var cls in overrides)
            {
                var dep = cls.ClassName ?? cls.File.ClassName;
                if (visited.Contains(dep))
                    continue;
                visited.Add(dep);
                Crawl(dep, result, visited, entry);
            }
        }
    }
}