﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Breezy.Dcm.Core;

public class ClassFile
{
    public string ClassName { get; }
    public FileInfo Path { get; }
    public bool IsOverride { get; }

    public ClassFile(string className, string path, bool isOverride)
    {
        ClassName = className;
        IsOverride = isOverride;
        Path = new FileInfo(path);
    }

    public override string ToString()
    {
        return $"<{nameof(ClassName)}: {ClassName}, {nameof(Path)}: {Path}>";
    }

    public override bool Equals(object? obj)
    {
        return obj is ClassFile cf && Path == cf.Path;
    }

    public override int GetHashCode()
    {
        return Path.GetHashCode();
    }
}

internal static class PackageUtil
{
    internal static IEnumerable<string> GetClasspath(IDictionary<string, JsonElement> extras, string prop)
    {
        if (extras.TryGetValue(prop, out var el))
        {
            return el.ValueKind switch
            {
                JsonValueKind.String => new List<string> { el.GetString() ?? throw new Exception() },
                JsonValueKind.Array => el.Deserialize<IEnumerable<string>>() ?? throw new Exception(),
                _ => throw new Exception($"Invalid {prop} value")
            };
        }

        return Array.Empty<string>();
    }

    internal static string ResolveString(string str, IDictionary<string, string> vars)
    {
        return vars.Aggregate(str, (current, entry) => current.Replace($"${{{entry.Key}}}", entry.Value));
    }
    
    internal static IList<ClassFile> ReadClasspath(string? ns, string extension, IEnumerable<string> classpath, IDictionary<string, string> vars, bool isOverride)
    {
        var result = new List<ClassFile>();
        
        ns ??= "Ext";
        
        foreach (var entry in classpath.Select(e => ResolveString(e, vars)))
        {
            if (!Directory.Exists(entry))
                continue;
            
            if (Path.HasExtension(entry))
            {
                result.Add(new ClassFile("", entry, isOverride));
            }
            else
            {
                var range = Directory.EnumerateFiles(entry, $"*.{extension}", SearchOption.AllDirectories)
                    .Select(e =>
                    {
                        var relPath = System.IO.Path.GetRelativePath(entry, e).Split(Path.DirectorySeparatorChar)
                            .Select(x => x.Replace($".{extension}", "")).ToList();
                        relPath.Insert(0, ns);
                        
                        var className = string.Join('.', relPath.ToArray());
                        
                        if (className == "Ext.Ext")
                            className = "Ext";
                        
                        return new ClassFile(className, e, isOverride);
                    });
                result.AddRange(range);
            }
        }

        return result;
    }
}

public class Package
{
    [JsonInclude]
    public string Name { get; private set; }
    
    [JsonInclude]
    public string Version { get; private set; }
    
    [JsonInclude]
    public SenchaPackage Sencha { get; private set; }
    
    // Runtime data

    public string? Path { get; private set; }

    private IList<ClassFile>? FSassEtcFiles;
    private IList<ClassFile>? FSassVarFiles;
    private IList<ClassFile>? FSassSrcFiles;
    private IList<ClassFile>? FClasspathFiles;
    private IList<ClassFile>? FOverrideFiles;

    public IEnumerable<ClassFile>? SassEtcFiles => FSassEtcFiles;

    public IEnumerable<ClassFile>? SassVarFiles => FSassVarFiles;

    public IEnumerable<ClassFile>? SassSrcFiles => FSassSrcFiles;

    public IEnumerable<ClassFile>? ClasspathFiles => FClasspathFiles;

    public IEnumerable<ClassFile>? OverrideFiles => FOverrideFiles;

    [JsonConstructor]
    public Package(string name, string version, SenchaPackage? sencha)
    {
        Name = name;
        Version = version;
        Sencha = sencha ?? throw new Exception($"Package {name} has no Sencha description");
    }

    public void ReadContents(string path, IReadOnlyDictionary<string, string> vars)
    {
        Path = path;

        var extendedVars = new Dictionary<string, string>(vars)
        {
            { "package.dir", path }
        };
        
        if (Sencha.Sass is not null)
        {
            FSassEtcFiles = PackageUtil.ReadClasspath( Sencha.Sass.Namespace, "scss", Sencha.Sass.EtcClasspath, extendedVars, false);
            FSassVarFiles = PackageUtil.ReadClasspath(Sencha.Sass.Namespace, "scss", Sencha.Sass.VarClasspath, extendedVars, false);
            FSassSrcFiles = PackageUtil.ReadClasspath(Sencha.Sass.Namespace, "scss", Sencha.Sass.SrcClasspath, extendedVars, false);
        }

        FClasspathFiles = PackageUtil.ReadClasspath(Sencha.Namespace, "js", Sencha.Classpath, extendedVars, false);
        FOverrideFiles = PackageUtil.ReadClasspath(Sencha.Namespace, "js", Sencha.OverridesClasspath, extendedVars, true);
    }

    public override string ToString()
    {
        return $"<Package {nameof(Name)}: {Name}, {nameof(Version)}: {Version}, {nameof(Sencha)}: {Sencha}>";
    }
}

public class SenchaPackage
{
    [JsonInclude]
    public string Name { get; private set; }
    [JsonInclude]
    public string? Namespace { get; private set; }
    [JsonInclude]
    [JsonPropertyName("type")]
    public SenchaPackageType PackageType { get; private set; }
    [JsonInclude]
    [JsonPropertyName("alternateName")]
    public IEnumerable<string>? AlternateNames { get; private set; }
    [JsonInclude]
    [JsonPropertyName("extend")]
    public string? Extends { get; private set; }
    [JsonInclude]
    public IEnumerable<string>? Requires { get; private set; }
    [JsonInclude]
    public string? Toolkit { get; private set; }
    [JsonInclude]
    public string Creator { get; private set; }
    [JsonInclude]
    public string Summary { get; private set; }
    [JsonInclude]
    public string? Description { get; private set; }
    [JsonInclude]
    public string? DetailedDescription { get; private set; }
    [JsonInclude]
    public string Version { get; private set; }
    [JsonInclude]
    public string CompatVersion { get; private set; }
    [JsonInclude]
    public SenchaPackageSassInfo? Sass { get; private set; }

    [JsonPropertyName("subpkgs")]
    [JsonInclude]
    public SenchaPackageSubpackages? Subpackages { get; private set; }
    
    [JsonExtensionData]
    [JsonInclude]
    public IDictionary<string, JsonElement> Extras { get; private set; }

    [JsonPropertyName("_classpath")]
    public IEnumerable<string> Classpath => PackageUtil.GetClasspath(Extras, "classpath");
    public IEnumerable<string> OverridesClasspath => PackageUtil.GetClasspath(Extras, "overrides");

    public override string ToString()
    {
        return $"<{nameof(Name)}: {Name}, {nameof(Namespace)}: {Namespace}, {nameof(PackageType)}: {PackageType}, {nameof(Creator)}: {Creator}, {nameof(Summary)}: {Summary}, {nameof(Description)}: {Description}, {nameof(DetailedDescription)}: {DetailedDescription}, {nameof(Version)}: {Version}, {nameof(CompatVersion)}: {CompatVersion}, {nameof(AlternateNames)}: [{string.Join(',', AlternateNames ?? Array.Empty<string>())}], {nameof(Extends)}: {Extends}, {nameof(Requires)}: [{string.Join(',', Requires ?? Array.Empty<string>())}], {nameof(Toolkit)}: {Toolkit}, {nameof(Sass)}: {Sass}, {nameof(Subpackages)}: {Subpackages}, {nameof(Classpath)}: [{string.Join(',', Classpath)}], {nameof(OverridesClasspath)}: [{string.Join(',', OverridesClasspath)}]>";
    }
}

public class SenchaPackageSubpackages
{
    [JsonInclude]
    [JsonPropertyName("dir")]
    public string? Path { get; private set; }
    
    [JsonInclude]
    public IEnumerable<string> Packages { get; private set; }

    public override string ToString()
    {
        return $"{nameof(Path)}: {Path}, {nameof(Packages)}: [{string.Join(',', Packages)}]";
    }
}

public enum SenchaPackageType
{
    Code,
    Theme,
    Toolkit
}

public class SenchaPackageSassInfo
{
    [JsonInclude]
    public string Namespace { get; private set; }

    [JsonInclude]
    [JsonExtensionData]
    public IDictionary<string, JsonElement> Extras { get; private set; }

    public IEnumerable<string> EtcClasspath => PackageUtil.GetClasspath(Extras, "etc");
    public IEnumerable<string> VarClasspath => PackageUtil.GetClasspath(Extras, "var");
    public IEnumerable<string> SrcClasspath => PackageUtil.GetClasspath(Extras, "src");

    public override string ToString()
    {
        return $"<{nameof(Namespace)}: {Namespace}, {nameof(EtcClasspath)}: [{string.Join(',', EtcClasspath)}], {nameof(VarClasspath)}: [{string.Join(',', VarClasspath)}], {nameof(SrcClasspath)}: [{string.Join(',', SrcClasspath)}]>";
    }
}