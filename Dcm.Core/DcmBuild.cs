using System.Collections.Concurrent;
using System.Text.Json;
using System.Text.Json.Serialization;
using Breezy.Dcm.Core.App;
using Breezy.Dcm.Core.Fixes;
using Breezy.Dcm.Core.Strict;
using Esprima;
using Esprima.Ast;
using Esprima.Utils;
using Microsoft.Extensions.Logging;

namespace Breezy.Dcm.Core;

public class DcmBuild
{
    private readonly ILogger<DcmBuild> Logger;
    private readonly ILoggerFactory LoggerFactory;

    private readonly BuildConfiguration BuildConfiguration;

    public Predicate<string>? ClassLoadFilter { get; set; }
    
    private DcmBuild(ILogger<DcmBuild> logger, ILoggerFactory loggerFactory, BuildConfiguration buildConfiguration)
    {
        Logger = logger;
        LoggerFactory = loggerFactory;
        BuildConfiguration = buildConfiguration;
    }

    public void SetVariable(string variable, string value) => BuildConfiguration.SetBuildVariable(variable, value);

    public async Task Build(IEnumerable<DirectoryInfo> packagesDir, DirectoryInfo appDir, string environment, string? profile, TextWriter? writer = null)
    {
        if (BuildConfiguration is not ApplicationBuildConfiguration appBuild)
            throw new Exception("Not an application build");
        
        Logger.LogInformation("Starting build for {environment} {profile}", environment, profile);

        appBuild.Environment = environment;
        appBuild.Profile = profile;
        
        await appBuild.DiscoverPackages(packagesDir, appDir);
        Logger.LogInformation("{} packages discovered", BuildConfiguration.Packages.Count);

        var buildAction = new ApplicationBuildAction(appBuild, LoggerFactory)
        {
            ClassLoadFilter = ClassLoadFilter
        };

        await buildAction.Build(writer);
    }

    public async Task GenerateTypeDefinitions(
        IEnumerable<DirectoryInfo> packagesDir,
        string toolkit,
        TextWriter? writer = null)
    {
        Logger.LogInformation("Starting build for {toolkit}", toolkit);
        await BuildConfiguration.DiscoverPackages(packagesDir, new List<string> { "core", toolkit }, toolkit, Logger);

        var buildAction = new TypeDefinitionBuildAction(BuildConfiguration, LoggerFactory)
        {
            ClassLoadFilter = ClassLoadFilter
        };

        var disposeWriter = false;

        if (writer is null)
        {
            disposeWriter = true;
            writer = File.CreateText("ext.d.ts");
        }
        
        await buildAction.Build(writer);

        if (disposeWriter)
            await writer.DisposeAsync();
    }

    public static DcmBuild Application(ILogger<DcmBuild> logger, ILoggerFactory loggerFactory)
    {
        return new DcmBuild(logger, loggerFactory, new ApplicationBuildConfiguration(loggerFactory.CreateLogger<ApplicationBuildConfiguration>()));
    }
    
    public static DcmBuild Generic(ILogger<DcmBuild> logger, ILoggerFactory loggerFactory)
    {
        return new DcmBuild(logger, loggerFactory, new BuildConfiguration());
    }
}