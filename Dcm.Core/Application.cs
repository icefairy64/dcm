using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;

namespace Breezy.Dcm.Core;

public class Application
{
    public string Name { get; set; }
    public string Framework { get; set; }
    public SenchaPackageSassInfo Sass { get; set; }
    public string IndexHtmlPath { get; set; }
    public string? Toolkit { get; set; }
    public string? Theme { get; set; }
    public IEnumerable<ApplicationJsEntry> Js { get; set; }
    public IEnumerable<ApplicationCssEntry> Css { get; set; }
    public IEnumerable<ApplicationResourceEntry> Resources { get; set; }
    public ApplicationOutputInfo Output { get; set; }
    public IDictionary<string, JsonElement>? Builds { get; set; }
    
    public string? Path { get; private set; }

    [JsonExtensionData]
    public IDictionary<string, JsonElement> Extras { get; set; }

    [JsonPropertyName("_classpath")]
    public IEnumerable<string> Classpath => PackageUtil.GetClasspath(Extras, "classpath");
    public IEnumerable<string> OverridesClasspath => PackageUtil.GetClasspath(Extras, "overrides");
    
    private IList<ClassFile>? FSassEtcFiles;
    private IList<ClassFile>? FSassVarFiles;
    private IList<ClassFile>? FSassSrcFiles;
    private IList<ClassFile>? FClasspathFiles;
    private IList<ClassFile>? FOverrideFiles;

    public IEnumerable<ClassFile>? SassEtcFiles => FSassEtcFiles;

    public IEnumerable<ClassFile>? SassVarFiles => FSassVarFiles;

    public IEnumerable<ClassFile>? SassSrcFiles => FSassSrcFiles;

    public IEnumerable<ClassFile>? ClasspathFiles => FClasspathFiles;

    public IEnumerable<ClassFile>? OverrideFiles => FOverrideFiles;

    public IDictionary<string, ApplicationBuild> Profiles =>
        new Dictionary<string, ApplicationBuild>(Builds?.Select(p =>
        {
            if (!p.Value.TryGetProperty("toolkit", out var toolkitEl))
                throw new Exception("No toolkit specified");
            if (!p.Value.TryGetProperty("theme", out var themeEl))
                throw new Exception("No theme specified");

            var toolkit = toolkitEl.GetString() ?? throw new Exception("toolkit is not a string");
            var theme = themeEl.GetString() ?? throw new Exception("theme is not a string");
            
            return new KeyValuePair<string, ApplicationBuild>(p.Key, new ApplicationBuild(toolkit, theme));
        }) ?? Array.Empty<KeyValuePair<string, ApplicationBuild>>());

    protected List<T> MergeArray<T>(string environment, string? profile, string prop)
    {
        var toolkit = Toolkit;
        var result = new List<T>();

        if (Extras.TryGetValue(prop, out var rootValue))
        {
            var data = rootValue switch
            {
                { ValueKind: JsonValueKind.Array } => rootValue.Deserialize<IEnumerable<T>>() ?? throw new Exception(),
                _ => new List<T>
                    { rootValue.Deserialize<T>() ?? throw new Exception($"Invalid {prop}") }
            };
            result.AddRange(data);
        }
        
        if (Extras.TryGetValue(environment, out var envOverrideEl))
        {
            if (envOverrideEl.TryGetProperty(prop, out var envValue))
            {
                var data = envValue switch
                {
                    { ValueKind: JsonValueKind.Array } => envValue.Deserialize<IEnumerable<T>>() ?? throw new Exception(),
                    _ => new List<T>
                        { rootValue.Deserialize<T>() ?? throw new Exception($"Invalid {prop}") }
                };
                result.AddRange(data);
            }
        }

        if (profile is not null && Builds is not null)
        {
            toolkit = Profiles[profile].Toolkit;
            if (Builds.TryGetValue(profile, out var profileOverrideEl))
            {
                if (profileOverrideEl.TryGetProperty(prop, out var profileValue))
                {
                    var data = profileValue switch
                    {
                        { ValueKind: JsonValueKind.Array } => profileValue.Deserialize<IEnumerable<T>>() ?? throw new Exception(),
                        _ => new List<T>
                            { rootValue.Deserialize<T>() ?? throw new Exception($"Invalid {prop}") }
                    };
                    result.AddRange(data);
                }
            }
        }

        if (toolkit is not null && Extras.TryGetValue(toolkit, out var toolkitOverrideEl))
        {
            if (toolkitOverrideEl.TryGetProperty(prop, out var toolkitValue))
            {
                var data = toolkitValue switch
                {
                    { ValueKind: JsonValueKind.Array } => toolkitValue.Deserialize<IEnumerable<T>>() ?? throw new Exception(),
                    _ => new List<T>
                        { rootValue.Deserialize<T>() ?? throw new Exception($"Invalid {prop}") }
                };
                result.AddRange(data);
            }
        }
        
        return result;
    }

    public IEnumerable<string> ResolvePackages(string environment, string? profile)
    {
        var result = MergeArray<string>(environment, profile, "requires");
        var theme = Theme;
        var toolkit = Toolkit;
        
        if (profile is not null)
        {
            var profileInfo = Profiles[profile];
            theme = profileInfo.Theme;
            toolkit = profileInfo.Toolkit;
        }

        if (toolkit is not null)
            result.Add(toolkit);
        
        if (theme is not null)
            result.Add(theme);

        return result;
    }

    public string ResolveToolkit(string? profile)
    {
        var toolkit = Toolkit;
        
        if (profile is not null)
        {
            var profileInfo = Profiles[profile];
            toolkit = profileInfo.Toolkit;
        }

        if (toolkit is null)
            throw new Exception("Could not determine toolkit");
        
        return toolkit;
    }

    public void ReadContents(string environment, string? profile, string path, IReadOnlyDictionary<string, string> vars)
    {
        Path = path;
        
        var theme = Theme;
        var toolkit = Toolkit;
        
        if (profile is not null)
        {
            var profileInfo = Profiles[profile];
            theme = profileInfo.Theme;
            toolkit = profileInfo.Toolkit;
        }

        if (toolkit is null)
            throw new Exception("Could not determine toolkit");
        
        if (theme is null)
            throw new Exception("Could not determine theme");

        var extendedVars = new Dictionary<string, string>(vars)
        {
            { "app.dir", path },
            { "toolkit.name", toolkit },
            { "theme.name", theme }
        };

        FSassEtcFiles = PackageUtil.ReadClasspath( Sass.Namespace, "scss", Sass.EtcClasspath, extendedVars, false);
        FSassVarFiles = PackageUtil.ReadClasspath(Sass.Namespace, "scss", Sass.VarClasspath, extendedVars, false);
        FSassSrcFiles = PackageUtil.ReadClasspath(Sass.Namespace, "scss", Sass.SrcClasspath, extendedVars, false);

        FClasspathFiles = PackageUtil.ReadClasspath(Name, "js", Classpath, extendedVars, false);
        FOverrideFiles = PackageUtil.ReadClasspath(Name, "js", OverridesClasspath, extendedVars, true);
        
        FClasspathFiles.Insert(0, new ClassFile("app", GetEntryPoint().FullName, false));
    }

    public FileInfo GetEntryPoint()
    {
        if (Path is null)
            throw new Exception("No path");

        var bundleEntry = Js.FirstOrDefault(e => e.Bundle == true);

        if (bundleEntry is null)
            throw new Exception("No JS bundle entry");

        return new FileInfo(System.IO.Path.Join(Path, bundleEntry.Path));
    }
}

public class ApplicationJsEntry
{
    public string Path { get; set; }
    public bool? IncludeInBundle { get; set; }
    public bool? Remote { get; set; }
    public bool? Bundle { get; set; }
}

public class ApplicationCssEntry
{
    public string Path { get; set; }
    public bool? Bundle { get; set; }
    public bool? Bootstrap { get; set; }
    public bool? Remote { get; set; }
    public IEnumerable<string>? Exclude { get; set; }
}

public class ApplicationResourceEntry
{
    public string Path { get; set; }
    public ResourceMode? Mode { get; set; }
    public string? Output { get; set; }
}

public class ApplicationOutputInfo
{
    public string Base { get; set; }
    public string Page { get; set; }
    public string Manifest { get; set; }
    public ApplicationJsOutputInfo Js { get; set; }
    public string Framework { get; set; }
    public IDictionary<string, string> Resources { get; set; }
}

public class ApplicationJsOutputInfo
{
    public string Path { get; set; }
    public string Version { get; set; }
}

public enum ResourceMode
{
    Relative
}

public class ApplicationBuild
{
    public string Toolkit { get; set; }
    public string Theme { get; set; }
    
    [JsonConstructor]
    public ApplicationBuild(string toolkit, string theme)
    {
        Toolkit = toolkit;
        Theme = theme;
    }
}