using System.Linq.Expressions;
using Esprima.Ast;

namespace Breezy.Dcm.Core;

public static class AstUtil
{
    public static T? DiveThroughFunctions<T>(this Node statement, Func<Node, T?> mapper) where T : class
    {
        if (mapper.Invoke(statement) is { } result)
            return result;

        return statement switch
        {
            Program program => program.Body.Select(s => s.DiveThroughFunctions(mapper)).FirstOrDefault(s => s is not null),
            BlockStatement block => block.Body.Select(s => s.DiveThroughFunctions(mapper)).FirstOrDefault(s => s is not null),
            ExpressionStatement exprStatement => exprStatement.Expression switch
            {
                CallExpression callExpr => callExpr.Callee switch
                {
                    FunctionExpression funcExpr => funcExpr.Body.DiveThroughFunctions(mapper),
                    _ => null
                },
                _ => null
            },
            _ => null
        };
    }
    
    public static T? DiveThroughDeclarations<T>(this Node statement, Func<Node, T?> mapper) where T : class
    {
        if (mapper.Invoke(statement) is { } result)
            return result;

        return statement switch
        {
            Program program => program.Body.Select(s => s.DiveThroughDeclarations(mapper)).FirstOrDefault(s => s is not null),
            BlockStatement block => block.Body.Select(s => s.DiveThroughDeclarations(mapper)).FirstOrDefault(s => s is not null),
            FunctionExpression function => function.Body.DiveThroughDeclarations(mapper),
            ExpressionStatement exprStatement => exprStatement.Expression switch
            {
                CallExpression callExpr => callExpr.Arguments.Select(a => a.DiveThroughDeclarations(mapper)).FirstOrDefault(a => a is not null),
                _ => null
            },
            _ => null
        };
    }

    public static NodeList<T> ReplaceNodes<T>(this NodeList<T> nodes, Func<T, object?> replacer) where T : Node
    {
        List<T>? newNodesList = new();
        IEnumerable<T> newNodes = newNodesList;

        foreach (var node in nodes)
        {
            var newNode = replacer.Invoke(node);
            switch (newNode)
            {
                case T singleNode:
                    newNodesList.Add(singleNode);
                    break;
                case IEnumerable<T> nodeRange:
                    newNodesList.AddRange(nodeRange);
                    break;
                case null:
                    newNodesList.Add(node);
                    break;
                default:
                    throw new Exception($"Unexpected replacement node {newNode.GetType().Name}");
            }
        }

        return NodeList.Create(newNodes);
    }

    public static void Replace<T, TN>(this T node, Expression<Func<T, TN>> expr) where T : Node
    {
        Console.WriteLine(expr);
    }
}