﻿namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptDefinitionEmitter
{
    private readonly TextWriter Writer;

    private int IndentLevel;

    public TypeScriptDefinitionEmitter(TextWriter writer)
    {
        Writer = writer;
    }

    public Task EmitAsync(TypeScriptNode node)
    {
        return node switch
        {
            TypeScriptNamespace ns => EmitNamespaceAsync(ns),
            TypeScriptClass cls => EmitClassAsync(cls),
            TypeScriptMethod mt => EmitMethodAsync(mt),
            TypeScriptObjectProperty prop => EmitPropertyAsync(prop),
            TypeScriptFunction fn => EmitFunctionAsync(fn),
            TypeScriptVariable vr => EmitVariableAsync(vr),
            TypeScriptType t => EmitTypeAsync(t),
            _ => Task.FromException(new Exception($"Unsupported node type {node.GetType().Name}"))
        };
    }

    private async Task EmitNamespaceAsync(TypeScriptNamespace node)
    {
        await Writer.WriteAsync("declare namespace ");
        await Writer.WriteAsync(node.Name);
        await Writer.WriteAsync(" {");
        IncreaseIndent();
        await Writer.WriteLineAsync();
        foreach (var child in node.Body)
        {
            await WriteIndentAsync();
            await EmitAsync(child);
            await Writer.WriteLineAsync();
        }
        DecreaseIndent();
        await WriteIndentAsync();
        await Writer.WriteAsync("}");
    }
    
    private async Task EmitClassAsync(TypeScriptClass node)
    {
        await Writer.WriteAsync("export class ");
        await Writer.WriteAsync(node.Name);
        if (node.Extends is { } parentName)
        {
            await Writer.WriteAsync(" extends ");
            await Writer.WriteAsync(parentName);
        }
        await Writer.WriteAsync(" {");
        IncreaseIndent();
        await Writer.WriteLineAsync();
        foreach (var child in node.Members)
        {
            await WriteIndentAsync();
            await EmitAsync(child);
            await Writer.WriteLineAsync();
        }
        DecreaseIndent();
        await WriteIndentAsync();
        await Writer.WriteAsync("}");
    }

    private async Task EmitFunctionAsync(TypeScriptFunction node)
    {
        await Writer.WriteAsync("export function ");
        await Writer.WriteAsync(node.Name);
        await Writer.WriteAsync("(");
        await EmitFunctionParametersAsync(node.Parameters);
        await Writer.WriteAsync("): ");
        await EmitTypeAsync(node.ReturnType);
    }
    
    private async Task EmitMethodAsync(TypeScriptMethod node)
    {
        await Writer.WriteAsync(node.Name);
        await Writer.WriteAsync("(");
        await EmitFunctionParametersAsync(node.Parameters);
        await Writer.WriteAsync("): ");
        await EmitTypeAsync(node.ReturnType);
    }

    private async Task EmitFunctionParametersAsync(IReadOnlyList<TypeScriptFunctionParameter> @params)
    {
        foreach (var param in @params)
        {
            await Writer.WriteAsync(param.Name);
            if (param.Optional)
                await Writer.WriteAsync('?');
            await Writer.WriteAsync(": ");
            await EmitTypeAsync(param.Type);
            if (param != @params[^1])
                await Writer.WriteAsync(", ");
        }
    }
    
    private async Task EmitVariableAsync(TypeScriptVariable node)
    {
        await Writer.WriteAsync("export let ");
        await Writer.WriteAsync(node.Name);
        await Writer.WriteAsync(": ");
        await EmitTypeAsync(node.Type);
    }
    
    private async Task EmitPropertyAsync(TypeScriptObjectProperty node)
    {
        await Writer.WriteAsync(node.Name);
        await Writer.WriteAsync(": ");
        await EmitTypeAsync(node.Type);
    }

    private Task EmitTypeAsync(TypeScriptType node)
    {
        return node switch
        {
            TypeScriptSimpleType st => EmitSimpleTypeAsync(st),
            TypeScriptUnionType ut => EmitUnionTypeAsync(ut),
            TypeScriptGenericType gt => EmitGenericTypeAsync(gt),
            _ => Task.FromException(new Exception($"Unsupported type {node.GetType().Name}"))
        };
    }

    private async Task EmitSimpleTypeAsync(TypeScriptSimpleType node)
    {
        await Writer.WriteAsync(node.Name);
    }
    
    private async Task EmitUnionTypeAsync(TypeScriptUnionType node)
    {
        await EmitTypeAsync(node.Left);
        await Writer.WriteAsync(" | ");
        await EmitTypeAsync(node.Right);
    }
    
    private async Task EmitGenericTypeAsync(TypeScriptGenericType node)
    {
        await Writer.WriteAsync(node.Name);
        await Writer.WriteAsync('<');
        foreach (var param in node.Parameters)
        {
            await EmitTypeAsync(param);
            if (param != node.Parameters[^1])
                await Writer.WriteAsync(", ");
        }
        await Writer.WriteAsync('>');
    }

    private async Task WriteIndentAsync()
    {
        for (var i = 0; i < IndentLevel; i++)
            await Writer.WriteAsync("    ");
    }

    private async Task NewLineAsync()
    {
        await Writer.WriteLineAsync();
        await WriteIndentAsync();
    }

    private void IncreaseIndent()
    {
        IndentLevel++;
    }

    private void DecreaseIndent()
    {
        IndentLevel--;
    }
}