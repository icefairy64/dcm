﻿namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptGenericType : TypeScriptType
{
    public required string Name { get; init; }
    public required IReadOnlyList<TypeScriptType> Parameters { get; set; }

    public override string ToString()
    {
        return $"{nameof(Name)}: {Name}, {nameof(Parameters)}: {Parameters.JoinString(x => x.ToString())}";
    }
}