﻿namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptVariable : TypeScriptNode
{
    public required string Name { get; set; }
    public required TypeScriptType Type { get; set; }
}