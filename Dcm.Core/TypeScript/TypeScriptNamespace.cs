﻿namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptNamespace : TypeScriptNode
{
    public string Name { get; init; }
    public IReadOnlyList<TypeScriptNode> Body { get; init; }
}