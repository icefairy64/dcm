﻿using System.Globalization;
using Breezy.Dcm.Core.Classes;

namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptDefinitionGenerator
{
    private readonly ClassExamineVisitor ExamineVisitor;

    public TypeScriptDefinitionGenerator(ClassExamineVisitor examineVisitor)
    {
        ExamineVisitor = examineVisitor;
    }

    public IEnumerable<TypeScriptNode> GenerateDefinition(string className)
    {
        if (ExamineVisitor.GetClassDeclaration(className) is not {} cls)
            throw new Exception("Class not found");

        return GenerateDefinition(cls);
    }
    
    public IEnumerable<TypeScriptNode> GenerateDefinition(IClassDeclaration @class)
    {
        var result = new List<TypeScriptNode>();
        
        if (GeneratePrototypeDefinition(@class) is {} proto)
            result.Add(proto);
        
        if (GenerateStaticDefinition(@class) is {} @static)
            result.Add(@static);

        return result;
    }

    private TypeScriptNamespace? GeneratePrototypeDefinition(IClassDeclaration cls)
    {
        if (cls is ExtClassDefineDeclaration { IsSingleton: true })
            return null;
        
        var members = new List<TypeScriptNode>();
        var presentMembers = new HashSet<string>();

        foreach (var prop in cls.Properties.Where(p => p.OwnerLevel is ExtClassMemberOwner.Instance))
        {
            presentMembers.Add(prop.Name);
            members.Add(new TypeScriptObjectProperty
            {
                Name = prop.Name,
                Type = ParseType(prop.Type)
            });
        }
        
        foreach (var method in cls.Methods.Where(p => p.OwnerLevel is ExtClassMemberOwner.Instance))
        {
            presentMembers.Add(method.Name);
            members.Add(new TypeScriptMethod()
            {
                Name = method.Name,
                ReturnType = ParseType(method.ReturnType),
                Parameters = method.Parameters.Select(param => new TypeScriptFunctionParameter
                {
                    Name = param.Name,
                    Type = ParseType(param.Type),
                    Optional = param.Optional,
                    DefaultValue = param.DefaultValue
                }).ToList()
            });
        }
        
        foreach (var cfg in cls.Configs)
        {
            if (!presentMembers.Contains($"get{Capitalize(cfg.Name)}"))
            {
                presentMembers.Add($"get{Capitalize(cfg.Name)}");
                members.Add(new TypeScriptMethod()
                {
                    Name = $"get{Capitalize(cfg.Name)}",
                    ReturnType = ParseType(cfg.Type),
                    Parameters = new List<TypeScriptFunctionParameter>()
                });
            }
            
            if (!presentMembers.Contains($"set{Capitalize(cfg.Name)}"))
            {
                presentMembers.Add($"set{Capitalize(cfg.Name)}");
                members.Add(new TypeScriptMethod()
                {
                    Name = $"set{Capitalize(cfg.Name)}",
                    ReturnType = new TypeScriptSimpleType { Name = "void" },
                    Parameters = new List<TypeScriptFunctionParameter>
                    {
                        new TypeScriptFunctionParameter
                        {
                            Name = "value",
                            Type = ParseType(cfg.Type),
                            Optional = false,
                            DefaultValue = null
                        }
                    }
                });
            }
        }

        return new TypeScriptNamespace
        {
            Name = cls.EffectiveClassName.Split('.')[..^1].JoinString('.'),
            Body = new List<TypeScriptNode>
            {
                new TypeScriptClass
                {
                    Name = cls.EffectiveClassName.Split('.')[^1],
                    Extends = ExamineVisitor.GetParentClass(cls)?.ClassName ?? (cls.ClassName is not "Ext.Base" ? "Ext.Base" : null),
                    Members = members
                }
            }
        };
    }
    
    private TypeScriptNamespace? GenerateStaticDefinition(IClassDeclaration cls)
    {
        var members = new List<TypeScriptNode>();
        
        // TODO: inherit statics from parent
        
        foreach (var prop in cls.Properties.Where(p => p.OwnerLevel is not ExtClassMemberOwner.Instance))
        {
            members.Add(new TypeScriptVariable()
            {
                Name = prop.Name,
                Type = ParseType(prop.Type)
            });
        }
        
        foreach (var method in cls.Methods.Where(p => p.OwnerLevel is not ExtClassMemberOwner.Instance))
        {
            members.Add(new TypeScriptFunction()
            {
                Name = method.Name,
                ReturnType = ParseType(method.ReturnType),
                Parameters = method.Parameters.Select(param => new TypeScriptFunctionParameter
                {
                    Name = param.Name,
                    Type = ParseType(param.Type),
                    Optional = param.Optional,
                    DefaultValue = param.DefaultValue
                }).ToList()
            });
        }

        if (!members.Any())
            return null;
        
        return new TypeScriptNamespace
        {
            Name = cls.EffectiveClassName,
            Body = members
        };
    }

    private TypeScriptType ParseType(string? type)
    {
        if (type is null)
            return new TypeScriptSimpleType { Name = "any" };

        var options = type.Split('/').Select(x => x.Trim()).ToList();
        
        if (options.Count > 1)
            return TypeListToUnion(options.Select(ParseType));

        if (options[0].EndsWith("[]"))
            return new TypeScriptGenericType
            {
                Name = "globalThis.Array",
                Parameters = new List<TypeScriptType>
                {
                    ParseType(options[0][..^2])
                }
            };

        var name = options[0] switch
        {
            "Boolean" => "boolean",
            "String" => "string",
            "Number" => "number",
            "Object" => "object",
            "Mixed" => "any",
            "Arguments" => "IArguments",
            { } n => n
        };

        if (name == "Array")
        {
            return new TypeScriptGenericType
            {
                Name = "globalThis.Array",
                Parameters = new List<TypeScriptType>
                {
                    new TypeScriptSimpleType { Name = "any" }
                }
            };
        }

        return new TypeScriptSimpleType { Name = name };
    }

    private TypeScriptUnionType TypeListToUnion(IEnumerable<TypeScriptType> types)
    {
        return types.Count() > 2
            ? new TypeScriptUnionType { Left = types.ElementAt(0), Right = TypeListToUnion(types.Skip(1)) }
            : new TypeScriptUnionType { Left = types.ElementAt(0), Right = types.ElementAt(1) };
    }

    private static string Capitalize(string str)
    {
        return str[0].ToString().ToUpperInvariant() + str[1..];
    }
}