﻿namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptObjectProperty : TypeScriptNode
{
    public required string Name { get; init; }
    public bool Optional { get; init; }
    public required TypeScriptType Type { get; init; }
}