﻿namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptFunction : TypeScriptNode
{
    public required string Name { get; init; }
    public required TypeScriptType ReturnType { get; init; }
    public required IReadOnlyList<TypeScriptFunctionParameter> Parameters { get; init; }
}

public class TypeScriptFunctionParameter : TypeScriptNode
{
    public required string Name { get; init; }
    public required TypeScriptType Type { get; init; }
    public bool Optional { get; init; }
    public string? DefaultValue { get; init; }
}