﻿namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptUnionType : TypeScriptType
{
    public required TypeScriptType Left { get; init; }
    public required TypeScriptType Right { get; init; }

    public override string ToString()
    {
        return $"{nameof(Left)}: {Left}, {nameof(Right)}: {Right}";
    }
}