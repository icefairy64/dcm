﻿namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptClass : TypeScriptNode
{
    public required string Name { get; init; }
    public string? Extends { get; init; }
    public required IReadOnlyList<TypeScriptNode> Members { get; init; }
}