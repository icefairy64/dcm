﻿namespace Breezy.Dcm.Core.TypeScript;

public class TypeScriptSimpleType : TypeScriptType
{
    public required string Name { get; init; }

    public override string ToString()
    {
        return $"{nameof(Name)}: {Name}";
    }
}