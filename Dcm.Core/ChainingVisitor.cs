﻿using Esprima.Ast;
using Esprima.Utils;

namespace Breezy.Dcm.Core;

public class ChainingVisitor : AstRewriter
{
    public required IReadOnlyList<AstVisitor> Visitors { private get; init; }
    
    public override object? Visit(Node node)
    {
        var result = Visitors.Aggregate(node, (current, visitor) => visitor.Visit(current) switch
        {
            Node newNode => newNode,
            _ => throw new Exception("Not a node")
        });
        return base.Visit(result);
    }
}