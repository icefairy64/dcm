﻿using System.Collections.Concurrent;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Logging;

namespace Breezy.Dcm.Core;

public class BuildConfiguration
{
    private readonly Dictionary<string, string> FBuildVariables = new();
    private readonly List<Package> FPackages = new();

    public IReadOnlyDictionary<string, string> BuildVariables => FBuildVariables;
    public IReadOnlyList<Package> Packages => FPackages;

    protected readonly JsonSerializerOptions JsonOptions = new JsonSerializerOptions
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        ReadCommentHandling = JsonCommentHandling.Skip,
        Converters =
        {
            new JsonStringEnumConverter(JsonNamingPolicy.CamelCase)
        }
    };

    public void SetBuildVariable(string key, string value)
    {
        FBuildVariables[key] = value;
    }

    public void AddPackage(Package package)
    {
        FPackages.Add(package);
    }
    
    protected async Task<Package> ReadPackage(FileInfo descriptorFile, CancellationToken token)
    {
        var json = await File.ReadAllTextAsync(descriptorFile.FullName, token).ConfigureAwait(false);
        try
        {
            var pkg = JsonSerializer.Deserialize<Package>(json, JsonOptions);
            if (pkg is null)
                throw new Exception($"Failed to read package from {descriptorFile.FullName}");
            pkg.ReadContents(descriptorFile.DirectoryName ?? throw new Exception("No directory"), BuildVariables);
            return pkg;
        }
        catch (Exception e)
        {
            throw new Exception($"Failed to read package from {descriptorFile.FullName}", e);
        }
    }
    
    public async Task DiscoverPackages(IEnumerable<DirectoryInfo> packagesDir, IEnumerable<string> rootPackages, string appToolkit, ILogger? logger)
    {
        var packageDescriptors = packagesDir.SelectMany(d => d.EnumerateFiles("package.json", SearchOption.AllDirectories));
        var packages = new ConcurrentQueue<Package>();
        await Parallel.ForEachAsync(packageDescriptors, async (info, token) =>
        {
            var pkg = await ReadPackage(info, token);
            packages.Enqueue(pkg);
        });

        var includedPackages = new List<string>(rootPackages);
        var allPackages = packages.Where(p => p.Sencha.Toolkit == null || p.Sencha.Toolkit == appToolkit).ToDictionary(p => p.Sencha.Name);

        do
        {
            var newPackages = new List<string>();

            foreach (var packageName in includedPackages)
            {
                if (allPackages.TryGetValue(packageName, out var package))
                {
                    if (package.Sencha.Extends is not null)
                    {
                        if (!newPackages.Contains(package.Sencha.Extends))
                            newPackages.Add(package.Sencha.Extends);
                    }
                    
                    if (package.Sencha.Requires is not null)
                    {
                        foreach (var dependencyName in package.Sencha.Requires)
                        {
                            if (!newPackages.Contains(dependencyName))
                                newPackages.Add(dependencyName);
                        }
                    }
                }

                if (!newPackages.Contains(packageName))
                    newPackages.Add(packageName);
            }
            
            if (newPackages.SequenceEqual(includedPackages))
                break;

            includedPackages = newPackages;
        } while (true);
        
        foreach (var missingPackageName in includedPackages.Where(p => !allPackages.ContainsKey(p)))
            logger?.LogWarning("Package {missingPackageName} is declared as application dependency but was not found",
                missingPackageName);

        foreach (var package in includedPackages.SelectMany(p => allPackages.TryGetValue(p, out var pkg) ? new List<Package> { pkg } : new List<Package>()))
            AddPackage(package);
    }
}