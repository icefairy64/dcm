using Esprima.Ast;
using Esprima.Utils;

namespace Breezy.Dcm.Core;

public class NodeReplacer<T> : AstRewriter where T : Node
{
    public T What { private get; init; }
    public T With { private get; init; }

    public override object? Visit(Node node)
    {
        if (What == With)
            return node;
        return node == What ? With : base.Visit(node);
    }
}