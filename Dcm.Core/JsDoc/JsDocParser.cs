﻿using System.Text.RegularExpressions;
using Breezy.Dcm.Core.Classes;
using Esprima;
using Esprima.Ast;

namespace Breezy.Dcm.Core.JsDoc;

public static class JsDocParser
{
    public static IEnumerable<object> ExtractJsDocTags(string jsDoc)
    {
        return jsDoc.Replace("\r", "")
            .Split('\n')
            .Select(x => x.Trim())
            .Where(x => x.Length > 1)
            .Select(x => x[1..].Trim())
            .Select(ExtractJsDocTag)
            .Where(x => x is not null)
            .Cast<object>();
    }
    
    private static object? ExtractJsDocTag(string jsDocLine)
    {
        if (jsDocLine.StartsWith("@param"))
            return JsDocParam.Parse(jsDocLine);
        if (jsDocLine.StartsWith("@property"))
            return JsDocProperty.Parse(jsDocLine);
        if (jsDocLine.StartsWith("@return"))
            return JsDocReturn.Parse(jsDocLine);
        if (jsDocLine.StartsWith("@class"))
            return JsDocClass.Parse(jsDocLine);
        if (jsDocLine.StartsWith("@cfg"))
            return JsDocCfg.Parse(jsDocLine);
        if (jsDocLine.StartsWith("@static"))
            return new JsDocStatic();
        return null;
    }
    
    public static string? FindJsDoc(IClassDeclaration classDeclaration, SyntaxElement node)
    {
        if (classDeclaration?.Program.Comments is not { } comments)
            return null;

        return comments.Where(c =>
                c.Type is CommentType.Block &&
                c.Value[0] == '*' &&
                c.Location.End.Line == node.Location.Start.Line - 1)
            .Select(c => c.Value)
            .FirstOrDefault();
    }
}

internal partial class JsDocParam
{
    private static readonly Regex LineRegex = JsDocParamRegex();
    
    public required string Name { get; init; }
    public string? Type { get; init; }
    public string? Description { get; init; }
    public bool Optional { get; init; }
    public string? DefaultValue { get; init; }

    [GeneratedRegex("^@param( +?\\{.+?\\})? *(\\[?[\\w\\.\\/\\$]+?=?.*?\\]?)( +?.+?)?$")]
    private static partial Regex JsDocParamRegex();

    public ExtClassMethodParameter ToExtClassMethodParameter()
    {
        return new ExtClassMethodParameter
        {
            Name = Name,
            Type = Type,
            Optional = Optional,
            DefaultValue = DefaultValue
        };
    }

    public static JsDocParam Parse(string line, Regex? regex = null)
    {
        regex ??= LineRegex;
        
        if (regex.Match(line) is not { Success: true } match)
            throw new Exception($"Invalid @param syntax: '{line}'");

        var type = match.Groups[1].Success ? match.Groups[1].Value.Trim() : null;
        var name = match.Groups[2].Success ? match.Groups[2].Value.Trim() : "";
        var optional = false;
        string? defaultValue = null;
        var description = match.Groups[3].Success ? match.Groups[3].Value.Trim() : null;

        if (name.StartsWith('['))
        {
            optional = true;
            name = name[1..^1];
        }

        if (name.Contains('='))
        {
            var parts = name.Split('=');
            name = parts[0];
            defaultValue = parts[1];
        }

        return new JsDocParam
        {
            Type = type?[1..^1]?.Trim(),
            Name = name,
            Description = description,
            Optional = optional,
            DefaultValue = defaultValue
        };
    }
}

internal partial class JsDocProperty
{
    private static readonly Regex LineRegex = JsDocPropertyRegex();
    
    public string? Type { get; init; }
    
    [GeneratedRegex("^@property ?(\\{.+?\\})? ?(.+?)?$")]
    private static partial Regex JsDocPropertyRegex();
    
    public static JsDocProperty Parse(string line)
    {
        if (LineRegex.Match(line) is not { Success: true } match)
            throw new Exception($"Invalid @property syntax: '{line}'");

        var type = match.Groups[1].Success ? match.Groups[1].Value : null;

        return new JsDocProperty()
        {
            Type = type?[1..^1]?.Trim()
        };
    }
}

internal partial class JsDocReturn
{
    private static readonly Regex LineRegex = JsDocReturnRegex();
    
    public string? Type { get; init; }
    public string? Description { get; init; }
    
    [GeneratedRegex("^@returns? (\\{.+?\\})? ?(.+?)?$")]
    private static partial Regex JsDocReturnRegex();
    
    public static JsDocReturn Parse(string line)
    {
        if (LineRegex.Match(line) is not { Success: true } match)
            throw new Exception($"Invalid @return syntax: '{line}'");

        var type = match.Groups[1].Success ? match.Groups[1].Value : null;
        var description = match.Groups[2].Value;

        return new JsDocReturn
        {
            Type = type?[1..^1]?.Trim(),
            Description = description
        };
    }
}

internal class JsDocStatic
{
    
}

internal partial class JsDocClass
{
    private static readonly Regex LineRegex = JsDocReturnRegex();
    
    public string? Type { get; init; }
    public string? Name { get; init; }
    
    [GeneratedRegex("^@class (\\{.+?\\})? ?(.+?)?$")]
    private static partial Regex JsDocReturnRegex();
    
    public static JsDocClass Parse(string line)
    {
        if (LineRegex.Match(line) is not { Success: true } match)
            throw new Exception($"Invalid @class syntax: '{line}'");

        var type = match.Groups[1].Success ? match.Groups[1].Value : null;
        var name = match.Groups[2].Value;

        return new JsDocClass
        {
            Type = type?[1..^1]?.Trim(),
            Name = name
        };
    }
}

internal partial class JsDocCfg
{
    public required string Name { get; init; }
    public string? Type { get; init; }
    public string? Description { get; init; }
    public bool Optional { get; init; }
    public string? DefaultValue { get; init; }
    
    [GeneratedRegex("^@cfg( +?\\{.+?\\})?( *\\[?[\\w\\.\\/]+?=?.*?\\]?)?( +?.+?)?$")]
    private static partial Regex JsDocCfgRegex();
    
    public static JsDocCfg Parse(string line)
    {
        var param = JsDocParam.Parse(line, JsDocCfgRegex());
        return new JsDocCfg
        {
            Name = param.Name,
            Description = param.Description,
            Optional = param.Optional,
            DefaultValue = param.DefaultValue,
            Type = param.DefaultValue
        };
    }
}