﻿using System.Text.RegularExpressions;
using Breezy.Dcm.Core.JsDoc;
using Esprima;
using Esprima.Ast;
using Esprima.Utils;

namespace Breezy.Dcm.Core.Classes;

public class ExtClassDefineStructureVisitor : DefineVisitor
{
    public override IEnumerable<Property>? VisitMethod(Property methodProperty, ExtClassMemberOwner owner)
    {
        if (CurrentClass is null)
            throw new Exception();
        
        var jsDoc = JsDocParser.FindJsDoc(CurrentClass, methodProperty);

        string? returnType = null;
        Dictionary<string, JsDocParam>? parameters = null;

        if (jsDoc is not null)
        {
            var tags = JsDocParser.ExtractJsDocTags(jsDoc).ToList();
            if (tags.OfType<JsDocReturn>().FirstOrDefault() is { } returnTag)
                returnType = returnTag.Type;
            parameters = new Dictionary<string, JsDocParam>();
            foreach (var paramTag in tags.OfType<JsDocParam>().Where(p => p.Type is not null))
                parameters[paramTag.Name] = paramTag;
        }

        CurrentClass?.Methods.Add(new ExtClassMethod
        {
            Name = methodProperty.Key switch
            {
                Identifier id => id.Name,
                Literal lt => lt.StringValue ?? throw new Exception("Not a string literal"),
                _ => throw new Exception($"Invalid method name type {methodProperty.Key.GetType().Name} for {methodProperty.Key}")
            },
            OwnerLevel = owner,
            JsDoc = jsDoc,
            ReturnType = returnType,
            Parameters = (methodProperty.Value as FunctionExpression)?.Params.Select(p => (parameters?.ContainsKey(GetFunctionParameterName(p)) ?? false)
                ? parameters[GetFunctionParameterName(p)].ToExtClassMethodParameter()
                : new ExtClassMethodParameter
                {
                    Name = GetFunctionParameterName(p)
                }).ToList() ?? new List<ExtClassMethodParameter>()
        });
        return null;
    }

    private static string GetFunctionParameterName(Node node)
    {
        return node switch
        {
            Identifier id => id.Name,
            RestElement re => (re.Argument as Identifier)?.Name ?? throw new Exception(),
            _ => throw new Exception()
        };
    }

    public override IEnumerable<Property>? VisitProperty(Property property, ExtClassMemberOwner owner)
    {
        if (CurrentClass is null)
            throw new Exception();

        var propName = property.Key switch
        {
            Identifier id => id.Name,
            Literal lt => lt.StringValue ?? throw new Exception("Not a string literal"),
            _ => throw new Exception($"Invalid property name type {property.Key.GetType().Name} for {property.Key}")
        };

        if (propName is "extend" or "requires" or "uses" or "singleton" or "override" or "alternateClassNames")
            return null;

        var jsDoc = JsDocParser.FindJsDoc(CurrentClass, property);

        string? type = null;

        if (jsDoc is not null)
        {
            var tags = JsDocParser.ExtractJsDocTags(jsDoc).ToList();
            if (tags.OfType<JsDocProperty>().FirstOrDefault() is { } propTag)
                type = propTag.Type;
        }

        type ??= property.Value switch
        {
            Literal { BooleanValue: not null } => "boolean",
            Literal { NumericValue: not null } => "number",
            Literal { StringValue: not null } => "string",
            ObjectExpression => "object",
            _ => null
        };
        
        CurrentClass?.Properties.Add(new ExtClassProperty
        {
            Name = propName,
            OwnerLevel = owner,
            JsDoc = jsDoc,
            Type = type
        });
        return null;
    }

    public override IEnumerable<Property>? VisitConfig(Property property)
    {
        if (CurrentClass is null)
            throw new Exception();
        
        var name = (property.Key as Identifier)?.Name ?? throw new Exception();

        string? type = null;

        if (JsDocParser.FindJsDoc(CurrentClass, property) is { } jsDoc)
        {
            type = JsDocParser.ExtractJsDocTags(jsDoc)
                .OfType<JsDocCfg>()
                .Select(c => c.Type)
                .FirstOrDefault();
        }
        
        type ??= CurrentClass?.Program.Comments?
            .Where(c => c.Type is CommentType.Block && c.Value[0] == '*')
            .Select(c => c.Value)
            .SelectMany(JsDocParser.ExtractJsDocTags)
            .OfType<JsDocCfg>()
            .Where(c => c.Name == name)
            .Select(c => c.Type)
            .FirstOrDefault();

        type ??= property.Value switch
        {
            Literal { BooleanValue: not null } => "boolean",
            Literal { NumericValue: not null } => "number",
            Literal { StringValue: not null } => "string",
            ObjectExpression => "object",
            _ => null
        };

        CurrentClass?.Configs.Add(new ExtClassConfig
        {
            Name = name,
            Type = type
        });
        return null;
    }
}