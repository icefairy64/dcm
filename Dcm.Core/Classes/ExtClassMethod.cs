﻿using Esprima;

namespace Breezy.Dcm.Core.Classes;

public class ExtClassMethod
{
    public required string Name { get; init; }
    public ExtClassMemberOwner OwnerLevel { get; init; }
    public required IReadOnlyList<ExtClassMethodParameter> Parameters { get; init; }
    public string? ReturnType { get; init; } = null;
    public string? JsDoc { get; init; } = null;

    public override string ToString()
    {
        return $"{nameof(Name)}: {Name}, {nameof(OwnerLevel)}: {OwnerLevel}, {nameof(Parameters)}: {string.Join(", ", Parameters.Select(p => p.ToString()))}, {nameof(ReturnType)}: {ReturnType}";
    }
}

public class ExtClassMethodParameter
{
    public required string Name { get; init; }
    public string? Type { get; init; } = null;
    public bool Optional { get; init; }
    public string? DefaultValue { get; init; } = null;

    public override string ToString()
    {
        return $"{nameof(Name)}: {Name}, {nameof(Type)}: {Type}, {nameof(Optional)}: {Optional}, {nameof(DefaultValue)}: {DefaultValue}";
    }
}