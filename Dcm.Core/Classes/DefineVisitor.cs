using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public abstract class DefineVisitor
{
    protected ExtClassDefineDeclaration? CurrentClass;
    
    public virtual void EnterClass(ExtClassDefineDeclaration cls)
    {
        CurrentClass = cls;
    }

    public virtual void LeaveClass(ExtClassDefineDeclaration cls)
    {
        CurrentClass = null;
    }

    public abstract IEnumerable<Property>? VisitMethod(Property methodProperty, ExtClassMemberOwner owner);
    public abstract IEnumerable<Property>? VisitProperty(Property property, ExtClassMemberOwner owner);

    public virtual IEnumerable<Property>? VisitConfig(Property property)
    {
        return null;
    }
}