﻿using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public interface IClassDeclaration
{
    public string? ClassName { get; }
    public ClassFile File { get; }
    public IEnumerable<SyntaxComment> Comments => Program.Comments ?? Array.Empty<SyntaxComment>();
    public Program Program { get; }
    public Node ClassDefinition { get; }
    
    public List<ExtClassProperty> Properties { get; }
    public List<ExtClassMethod> Methods { get; }
    public List<ExtClassConfig> Configs { get; }

    public string EffectiveClassName => ClassName ?? File.ClassName;

    public void AcceptClassDeclarationVisitor(IClassDeclarationVisitor visitor);
}