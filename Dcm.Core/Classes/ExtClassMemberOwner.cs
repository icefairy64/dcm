﻿namespace Breezy.Dcm.Core.Classes;

public enum ExtClassMemberOwner
{
    Instance,
    Static,
    InheritableStatic
}