﻿using Esprima;

namespace Breezy.Dcm.Core.Classes;

public class ExtClassProperty
{
    public required string Name { get; init; }
    public ExtClassMemberOwner OwnerLevel { get; init; }
    public string? Type { get; init; } = null;
    public string? JsDoc { get; init; } = null;

    public override string ToString()
    {
        return $"{nameof(Name)}: {Name}, {nameof(OwnerLevel)}: {OwnerLevel}, {nameof(Type)}: {Type}";
    }
}