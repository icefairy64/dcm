﻿using Breezy.Dcm.Core.JsDoc;
using Esprima;
using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public class ExtClassAssignmentDeclaration : IClassDeclaration
{
    public string ClassName { get; private set; }
    public ClassFile File { get; }
    public Program Program { get; private set; }
    public Node AssignmentExpression { get; private set; }
    public Node ClassDefinition => AssignmentExpression;
    
    public List<ExtClassProperty> Properties { get; } = new();
    public List<ExtClassMethod> Methods { get; } = new();
    public List<ExtClassConfig> Configs { get; } = new();

    public ExtClassAssignmentDeclaration(string className, ClassFile file, Node assignmentExpression, Program program)
    {
        ClassName = className;
        File = file;
        AssignmentExpression = assignmentExpression;
        Program = program;
        TryFindJsDocClassName();
    }

    private void TryFindJsDocClassName()
    {
        var classTag = Program.Comments?
            .Where(c => c.Type is CommentType.Block && c.Value[0] == '*')
            .SelectMany(c => JsDocParser.ExtractJsDocTags(c.Value))
            .OfType<JsDocClass>()
            .FirstOrDefault(c => c.Name is not null);
        if (classTag is null)
            return;
        ClassName = classTag.Name ?? ClassName;
    }
    
    public void AcceptClassDeclarationVisitor(IClassDeclarationVisitor visitor)
    {
        var prevProgram = Program;
        var result = visitor.VisitClassDeclaration(this, Program);
        if (prevProgram == result)
            return;
        switch (result)
        {
            case Esprima.Ast.Program program:
                Program = program;
                // TODO: re-capture assignment expression
                break;
            default:
                throw new Exception($"Unexpected node {result.Type}");
        }
    }

    public override string ToString()
    {
        return $"<AssignmentDeclaration {nameof(ClassName)}: {ClassName}>";
    }
}