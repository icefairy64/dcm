﻿using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public class ExtClassApplyDeclaration : IClassDeclaration
{
    private CallExpression ApplyCallExpression;
    public string? ClassName { get; }
    public ClassFile File { get; }
    public Program Program { get; private set; }
    public string Target { get; }
    public Node ClassDefinition => ApplyCallExpression;
    
    public List<ExtClassProperty> Properties { get; } = new();
    public List<ExtClassMethod> Methods { get; } = new();
    public List<ExtClassConfig> Configs { get; } = new();
    
    public ExtClassApplyDeclaration(string? className, ClassFile file, string target, CallExpression applyCallExpression, Program program)
    {
        ApplyCallExpression = applyCallExpression;
        ClassName = className;
        File = file;
        Target = target;
        Program = program;
    }

    public void AcceptClassDeclarationVisitor(IClassDeclarationVisitor visitor)
    {
        var prevCall = ApplyCallExpression;
        var result = visitor.VisitClassDeclaration(this, prevCall);
        if (prevCall == result)
            return;
        switch (result)
        {
            case CallExpression callExpression:
                ApplyCallExpression = callExpression;
                Program = new NodeReplacer<CallExpression>
                {
                    What = prevCall,
                    With = callExpression
                }.VisitAndConvert(Program);
                break;
            default:
                throw new Exception($"Unexpected node {result.Type}");
        }
    }
}