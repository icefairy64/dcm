using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public class ChainingDefineVisitor : DefineVisitor
{
    public IReadOnlyList<DefineVisitor> Visitors { private get; init; }

    public override void EnterClass(ExtClassDefineDeclaration cls)
    {
        base.EnterClass(cls);
        foreach (var visitor in Visitors)
            visitor.EnterClass(cls);
    }

    public override void LeaveClass(ExtClassDefineDeclaration cls)
    {
        foreach (var visitor in Visitors)
            visitor.LeaveClass(cls);
        base.LeaveClass(cls);
    }

    public override IEnumerable<Property>? VisitMethod(Property methodProperty, ExtClassMemberOwner owner)
    {
        var methods = new List<Property> { methodProperty };
        
        foreach (var visitor in Visitors)
        {
            var replacement = new List<Property>();
            foreach (var method in methods)
            {
                if (visitor.VisitMethod(method, owner) is { } newMethods)
                    replacement.AddRange(newMethods);
                else
                    replacement.Add(method);
            }

            methods = replacement;
        }

        return methods;
    }

    public override IEnumerable<Property>? VisitProperty(Property property, ExtClassMemberOwner owner)
    {
        var properties = new List<Property> { property };
        
        foreach (var visitor in Visitors)
        {
            var replacement = new List<Property>();
            foreach (var prop in properties)
            {
                if (visitor.VisitProperty(prop, owner) is { } newMethods)
                    replacement.AddRange(newMethods);
                else
                    replacement.Add(prop);
            }

            properties = replacement;
        }

        return properties;
    }
    
    public override IEnumerable<Property>? VisitConfig(Property property)
    {
        var properties = new List<Property> { property };
        
        foreach (var visitor in Visitors)
        {
            var replacement = new List<Property>();
            foreach (var prop in properties)
            {
                if (visitor.VisitConfig(prop) is { } newMethods)
                    replacement.AddRange(newMethods);
                else
                    replacement.Add(prop);
            }

            properties = replacement;
        }

        return properties;
    }
}