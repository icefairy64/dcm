using System.Text.RegularExpressions;
using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public class ClassExamineVisitor : DefineVisitor
{
    private static readonly Regex RequireCommentRegex = new("@require ([\\S]+)");
    private static readonly Regex DefineCommentRegex = new("@define ([\\S]+)");
    private static readonly Regex UsesCommentRegex = new("@uses ([\\S]+)");
    
    private readonly Dictionary<string, IClassDeclaration> FClasses = new();
    private readonly Dictionary<string, IEnumerable<string>> FRequires = new();
    private readonly Dictionary<string, IEnumerable<string>> FMixins = new();
    private readonly Dictionary<string, IEnumerable<string>> FUses = new();
    private readonly Dictionary<string, string> FOverrides = new();
    private readonly Dictionary<string, IList<IClassDeclaration>> FClassOverrides = new();
    private readonly Dictionary<string, string> FExtends = new();
    private readonly HashSet<string> FSingletons = new();

    private readonly Dictionary<string, IList<string>?> EffectiveDependencies = new();

    private bool IsOverride;
    
    public IReadOnlyDictionary<string, IClassDeclaration> Classes => FClasses;
    public IReadOnlyDictionary<string, IEnumerable<string>> Requires => FRequires;
    public IReadOnlyDictionary<string, string> Overrides => FOverrides;
    public IReadOnlyDictionary<string, string> Extends => FExtends;

    public ExtApplicationDeclaration? ApplicationDeclaration { get; private set; }
    
    public override void EnterClass(ExtClassDefineDeclaration cls)
    {
        base.EnterClass(cls);
        IsOverride = false;
    }

    public override void LeaveClass(ExtClassDefineDeclaration cls)
    {
        if (cls.ClassName is not null)
            FClasses[cls.ClassName] = cls;
        
        if (!IsOverride)
            FClasses[cls.File.ClassName] = cls;

        base.LeaveClass(cls);
    }

    public override IEnumerable<Property>? VisitMethod(Property methodProperty, ExtClassMemberOwner owner)
    {
        return null;
    }

    public override IEnumerable<Property>? VisitProperty(Property property, ExtClassMemberOwner owner)
    {
        if (property.Key is not Identifier id)
            return null;

        if (CurrentClass is null)
            throw new Exception("No class");
        
        switch (id.Name)
        {
            case "alternateClassName":
            {
                var names = property.Value switch
                {
                    ArrayExpression array => array.Elements.Select(el => (el as Literal)?.StringValue ?? throw new Exception("Not a string literal")),
                    Literal { StringValue: {} value } => new List<string> { value },
                    _ => throw new Exception("Invalid alternativeClassName")
                };
                    
                foreach (var name in names)
                    FClasses[name] = CurrentClass;
                    
                break;
            }
            case "requires":
            {
                var requires = property.Value switch
                {
                    ArrayExpression array => array.Elements.Select(el => (el as Literal)?.StringValue ?? throw new Exception("Not a string literal")).ToList(),
                    Literal { StringValue: {} value } => new List<string> { value },
                    _ => throw new Exception("Invalid requires")
                };

                FRequires[CurrentClass.ClassName ?? CurrentClass.File.ClassName] = requires;
                break;
            }
            case "mixins":
            {
                var mixins = property.Value switch
                {
                    ArrayExpression array => array.Elements.Select(el => (el as Literal)?.StringValue ?? throw new Exception("Not a string literal")).ToList(),
                    ObjectExpression obj => obj.Properties.Select(el => ((el as Property)?.Value as Literal)?.StringValue ?? throw new Exception("Not a string literal")).ToList(),
                    Literal { StringValue: {} value } => new List<string> { value },
                    _ => throw new Exception($"Invalid mixins for class {CurrentClass.File.Path.FullName}")
                };

                FMixins[CurrentClass.ClassName ?? CurrentClass.File.ClassName] = mixins;
                break;
            }
            case "uses":
            {
                var uses = property.Value switch
                {
                    ArrayExpression array => array.Elements.Select(el => (el as Literal)?.StringValue ?? throw new Exception("Not a string literal")).ToList(),
                    Literal { StringValue: {} value } => new List<string> { value },
                    _ => throw new Exception("Invalid uses")
                };

                FUses[CurrentClass.ClassName ?? CurrentClass.File.ClassName] = uses;
                break;
            }
            case "override":
            {
                if (property.Value is not Literal { StringValue: { } value })
                    throw new Exception("override is not a string literal");

                IsOverride = true;
                
                if ((CurrentClass.ClassName ?? CurrentClass.File.ClassName) != value)
                    FOverrides[CurrentClass.ClassName ?? CurrentClass.File.ClassName] = value;

                if (!FClassOverrides.TryGetValue(value, out var classOverrides))
                {
                    classOverrides = new List<IClassDeclaration>();
                    FClassOverrides.Add(value, classOverrides);
                }

                classOverrides.Add(CurrentClass);
                
                break;
            }
            case "extend":
            {
                if (property.Value is not Literal { StringValue: { } value })
                    throw new Exception("extend is not a string literal");

                FExtends[CurrentClass.ClassName ?? CurrentClass.File.ClassName] = value;
                
                break;
            }
            case "singleton":
            {
                if (property.Value is Literal { BooleanValue: true })
                {
                    CurrentClass.IsSingleton = true;
                    FSingletons.Add((CurrentClass as IClassDeclaration).EffectiveClassName);
                }
                break;
            }
        }

        return null;
    }

    public void VisitAssignmentDeclaration(ExtClassAssignmentDeclaration declaration)
    {
        FClasses[declaration.ClassName] = declaration;
        if (!declaration.File.IsOverride)
            FClasses[declaration.File.ClassName] = declaration;
        
        VisitComments(declaration);
    }

    public void VisitApplyDeclaration(ExtClassApplyDeclaration declaration)
    {
        FOverrides[declaration.File.ClassName] = declaration.Target;
        VisitComments(declaration);
    }

    private void VisitComments(IClassDeclaration declaration)
    {
        var alternateClassNames = new List<string>();
        var requires = new List<string>();
        var uses = new List<string>();

        foreach (var comment in declaration.Comments)
        {
            var defineMatches = DefineCommentRegex.Matches(comment.Value);
            alternateClassNames.AddRange(defineMatches.Select(m => m.Groups[1].Value).Where(c => c != declaration.ClassName));

            var requireMatches = RequireCommentRegex.Matches(comment.Value);
            requires.AddRange(requireMatches.Select(m => m.Groups[1].Value));

            var usesMatches = UsesCommentRegex.Matches(comment.Value);
            uses.AddRange(usesMatches.Select(m => m.Groups[1].Value));
        }

        var className = declaration.ClassName ?? declaration.File.ClassName;
        
        FRequires[className] = requires;
        FUses[className] = uses;
        
        foreach (var alternateClassName in alternateClassNames)
            FClasses[alternateClassName] = declaration;
    }

    public void VisitApplicationDeclaration(ExtApplicationDeclaration declaration)
    {
        ApplicationDeclaration = declaration;
    }

    private IEnumerable<string> ResolveClassPattern(string classPattern)
    {
        if (classPattern.EndsWith('*'))
        {
            var prefix = classPattern.Replace("*", "");
            return Classes.Keys.Where(x => x.StartsWith(prefix));
        }
        else
        {
            return new[] { classPattern };
        }
    }

    public IList<string>? GetEffectiveDependencies(string className)
    {
        if (EffectiveDependencies.TryGetValue(className, out var cache))
            return cache;
        
        if (!Classes.TryGetValue(className, out var cls))
        {
            EffectiveDependencies.Add(className, null);
            return null;
        }

        var effectiveClassName = cls.EffectiveClassName;
        var deps = new List<string>();

        if (className != "Ext")
            deps.Add("Ext");

        if (cls is ExtClassDefineDeclaration)
        {
            deps.Add("Ext.Base");
            deps.Add("Ext.Class");
            if (cls.ClassName != "Ext.mixin.Watchable")
                deps.Add("Ext.Loader");
        }
        
        if (ApplicationDeclaration?.ApplicationClassName == className)
            deps.Add("Ext.application");
        
        if (className == "Ext.util.Operators")
            deps.Add("Ext.ClassManager");
        
        if (className == "Ext.Configurator")
            deps.Add("Ext.Object");
        
        if (className == "Ext.mixin.ComponentDelegation")
            deps.Add("Ext.mixin.Observable");
        
        if (className == "Ext.dom.Query")
            deps.Add("Ext.util.LruCache");
        
        if (Requires.TryGetValue(effectiveClassName, out var reqs))
            deps.AddRange(reqs.SelectMany(ResolveClassPattern).Where(x => x != effectiveClassName));

        if (FMixins.TryGetValue(effectiveClassName, out var mixins))
        {
            var mixinsWithOverrides = mixins.SelectMany(c => (GetOverrides(c) ?? new List<IClassDeclaration>())
                .Select(x => x.ClassName ?? x.File.ClassName)
                .Where(x => x != className)
                .Append(c)
                .Reverse());
            deps.AddRange(mixinsWithOverrides.SelectMany(ResolveClassPattern).Where(x => x != effectiveClassName));
        }

        if (Extends.TryGetValue(effectiveClassName, out var extend))
            deps.Add(extend);

        if (Overrides.TryGetValue(effectiveClassName, out var over))
            deps.Add(over);

        EffectiveDependencies.Add(className, deps);

        return deps;
    }

    public IList<string>? GetEffectiveLateDependencies(string className)
    {
        if (!Classes.TryGetValue(className, out var cls))
            return null;

        var deps = new List<string>();
        
        if (FUses.TryGetValue(cls.EffectiveClassName, out var uses))
            deps.AddRange(uses.SelectMany(ResolveClassPattern));

        return deps;
    }

    public IList<IClassDeclaration>? GetOverrides(string className)
    {
        if (!Classes.TryGetValue(className, out var cls))
            return null;
        
        return FClassOverrides.TryGetValue(cls.EffectiveClassName, out var result) 
            ? result 
            : Array.Empty<IClassDeclaration>();
    }

    public IClassDeclaration? GetParentClass(string className)
    {
        return !Classes.TryGetValue(className, out var cls) ? null : GetParentClass(cls);
    }
    
    public IClassDeclaration? GetParentClass(IClassDeclaration cls)
    {
        return FExtends.TryGetValue(cls.EffectiveClassName, out var result)
            ? GetClassDeclaration(result)
            : null;
    }

    public IClassDeclaration? GetRuntimeClass(string className)
    {
        return !Classes.TryGetValue(className, out var cls) ? null : GetRuntimeClass(cls);
    }
    
    public IClassDeclaration? GetRuntimeClass(IClassDeclaration cls)
    {
        return FOverrides.TryGetValue(cls.EffectiveClassName, out var origName) ? GetClassDeclaration(origName) : cls;
    }

    public bool IsOverrideClass(string className)
    {
        return Classes.TryGetValue(className, out var cls) && IsOverrideClass(cls);
    }
    
    public bool IsOverrideClass(IClassDeclaration cls)
    {
        return FOverrides.ContainsKey(cls.EffectiveClassName);
    }
    
    public bool IsSingletonClass(string className)
    {
        return Classes.TryGetValue(className, out var cls) && IsSingletonClass(cls);
    }
    
    public bool IsSingletonClass(IClassDeclaration cls)
    {
        return FSingletons.Contains(cls.EffectiveClassName);
    }

    public IClassDeclaration? GetClassDeclaration(string className)
    {
        return !Classes.TryGetValue(className, out var cls) ? null : cls;
    }
}