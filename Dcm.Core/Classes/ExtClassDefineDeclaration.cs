﻿using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public class ExtClassDefineDeclaration : IClassDeclaration
{
    public string? ClassName { get; }
    public ClassFile File { get; }
    public Program Program { get; private set; }
    public CallExpression DefineCallExpression { get; private set; }
    public Node ClassDefinition => DefineCallExpression;
    public bool IsSingleton { get; internal set; }

    public List<ExtClassProperty> Properties { get; } = new();
    public List<ExtClassMethod> Methods { get; } = new();
    public List<ExtClassConfig> Configs { get; } = new();

    private static int AnonymousClassCounter;
    
    public ExtClassDefineDeclaration(string? className, ClassFile file, CallExpression defineCallExpression, Program program)
    {
        ClassName = className ?? $"$anonymous{Interlocked.Increment(ref AnonymousClassCounter)}";
        File = file;
        DefineCallExpression = defineCallExpression;
        Program = program;
    }

    public void Accept(DefineVisitor visitor)
    {
        visitor.EnterClass(this);
        var configExpr = FindConfigExpression();

        var newProps = configExpr.Properties.ReplaceNodes(expr => expr switch
        {
            Property { Key: Identifier { Name: "config" }, Value: ObjectExpression oe } prop =>
                prop.UpdateWith(prop.Key, AcceptConfig(visitor, oe)),
            
            Property { Key: Identifier { Name: "statics" }, Value: ObjectExpression oe } prop =>
                prop.UpdateWith(prop.Key, AcceptStatics(visitor, oe, false)),

            Property { Key: Identifier { Name: "inheritableStatics" }, Value: ObjectExpression oe } prop =>
                prop.UpdateWith(prop.Key, AcceptStatics(visitor, oe, true)),
            
            Property { Key: Identifier { Name: "privates" }, Value: ObjectExpression oe } prop =>
                prop.UpdateWith(prop.Key, AcceptPrivates(visitor, oe)),

            Property { Value: FunctionExpression } prop =>
                visitor.VisitMethod(prop, IsSingleton ? ExtClassMemberOwner.Static : ExtClassMemberOwner.Instance),
            
            Property prop =>
                visitor.VisitProperty(prop, IsSingleton ? ExtClassMemberOwner.Static : ExtClassMemberOwner.Instance),
            
            _ => null
        });

        var replacement = configExpr.UpdateWith(newProps);

        visitor.LeaveClass(this);

        var prevDefineCall = DefineCallExpression;
        DefineCallExpression = new NodeReplacer<ObjectExpression>()
        {
            What = configExpr,
            With = replacement
        }.VisitAndConvert(DefineCallExpression);

        Program = new NodeReplacer<CallExpression>
        {
            What = prevDefineCall,
            With = DefineCallExpression
        }.VisitAndConvert(Program);
    }

    private Node AcceptStatics(DefineVisitor visitor, ObjectExpression statics, bool inheritable)
    {
        var owner = inheritable ? ExtClassMemberOwner.InheritableStatic : ExtClassMemberOwner.Static;
        
        var newProps = statics.Properties.ReplaceNodes(expr => expr switch
        {
            Property { Value: FunctionExpression } prop => visitor.VisitMethod(prop, owner),
            Property prop => visitor.VisitProperty(prop, owner),
            _ => null
        });

        return statics.UpdateWith(newProps);
    }
    
    private Node AcceptPrivates(DefineVisitor visitor, ObjectExpression statics)
    {
        var newProps = statics.Properties.ReplaceNodes(expr => expr switch
        {
            Property { Value: FunctionExpression } prop => visitor.VisitMethod(prop, ExtClassMemberOwner.Instance),
            Property prop => visitor.VisitProperty(prop, ExtClassMemberOwner.Instance),
            _ => null
        });

        return statics.UpdateWith(newProps);
    }
    
    private Node AcceptConfig(DefineVisitor visitor, ObjectExpression config)
    {
        var newProps = config.Properties.ReplaceNodes(expr => expr switch
        {
            Property prop => visitor.VisitConfig(prop),
            _ => null
        });

        return config.UpdateWith(newProps);
    }

    public void AcceptClassDeclarationVisitor(IClassDeclarationVisitor visitor)
    {
        var prevDefineCall = DefineCallExpression;
        var result = visitor.VisitClassDeclaration(this, prevDefineCall);
        if (prevDefineCall == result)
            return;
        switch (result)
        {
            case CallExpression callExpression:
                DefineCallExpression = callExpression;
                Program = new NodeReplacer<CallExpression>
                {
                    What = prevDefineCall,
                    With = callExpression
                }.VisitAndConvert(Program);
                break;
            default:
                throw new Exception($"Unexpected node {result.Type}");
        }
    }

    private ObjectExpression FindConfigExpression()
    {
        var configArg = DefineCallExpression.Arguments[1];
        return configArg switch
        {
            ObjectExpression o => o,
            CallExpression ce => ce.Callee switch
            {
                FunctionExpression fe => fe.Body.Body
                    .Select(s => s as ReturnStatement)
                    .FirstOrDefault(s => s is not null)?.Argument as ObjectExpression ?? throw new Exception("Returned value is not an object expression"),
                _ => throw new Exception("Config is not an IIFE")
            },
            FunctionExpression fe => fe.Body.Body
                .Select(s => s as ReturnStatement)
                .FirstOrDefault(s => s is not null)?.Argument as ObjectExpression ?? throw new Exception("Returned value is not an object expression"),
            _ => throw new Exception("Could not find the config expression")
        };
    }

    public override string ToString()
    {
        return $"<DefineDeclaration {nameof(ClassName)}: {ClassName}>";
    }
}