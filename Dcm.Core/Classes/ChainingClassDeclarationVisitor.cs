﻿using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public class ChainingClassDeclarationVisitor : IClassDeclarationVisitor
{
    public required IReadOnlyList<IClassDeclarationVisitor> Visitors { private get; init; }

    public Node VisitClassDeclaration(IClassDeclaration classDeclaration, Node rootNode)
    {
        return Visitors.Aggregate(rootNode, (current, visitor) => visitor.VisitClassDeclaration(classDeclaration, current));
    }
}