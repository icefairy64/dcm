using System.Text.RegularExpressions;
using Esprima;
using Esprima.Ast;
using Esprima.Utils;

namespace Breezy.Dcm.Core.Classes;

public static class ExtClassDeclarationDetector
{
    private static readonly Regex ExtAssignmentRegex = new("^\\(?Ext(?:\\..+?)?$");

    public static IClassDeclaration? ReadDeclaration(ClassFile cls, Program root, JavaScriptParser parser)
    {
        // Ext.define(...)

        var defineCallExpr = root.DiveThroughDeclarations(node => node switch
        {
            ExpressionStatement
            {
                Expression: CallExpression
                {
                    Callee: MemberExpression me
                } ce
            } when me.ToJavaScriptString() == "Ext.define" => ce,
            _ => null
        });
        
        if (defineCallExpr is not null)
        {
            var classNameArg = defineCallExpr.Arguments[0];
            return classNameArg switch
            {
                Literal { StringValue: not null } l => new ExtClassDefineDeclaration(l.StringValue, cls, defineCallExpr, root),
                Literal { TokenType: TokenType.NullLiteral } => new ExtClassDefineDeclaration(null, cls, defineCallExpr, root),
                _ => throw new Exception("Unexpected first argument to Ext.define call")
            };
        }
        
        // Ext.apply(...)

        var applyCallExpression = root.Body.Select(node => node switch
            {
                ExpressionStatement
                {
                    Expression: CallExpression
                    {
                        Callee: MemberExpression me
                    } ce
                } when me.ToJavaScriptString() == "Ext.apply" => ce,
                _ => null
            })
            .FirstOrDefault();

        if (applyCallExpression is not null)
        {
            var targetArg = applyCallExpression.Arguments[0];
            var target = targetArg switch
            {
                Identifier id => id.Name,
                MemberExpression me => me.ToJavaScriptString(),
                _ => throw new Exception("Invalid first argument to Ext.apply")
            };
            return new ExtClassApplyDeclaration(null, cls, target, applyCallExpression, root);
        }
        
        // Ext.application(...)
        
        var appCallExpr = root.Body
            .Select(s => (s as ExpressionStatement)?.Expression as CallExpression)
            .FirstOrDefault(ce => (ce?.Callee as MemberExpression)?.ToJavaScriptString() == "Ext.application");

        if (appCallExpr is not null)
        {
            var arg = appCallExpr.Arguments[0];
            return arg switch
            {
                Literal { StringValue: { } clsName } => new ExtApplicationDeclaration(null, cls, appCallExpr, clsName, root),
                ObjectExpression obj => new ExtApplicationDeclaration(null, cls, appCallExpr, 
                    (obj.Properties.Select(p => p as Property)
                        .FirstOrDefault(p => p?.Key.ToJavaScriptString() == "extend")?.Value as Literal)?.StringValue ?? throw new Exception("Failed to determine app class name"),
                    root),
                _ => throw new Exception("Unexpected first argument to Ext.application call")
            };
        }
        
        // var Ext = ...
        // Ext.util = ...
        
        var assignmentExpr = root.DiveThroughFunctions<object>(s => s switch
            {
                ExpressionStatement { Expression: AssignmentExpression assignmentExpression } when ExtAssignmentRegex.Match(assignmentExpression
                    .Left.ToJavaScriptString()).Success => assignmentExpression,
                VariableDeclaration decl => decl.Declarations.Select(d => ExtAssignmentRegex.Match(d.Id.ToJavaScriptString()).Success ? d : null).FirstOrDefault(),
                _ => null
            });

        if (assignmentExpr is not null)
        {
            return assignmentExpr switch
            {
                AssignmentExpression ae => new ExtClassAssignmentDeclaration(ae.Left.ToJavaScriptString(),
                    cls, ae, root),
                VariableDeclarator d => new ExtClassAssignmentDeclaration(d.Id.ToJavaScriptString(), cls, d, root),
                _ => throw new Exception()
            };
        }

        return null;
    }
}