﻿using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public class ExtApplicationDeclaration : IClassDeclaration
{
    public string? ClassName { get; }
    public ClassFile File { get; }
    public Program Program { get; private set; }
    public CallExpression ApplicationCallExpression { get; private set; }
    public Node ClassDefinition => ApplicationCallExpression;
    public string ApplicationClassName { get; }
    
    public List<ExtClassProperty> Properties { get; } = new();
    public List<ExtClassMethod> Methods { get; } = new();
    public List<ExtClassConfig> Configs { get; } = new();
    
    public ExtApplicationDeclaration(string? className, ClassFile file, CallExpression applicationCallExpression, string applicationClassName, Program program)
    {
        ClassName = className;
        File = file;
        ApplicationCallExpression = applicationCallExpression;
        ApplicationClassName = applicationClassName;
        Program = program;
    }

    public void AcceptClassDeclarationVisitor(IClassDeclarationVisitor visitor)
    {
        var prevCall = ApplicationCallExpression;
        var result = visitor.VisitClassDeclaration(this, prevCall);
        if (prevCall == result)
            return;
        switch (result)
        {
            case CallExpression callExpression:
                ApplicationCallExpression = callExpression;
                Program = new NodeReplacer<CallExpression>
                {
                    What = prevCall,
                    With = callExpression
                }.VisitAndConvert(Program);
                break;
            default:
                throw new Exception($"Unexpected node {result.Type}");
        }
    }
}