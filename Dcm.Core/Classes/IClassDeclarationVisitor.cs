﻿using Esprima.Ast;

namespace Breezy.Dcm.Core.Classes;

public interface IClassDeclarationVisitor
{
    public Node VisitClassDeclaration(IClassDeclaration classDeclaration, Node rootNode);
}