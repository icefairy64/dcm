﻿using Breezy.Dcm.Core.JsDoc;
using Esprima.Ast;
using Esprima.Utils;

namespace Breezy.Dcm.Core.Classes;

public class ExtClassStructureVisitor : IClassDeclarationVisitor
{
    public Node VisitClassDeclaration(IClassDeclaration classDeclaration, Node rootNode)
    {
        if (classDeclaration is ExtClassDefineDeclaration)
            return rootNode;

        var astVisitor = new ExtClassStructureAstVisitor(classDeclaration);
        astVisitor.Visit(rootNode);
        
        return rootNode;
    }
}

internal class ExtClassStructureAstVisitor : AstVisitor
{
    private readonly IClassDeclaration Class;

    public ExtClassStructureAstVisitor(IClassDeclaration @class)
    {
        Class = @class;
    }

    protected override object? VisitProperty(Property property)
    {
        if (JsDocParser.FindJsDoc(Class, property) is { } jsDoc)
        {
            var tags = JsDocParser.ExtractJsDocTags(jsDoc).ToList();
            var @static = tags.OfType<JsDocStatic>().FirstOrDefault() != null;
            var level = @static ? ExtClassMemberOwner.Static : ExtClassMemberOwner.Instance;
            var propName = property.Key.ToJavaScriptString();
            
            if (tags.OfType<JsDocProperty>().FirstOrDefault() is { } propTag)
            {
                Class.Properties.Add(new ExtClassProperty
                {
                    Name = propName,
                    JsDoc = jsDoc,
                    OwnerLevel = level,
                    Type = propTag.Type
                });
            }
            else if (property.Value is FunctionExpression { } func)
            {
                var returnTag = tags.OfType<JsDocReturn>().FirstOrDefault();
                
                var parameters = func.Params.Select(param =>
                {
                    var name = param switch
                    {
                        Identifier id => id.Name,
                        RestElement re => (re.Argument as Identifier)?.Name ?? throw new Exception(),
                        _ => throw new Exception()
                    };

                    var paramTag = tags.OfType<JsDocParam>().FirstOrDefault(x => x.Name == name);
                    
                    return new ExtClassMethodParameter
                    {
                        Name = name,
                        Type = paramTag?.Type,
                        Optional = paramTag?.Optional ?? false,
                        DefaultValue = paramTag?.DefaultValue
                    };
                });

                Class.Methods.Add(new ExtClassMethod
                {
                    Name = propName,
                    JsDoc = jsDoc,
                    OwnerLevel = level,
                    ReturnType = returnTag?.Type,
                    Parameters = parameters.ToList()
                });
            }
        }
        
        return base.VisitProperty(property);
    }
}