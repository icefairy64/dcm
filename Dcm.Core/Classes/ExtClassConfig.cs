﻿namespace Breezy.Dcm.Core.Classes;

public class ExtClassConfig
{
    public required string Name { get; init; }
    public string? Type { get; init; } = null;

    public override string ToString()
    {
        return $"{nameof(Name)}: {Name}, {nameof(Type)}: {Type}";
    }
}