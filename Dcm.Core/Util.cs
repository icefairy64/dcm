namespace Breezy.Dcm.Core;

public static class Util
{
    public static string JoinString<T>(this IEnumerable<T> enumerable, Func<T, string> stringFn)
    {
        return enumerable.Select(stringFn).Aggregate((a, x) => string.Join(", ", a, x));
    }
    
    public static string JoinString(this IEnumerable<string> enumerable)
    {
        return !enumerable.Any() ? "" : enumerable.Aggregate((a, x) => string.Join(", ", a, x));
    }
    
    public static string JoinString(this IEnumerable<string> enumerable, char separator)
    {
        return !enumerable.Any() ? "" : enumerable.Aggregate((a, x) => string.Join(separator, a, x));
    }
}