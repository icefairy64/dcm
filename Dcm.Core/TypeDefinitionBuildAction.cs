﻿using Breezy.Dcm.Core.Classes;
using Breezy.Dcm.Core.TypeScript;
using Microsoft.Extensions.Logging;

namespace Breezy.Dcm.Core;

public class TypeDefinitionBuildAction : AbstractJsBuildAction<BuildConfiguration>
{
    public TypeDefinitionBuildAction(BuildConfiguration configuration, ILoggerFactory loggerFactory)
        : base(configuration, loggerFactory.CreateLogger<TypeDefinitionBuildAction>(), loggerFactory)
    {
    }

    protected override IReadOnlyList<DefineVisitor> ExamineDefineVisitors
    {
        get
        {
            var visitors = new List<DefineVisitor>(base.ExamineDefineVisitors)
            {
                new ExtClassDefineStructureVisitor()
            };
            return visitors;
        }
    }

    protected override IReadOnlyList<IClassDeclarationVisitor> ExamineVisitors =>
        new List<IClassDeclarationVisitor>
        {
            new ExtClassStructureVisitor()
        };

    public async Task Build(TextWriter writer)
    {
        await ExamineClasses();
        var batches = BuildDependencyOrder(ClassExamineVisitor.Classes.Keys.ToList());
        var emitter = new TypeScriptDefinitionEmitter(writer);
        var tsDefinitionGenerator = new TypeScriptDefinitionGenerator(ClassExamineVisitor);
        var definedClasses = new HashSet<string>();
        var definitions = new List<(string, TypeScriptNode)>();
        foreach (var batch in batches)
        {
            foreach (var clsName in batch)
            {
                if (ClassExamineVisitor.GetClassDeclaration(clsName) is not {} cls)
                    continue;
                if (!definedClasses.Add(cls.EffectiveClassName))
                    continue;
                Logger.LogDebug("Defining {}", clsName);
                definitions.AddRange(tsDefinitionGenerator
                    .GenerateDefinition(cls)
                    .Select(node => (clsName, node)));
            }
        }

        var rootNamespaces = definitions.OrderBy(x => x.Item1)
            .GroupBy(x => (x.Item2 as TypeScriptNamespace)?.Name ?? x.Item1, x => x.Item2)
            .Select(g => MergeNamespaces(g.OfType<TypeScriptNamespace>()))
            .ToList();
        
        foreach (var node in rootNamespaces)
        {
            if (node is null)
                return;
            await emitter.EmitAsync(node);
            await writer.WriteLineAsync();
            await writer.WriteLineAsync();
        }
    }

    private static TypeScriptNamespace? MergeNamespaces(IEnumerable<TypeScriptNamespace> namespaces)
    {
        if (!namespaces.Any())
            return null;
        
        return new TypeScriptNamespace
        {
            Name = namespaces.First().Name,
            Body = namespaces.SelectMany(n => n.Body).ToList()
        };
    }
}