﻿using Breezy.Dcm.Core.Classes;
using Esprima.Ast;
using Esprima.Utils;

namespace Breezy.Dcm.Core.Strict;

/// <summary>
/// Rewrites method 'name' property writes to no-op.
/// </summary>
public class ExtBaseMethodNameVisitor : AstRewriter, IClassDeclarationVisitor
{
    private IClassDeclaration? CurrentClass;
    
    protected override object? VisitExpressionStatement(ExpressionStatement expressionStatement)
    {
        if (expressionStatement.Expression is AssignmentExpression
            {
                Left: MemberExpression
                {
                    Object: Identifier { Name: "member" or "constructor" or "ctor" or "value" or "instantiator" }, Property: Identifier { Name: "name" }
                }
            })
            return expressionStatement.UpdateWith(new Literal(0d, "0"));
        
        return base.VisitExpressionStatement(expressionStatement);
    }

    protected override object? VisitAssignmentExpression(AssignmentExpression assignmentExpression)
    {
        if (CurrentClass?.EffectiveClassName == "Ext.dom.Fly" &&
            assignmentExpression.Right is LogicalExpression { Left: LogicalExpression { Left: Identifier id, Right: LogicalExpression toReplace }, Right: Literal namedLiteral } le &&
            assignmentExpression.ToJavaScriptString().Contains("fn.caller"))
        {
            return base.VisitAssignmentExpression(assignmentExpression.UpdateWith(assignmentExpression.Left, le.UpdateWith(id, namedLiteral)));
        }
        
        return base.VisitAssignmentExpression(assignmentExpression);
    }

    public Node VisitClassDeclaration(IClassDeclaration classDeclaration, Node rootNode)
    {
        if (classDeclaration.ClassName != "Ext.Base" &&
            classDeclaration.ClassName != "Ext.Class" &&
            classDeclaration.ClassName != "Ext.ClassManager" &&
            classDeclaration.ClassName != "Ext" &&
            classDeclaration.ClassName != "Ext.dom.Fly")
            return rootNode;

        CurrentClass = classDeclaration;
        return Visit(rootNode) as Node ?? rootNode;
    }
}