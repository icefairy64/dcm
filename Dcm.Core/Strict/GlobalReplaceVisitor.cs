﻿using Esprima.Ast;
using Esprima.Utils;

namespace Breezy.Dcm.Core.Strict;

/// <summary>
/// Rewrites global object references from 'global' to 'window' to support strict mode.
/// </summary>
public class GlobalReplaceVisitor : AstRewriter
{
    private Identifier? TargetObject;

    protected override object? VisitVariableDeclarator(VariableDeclarator variableDeclarator)
    {
        if (variableDeclarator is { Id: Identifier { Name: "global" } identifier, Init: ThisExpression })
            return variableDeclarator.UpdateWith(identifier, new Identifier("window"));
        return base.VisitVariableDeclarator(variableDeclarator);
    }

    protected override object? VisitMemberExpression(MemberExpression memberExpression)
    {
        if (memberExpression.Object is Identifier identifier)
            TargetObject = identifier;
        var result = base.VisitMemberExpression(memberExpression);
        if (memberExpression.Object is Identifier)
            TargetObject = null;
        return result;
    }

    protected override object? VisitIdentifier(Identifier identifier)
    {
        if (identifier == TargetObject && identifier.Name == "global")
            return new Identifier("window");
        return base.VisitIdentifier(identifier);
    }
}