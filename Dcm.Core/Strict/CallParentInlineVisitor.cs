﻿using Breezy.Dcm.Core.Classes;
using Esprima;
using Esprima.Ast;
using Esprima.Utils;

namespace Breezy.Dcm.Core.Strict;

public class CallParentInlineVisitor : AstRewriter, IClassDeclarationVisitor
{
    private readonly ClassExamineVisitor ClassExamineVisitor;
    private readonly JavaScriptParser Parser;
    
    private ExtClassDefineDeclaration? CurrentClass;
    private string? CurrentFunction;

    public CallParentInlineVisitor(ClassExamineVisitor classExamineVisitor)
    {
        ClassExamineVisitor = classExamineVisitor;
        Parser = new JavaScriptParser();
    }

    protected override object? VisitProperty(Property property)
    {
        if (property is not { Key: Identifier { Name: var propName }, Value: FunctionExpression })
            return base.VisitProperty(property);
        
        CurrentFunction = propName;
        var result = base.VisitProperty(property);
        CurrentFunction = null;
        return result;
    }

    protected override object? VisitCallExpression(CallExpression callExpression)
    {
        if (callExpression is
            { Callee: MemberExpression { Object: ThisExpression or Identifier { Name: "me" }, Property: Identifier { Name: "callParent" } } })
            return GenerateCallParentReplacement(callExpression);
        return base.VisitCallExpression(callExpression);
    }

    private CallExpression GenerateCallParentReplacement(CallExpression callExpression)
    {
        if (CurrentFunction is not {} functionName)
            throw new Exception("Not in the function scope");
        
        var runtimeClass =
            ClassExamineVisitor.GetRuntimeClass(CurrentClass?.ClassName ?? throw new Exception())?.ClassName;
        if (runtimeClass is null)
            throw new Exception("Runtime class is unknown");
        
        var parentClass =
            ClassExamineVisitor.GetParentClass(runtimeClass)?.ClassName ?? "Ext.Base";
        
        return GenerateCallParentReplacement(runtimeClass, parentClass, functionName, callExpression.Arguments.Select(x => x.ToJavaScriptString()).JoinString());
    }

    private CallExpression GenerateCallParentReplacement(string className, string parentClassName, string methodName, string args)
    {
        if (methodName == "constructor")
            return GenerateCallParentConstructorReplacement(className, parentClassName, args);

        if (className == "Ext.Mixin")
            return GenerateCallParentMixinReplacement(args);
        
        var js = $"$${methodName}.apply(this, {args})";

        return Parser.ParseExpression(js) as CallExpression ?? throw new Exception();
    }
    
    private CallExpression GenerateCallParentConstructorReplacement(string className, string parentClassName, string args)
    {
        var js = $$"""
(function () {
if (!window['{{className}}[]ctorChain']) {
    window['{{className}}[]ctorChain'] = [];
}
const __$ = window['{{className}}[]ctorChain'];
let __method = this.constructor;
do {
    const curMethod = __method;
    __method = __method.$previous;
    if (!__method) {
        __method = curMethod.$owner.superclass.constructor;
    }
} while (__$.includes(__method));
__$.push(__method);
const result = __method.apply(this, arguments);
__$.pop();
return result;
}).apply(this, {{args}})
""";

        return Parser.ParseExpression(js) as CallExpression ?? throw new Exception();
    }

    private CallExpression GenerateCallParentMixinReplacement(string args)
    {
        if (CurrentFunction == "addHook")
        {
            var js = $$"""
(function () {
if (!window[`${targetClass.$className}${methodName}Chain`]) {
    window[`${targetClass.$className}${methodName}Chain`] = [];
}
const __$ = window[`${targetClass.$className}${methodName}Chain`];
let __method = targetClass.prototype[methodName];
do {
    __method = __method.$previous;
    if (!__method) {
        __method = targetClass.superclass[methodName]
    }
} while (__$.includes(__method));
__$.push(__method);
const result = __method.apply(this, arguments);
__$.pop();
return result;
}).apply(this, {{args}})
""";
            return Parser.ParseExpression(js) as CallExpression ?? throw new Exception();
        }
        
        var js2 = $$"""
(function () {
if (!window[`${targetClass.$className}${key}Chain`]) {
    window[`${targetClass.$className}${key}Chain`] = [];
}
const __$ = window[`${targetClass.$className}${key}Chain`];
let __method = targetProto[key];
do {
    __method = __method.$previous;
    if (!__method) {
        __method = targetClass.superclass[key]
    }
} while (__$.includes(__method));
__$.push(__method);
const result = __method.apply(this, arguments);
__$.pop();
return result;
}).apply(this, {{args}})
""";
        return Parser.ParseExpression(js2) as CallExpression ?? throw new Exception();
    }

    public Node VisitClassDeclaration(IClassDeclaration classDeclaration, Node rootNode)
    {
        CurrentClass = classDeclaration as ExtClassDefineDeclaration;
        if (CurrentClass is null)
            return rootNode;
        if (CurrentClass.ClassName != "Ext.Mixin")
            rootNode = new CallParentFreezeVisitor(CurrentClass, ClassExamineVisitor).Visit(rootNode) as Node ?? throw new Exception("Not a node");
        var result = Visit(rootNode) as Node ?? throw new Exception("Not a node");
        CurrentClass = null;
        return result;
    }
}

internal class CallParentFreezeVisitor : AstRewriter
{
    private readonly ExtClassDefineDeclaration CurrentClass;
    private readonly ClassExamineVisitor ClassExamineVisitor;
    private readonly JavaScriptParser Parser = new JavaScriptParser();
    private bool Done;

    public CallParentFreezeVisitor(ExtClassDefineDeclaration currentClass, ClassExamineVisitor classExamineVisitor)
    {
        CurrentClass = currentClass;
        ClassExamineVisitor = classExamineVisitor;
    }

    protected override object? VisitCallExpression(CallExpression callExpression)
    {
        if (!Done && callExpression is { Callee: MemberExpression { Object: Identifier { Name: "Ext" }, Property: Identifier { Name: "define" } } })
        {
            var classConfigExpression = callExpression.Arguments[1];
            var replacementArgument = classConfigExpression switch
            {
                ObjectExpression oe => ReplaceConfigObject(oe),
                FunctionExpression fe => ReplaceConfigFunction(fe),
                CallExpression ce => ReplaceConfigIife(ce),
                _ => throw new Exception($"Unexpected node type {classConfigExpression.Type}")
            };
            var args = callExpression.Arguments.Select(a => a == classConfigExpression ? replacementArgument : a);
            Done = true;
            return base.VisitCallExpression(callExpression.UpdateWith(callExpression.Callee, NodeList.Create(args)));
        }
        return base.VisitCallExpression(callExpression);
    }

    private Expression ReplaceConfigObject(ObjectExpression objectExpression, bool iife = true, IEnumerable<Node>? args = null, IEnumerable<Statement>? additionalStatements = null)
    {
        var freezeStatements = CreateFreezeStatements(objectExpression);
        var funcBody = new List<Statement>(freezeStatements.Concat(additionalStatements ?? Array.Empty<Statement>()))
        {
            new ReturnStatement(objectExpression)
        };
        var block = new BlockStatement(NodeList.Create(funcBody));
        var funcExpr = new FunctionExpression(null, NodeList.Create(args ?? Array.Empty<Node>()), block, false, false, false);
        if (!iife)
            return funcExpr;
        return new CallExpression(funcExpr, NodeList.Create(Array.Empty<Expression>()), false);
    }
    
    private Expression ReplaceConfigFunction(FunctionExpression functionExpression, bool iife = false)
    {
        var configReturn = functionExpression.Body.Body.OfType<ReturnStatement>().First();
        if (configReturn.Argument is not ObjectExpression oe)
            throw new Exception("Does not return an object");
        return ReplaceConfigObject(oe, iife, functionExpression.Params, functionExpression.Body.Body.Where(s => s is not ReturnStatement));
    }
    
    private Expression ReplaceConfigIife(CallExpression callExpression)
    {
        return ReplaceConfigFunction(callExpression.Callee as FunctionExpression ?? throw new Exception("Not a function expression"), true);
    }

    private IEnumerable<VariableDeclaration> CreateFreezeStatements(ObjectExpression configExpression)
    {
        var isOverride = ClassExamineVisitor.IsOverrideClass(CurrentClass);
        var targetClass = isOverride
            ? ClassExamineVisitor.GetRuntimeClass(CurrentClass)?.EffectiveClassName ?? throw new Exception($"No runtime class for {CurrentClass.ClassName}")
            : ClassExamineVisitor.GetParentClass(CurrentClass)?.EffectiveClassName ?? "Ext.Base";
        var isSingleton = ClassExamineVisitor.IsSingletonClass(targetClass);
        return configExpression.Properties.OfType<Property>()
            .SelectMany(p => p is { Key: Identifier { Name: "privates" }, Value: ObjectExpression oe } ? oe.Properties.OfType<Property>() : new Property[] { p })
            .Where(p => p.Value is FunctionExpression or ArrowFunctionExpression)
            .Select(p =>
            {

                var methodName = p.Key switch
                {
                    Identifier { Name: var idName } => idName,
                    Literal { StringValue: var stringLiteral } => stringLiteral,
                    _ => throw new Exception($"Unexpected property key type {p.Key.Type}")
                };
                var memberExpr = Parser.ParseExpression(isSingleton
                    ? $"{targetClass}.{methodName} || $$generateCallParent({targetClass}, '{methodName}', true)"
                    : $"{targetClass}.prototype.{methodName} || $$generateCallParent({targetClass}, '{methodName}')");
                var declarator = new VariableDeclarator(new Identifier($"$${methodName}"), memberExpr);
                return new VariableDeclaration(NodeList.Create<VariableDeclarator>(new VariableDeclarator[] { declarator }), VariableDeclarationKind.Const);
            });
    }
}