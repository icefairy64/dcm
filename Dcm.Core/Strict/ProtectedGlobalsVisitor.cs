﻿using System.Linq.Expressions;
using Esprima.Ast;
using Esprima.Utils;
using MemberExpression = Esprima.Ast.MemberExpression;

namespace Breezy.Dcm.Core.Strict;

/// <summary>
/// Replaces assignments to properties of protected globals like 'Number' with no-ops.
/// </summary>
public class ProtectedGlobalsVisitor : AstRewriter
{
    protected override object? VisitExpressionStatement(ExpressionStatement expressionStatement)
    {
        if (expressionStatement.Expression is AssignmentExpression { Left: MemberExpression { Object: Identifier { Name: "Number" } } })
            return expressionStatement.UpdateWith(new Literal(0d, "0"));
        return expressionStatement;
    }
}