using Breezy.Dcm.Core.Classes;
using Esprima.Ast;
using Microsoft.Extensions.Logging;

namespace Breezy.Dcm.Core.Fixes;

public class MissingMixinExtendsVisitor : DefineVisitor
{
    private bool AppliedFix;
    private ILogger<MissingMixinExtendsVisitor> Logger;

    public MissingMixinExtendsVisitor(ILogger<MissingMixinExtendsVisitor> logger)
    {
        Logger = logger;
    }

    public override void LeaveClass(ExtClassDefineDeclaration cls)
    {
        AppliedFix = false;
        base.LeaveClass(cls);
    }

    public override IEnumerable<Property>? VisitMethod(Property methodProperty, ExtClassMemberOwner owner)
    {
        return null;
    }

    public override IEnumerable<Property>? VisitProperty(Property property, ExtClassMemberOwner owner)
    {
        if (AppliedFix)
            return null;
        
        if (property.Key is Identifier { Name: "extend" })
        {
            AppliedFix = true;
            return null;
        }
        
        if (property.Key is Identifier { Name: "mixinId" })
        {
            Logger.LogWarning("Adding missing extend specifier to {}", CurrentClass?.ClassName);
            AppliedFix = true;
            return new List<Property>
            {
                new(PropertyKind.Property, new Identifier("extend"), false, new Literal("Ext.Mixin", "'Ext.Mixin'"), false, false),
                property
            };
        }

        return null;
    }
}