﻿using System.Collections.Concurrent;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace Breezy.Dcm.Core.App;

public class ApplicationBuildConfiguration : BuildConfiguration
{
    private readonly ILogger<ApplicationBuildConfiguration> Logger;
    
    public Application? Application { get; set; }
    public string? Environment { get; set; }
    public string? Profile { get; set; }

    public ApplicationBuildConfiguration(ILogger<ApplicationBuildConfiguration> logger)
    {
        Logger = logger;
    }

    public async Task DiscoverPackages(IEnumerable<DirectoryInfo> packagesDir, DirectoryInfo appDir)
    {
        if (Environment is null)
            throw new Exception("Environment is not specified");
        
        Application = await ReadApplication(appDir, Environment, Profile);
        var appToolkit = Application.ResolveToolkit(Profile);

        SetBuildVariable("toolkit.name", appToolkit);

        var appPackages = Application.ResolvePackages(Environment, Profile).ToList();

        await DiscoverPackages(packagesDir, appPackages, appToolkit, Logger);
    }
    
    private async Task<Application> ReadApplication(DirectoryInfo appDir, string environment, string? profile)
    {
        var json = await File.ReadAllTextAsync(Path.Join(appDir.FullName, "app.json")).ConfigureAwait(false);
        var app = JsonSerializer.Deserialize<Application>(json, JsonOptions);
        if (app is null)
            throw new Exception("Failed to read application");
        app.ReadContents(environment, profile, appDir.FullName, BuildVariables);
        return app;
    }
}